<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 18.08.2016
  Time: 10:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="header.">
<div class="row">
    <div class="navbar navbar-static-top<c:if test="${user.role != 'ADMINISTRATOR'}"> navbar-inverse</c:if>">
        <div class="container">
            <div class="collapse navbar-collapse" id="responsive-menu">
                <ul class="nav navbar-nav">
                    <li class="logo"><a href="../../index.jsp"><img src="../img/logo.svg" width="40px" alt=""></a></li>
                    <li><a href="controller?command=take_top">Top20</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle = "dropdown"><fmt:message key="movies"/> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <c:forEach var="genre" items="${genres}">
                                <li><a href="controller?command=take_by_genre&genreId=${genre.genreId}">${genre.genre}</a></li>
                            </c:forEach>
                            <li class="divider"></li>
                            <li><a href="controller?command=take_last"><fmt:message key="new"/> </a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle = "dropdown"><fmt:message key="staff"/> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="controller?command=take_all_actors"><fmt:message key="actors"/> </a></li>
                            <li><a href="controller?command=take_all_directors"><fmt:message key="directors"/> </a></li>
                        </ul>
                    </li>
                </ul>
                <div class="btn-group btn-group-xs navbar-right btn-lng">
                    <a class="btn btn-default " href="controller?lang=en&command=language">ENG</a>
                    <a class="btn btn-default" href="controller?lang=ru&command=language">RUS</a>
                </div>
                <c:choose>
                    <c:when test="${user.role == 'GUEST'}">
                        <a class="btn btn-default btn-xs navbar-right btn-lng" href="controller?command=goto_login_page"><fmt:message key="login"/> </a>
                        <a class="btn btn-default btn-xs navbar-right btn-lng" href="controller?command=goto_register_page"><fmt:message key="registration"/> </a>
                    </c:when>
                    <c:otherwise>
                        <p class="navbar-text navbar-right"><fmt:message key="signed"/> ${user.firstName}
                            <a href="controller?command=goto_user_page" class="navbar-link">(${user.role})  </a>
                            <a class="btn btn-default btn-xs " href="controller?command=logout"><fmt:message key="logout"/> </a>
                        </p>
                    </c:otherwise>
                </c:choose>
                <div class="col-sm-3">
                    <form class="navbar-form" role="search" action="controller">
                        <input type="hidden" name="command" value="search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="<fmt:message key="search"/> " name="string" value="">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</fmt:bundle>