<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="main.">
    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>MovieRating</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <link href="css/font-awesome.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <jsp:include page="include/header.jsp"/>
    <div class="row">
        <h2 class="text-center"> ${message}</h2>
    </div>
    <div class="container">
        <c:if test="${searchResult != null}">
            <div class="row">
                <br>
                    <h3 class="text-center"><fmt:message key="all"/> (${searchResult})</h3>
            </div>
        </c:if>
        <c:forEach var="movie" begin="${0+10*pageNumber}" end="${9+pageNumber*10}" items="${movies}">
            <div class="row">
                <div class="col-xs-2">
                    <a href="controller?command=movie&movieId=${movie.movieId}"> <img class="img" src="${movie.cover}" alt=""></a>
                </div>
                <div class="col-xs-7 col-lg-offset-1">
                    <h3><a href="controller?command=movie&movieId=${movie.movieId}">${movie.name} </a>
                        (${movie.year})
                        <div style="font-size:16px;">
                            <span class="glyphicon glyphicon-star"></span>
                            <c:forEach var="i" begin="2" end="10">
                                <span class="glyphicon <c:choose>
                                                            <c:when test="${movie.rating < i}"> glyphicon-star-empty </c:when>
                                                            <c:otherwise> glyphicon-star </c:otherwise>
                                                       </c:choose>">
                                </span>
                            </c:forEach>
                        </div>
                    </h3>
                    <br>
                    <p>${movie.description}</p>
                </div>
            </div>
            <hr>
        </c:forEach>
        <c:forEach var="actor" begin="${0+10*pageNumber}" end="${9+pageNumber*10}" items="${actors}">
            <div class="row">
                <div class="col-xs-offset-3">
                    <h3><a href="controller?command=movie_by_actor&actorId=${actor.actorId}">${actor.lastName}
                    ${actor.firstName}</a></h3>
                </div>
            </div>
        </c:forEach>
        <c:forEach var="director" begin="${0+10*pageNumber}" end="${9+pageNumber*10}" items="${directors}">
            <div class="row">
                <div class="col-xs-offset-3">
                    <h3><a href="controller?command=movie_by_director&directorId=${director.directorId}">${director.lastName}
                            ${director.firstName}</a></h3>
                </div>
            </div>
        </c:forEach>
        <jsp:include page="include/paginator.jsp"/>
    </div>
    <ctg:footer/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>
    </body>
    </html>
</fmt:bundle>
