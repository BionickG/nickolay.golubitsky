<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 18.08.2016
  Time: 19:50
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="admin.">
    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><fmt:message key="title"/> </title>

        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/font-awesome.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <jsp:include page="include/header.jsp"/>
    <div class="row">
        <h2 class="text-center"> ${message}</h2>
        <c:forEach var="message" items="${errorMessage}">
            <h4 class="text-center">${message}</h4>
        </c:forEach>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="tabs">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#tab-1" data-toggle="tab"><fmt:message key="page"/> </a></li>
                        <li><a href="#tab-2" data-toggle="tab"><fmt:message key="users"/> </a></li>
                        <li><a href="#tab-3" data-toggle="tab"><fmt:message key="actors.directors"/> </a></li>
                        <li><a href="#tab-4" data-toggle="tab"><fmt:message key="movies"/> </a></li>
                        <li><a href="#tab-5" data-toggle="tab"><fmt:message key="country.genre"/> </a></li>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-1">
                            <jsp:include page="userPage.jsp"/>
                        </div>
                        <div class="tab-pane" id="tab-2">
                            <table class="table table-bordered">
                                <tr>
                                    <td><fmt:message key="id"/> </td>
                                    <td><fmt:message key="userRating"/></td>
                                    <td><fmt:message key="login"/> </td>
                                    <td><fmt:message key="first.name"/> </td>
                                    <td><fmt:message key="last.name"/> </td>
                                    <td><fmt:message key="email"/> </td>
                                    <td><fmt:message key="role"/> </td>
                                    <td><fmt:message key="actions"/> </td>
                                </tr>
                                <c:forEach var="userC" items="${users}">
                                    <tr>
                                        <td>${userC.userId}</td>
                                        <td>${userC.userRating}</td>
                                        <td>${userC.login}</td>
                                        <td>${userC.firstName}</td>
                                        <td>${userC.lastName}</td>
                                        <td>${userC.email}</td>
                                        <td>${userC.role}</td>
                                        <td>
                                            <c:if test="${user.userId != userC.userId}">
                                                <c:if test="${userC.banned}">
                                                    <a class="btn btn-default"
                                                       href="controller?command=ban_unban&userId=${userC.userId}&check=${check}">
                                                        <fmt:message key="unban"/> </a>
                                                </c:if>
                                                <c:if test="${!userC.banned}">
                                                    <a class="btn btn-default"
                                                       href="controller?command=ban_unban&userId=${userC.userId}&check=${check}">
                                                        <fmt:message key="ban"/> </a>
                                                </c:if>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab-3">
                            <div class="row">
                                <p class="text-center"> <fmt:message key="add.actor"/> </p>
                            </div>
                            <div class="row">
                                <form class="form-horizontal" action="controller" method="post">
                                    <input type="hidden" name="command" value="add_actor">
                                    <input type="hidden" name="check" value="${check}"/>
                                    <div class="form-group">
                                        <label for="first_name" class="col-xs-2 control-label"><fmt:message key="first.name"/> </label>
                                        <div class="col-xs-9">
                                            <input type="text" class="form-control" id="first_name" name="firstName"
                                                    placeholder="<fmt:message key="first.name"/>" pattern="[A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name" class="col-xs-2 control-label"><fmt:message key="last.name"/> </label>
                                        <div class="col-xs-9">
                                            <input type="text" class="form-control" id="last_name" name="lastName"
                                                    placeholder="<fmt:message key="last.name"/> " pattern="[A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-default"><fmt:message key="add.actor"/> </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row">
                                <p class="text-center"> <fmt:message key="add.director"/> </p>
                            </div>
                            <div class="row">
                                <form class="form-horizontal" action="controller" method="post">
                                    <input type="hidden" name="command" value="add_director">
                                    <input type="hidden" name="check" value="${check}"/>

                                    <div class="form-group">
                                        <label for="f_name" class="col-xs-2 control-label"><fmt:message key="first.name"/> </label>
                                        <div class="col-xs-9">
                                            <input type="text" class="form-control" id="f_name" name="firstName"
                                                   placeholder="<fmt:message key="first.name"/>" pattern="[A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="l_name" class="col-xs-2 control-label"><fmt:message key="last.name"/> </label>
                                        <div class="col-xs-9">
                                            <input type="text" class="form-control" id="l_name" name="lastName"
                                                   placeholder="<fmt:message key="last.name"/>" pattern="[A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-default"><fmt:message key="add.director"/> </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row">
                                <h3 class="text-center"> <fmt:message key="actors"/> </h3>
                            </div>
                            <table class="table table-bordered">
                                <tr>
                                    <td><fmt:message key="id"/> </td>
                                    <td><fmt:message key="first.name"/> </td>
                                    <td><fmt:message key="last.name"/> </td>
                                    <td><fmt:message key="actions"/> </td>
                                </tr>
                                <c:forEach var="actor" items="${actors}">
                                    <tr>
                                        <td>${actor.actorId}</td>
                                        <td>${actor.firstName}</td>
                                        <td>${actor.lastName}</td>
                                        <td>
                                            <a class="btn btn-default" href="controller?command=delete_actor&actorId=${actor.actorId}">
                                                <fmt:message key="delete"/> </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                            <div class="row">
                                <h3 class="text-center"> <fmt:message key="directors"/> </h3>
                            </div>
                            <table class="table table-bordered">
                                <tr>
                                    <td><fmt:message key="id"/> </td>
                                    <td><fmt:message key="first.name"/> </td>
                                    <td><fmt:message key="last.name"/> </td>
                                    <td><fmt:message key="actions"/> </td>
                                </tr>
                                <c:forEach var="director" items="${directors}">
                                    <tr>
                                        <td>${director.directorId}</td>
                                        <td>${director.firstName}</td>
                                        <td>${director.lastName}</td>
                                        <td>
                                            <a class="btn btn-default" href="controller?command=delete_director&actorId=${actor.actorId}">
                                                <fmt:message key="delete"/> </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab-4">
                            <form action="controller" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="command" value="movie_add">
                                <input type="hidden" name="check" value="${check}"/>
                                <div class="row">
                                    <form action="controller">
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="name">Название</label>
                                                <input class="form-control" type="text" value="" placeholder="name" name="name" id="name">
                                            </div>
                                            <div class="form-group">
                                                <label for="actor"><fmt:message key="actors"/> </label>
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="actor"/> " name="actors[]" id="actor"
                                                       pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="actor"/> " name="actors[]"
                                                       pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="actor"/> " name="actors[]"
                                                       pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="actor"/> " name="actors[]"
                                                       pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="actor"/> " name="actors[]"
                                                       pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="actor"/> " name="actors[]"
                                                       pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="actor"/> " name="actors[]"
                                                       pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                                                <br>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="duration"><fmt:message key="duration"/> </label>
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="duration"/> " name="duration" id="duration"
                                                       pattern="\d{2,3}">
                                            </div>
                                            <div class="form-group">
                                                <label for="description"><fmt:message key="description"/> </label>
                                                <textarea class="form-control" value="" placeholder="<fmt:message key="description"/>" rows="8" name="description" id="description"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="trailer"><fmt:message key="trailer"/> </label>
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="trailer"/>" name="trailerLink" id="trailer">
                                            </div>
                                            <div class="form-group">
                                                <label for="cover"><fmt:message key="cover"/> </label>
                                                <input class="form-control" type="file" value=""  name="cover" id="cover">
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="genre"><fmt:message key="genre"/> </label>
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="genre"/>" name="genres[]" id="genre"
                                                       pattern="[A-zА-яё \-]{2,20}">
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="genre"/>" name="genres[]"
                                                       pattern="[A-zА-яё \-]{2,20}">
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="genre"/>" name="genres[]"
                                                       pattern="[A-zА-яё \-]{2,20}">
                                            </div>
                                            <div class="form-group">
                                                <label for="year"><fmt:message key="year"/> </label>
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="year"/>" name="year" id="year"
                                                       pattern="(19|20){1}\d{2}">
                                            </div>
                                            <div class="form-group">
                                                <label for="country"><fmt:message key="country"/> </label>
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="country"/>" name="country" id="country"
                                                       pattern="[A-zА-яё ]{2,20}">
                                            </div>
                                            <div class="form-group">
                                                <label for="director"><fmt:message key="director"/> </label>
                                                <input class="form-control" type="text" value="" placeholder="<fmt:message key="director"/> " name="director" id="director"
                                                       pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <br>
                                            <button class="btn btn-default btn-block" type="submit"><fmt:message key="commit"/> </button>
                                        </div>
                                    </form>
                                </div>
                            </form>
                            <div class="col-xs-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <td><fmt:message key="id"/> </td>
                                        <td><fmt:message key="name"/> </td>
                                        <td><fmt:message key="year"/></td>
                                        <td><fmt:message key="country"/></td>
                                        <td><fmt:message key="director"/></td>
                                        <td><fmt:message key="added"/></td>
                                        <td><fmt:message key="actions"/> </td>
                                    </tr>
                                    <c:forEach var="movie" items="${movies}">
                                        <tr>
                                            <td>${movie.movieId}</td>
                                            <td>${movie.name}</td>
                                            <td>${movie.year}</td>
                                            <td>${movie.country.country}</td>
                                            <td>${movie.director}</td>
                                            <td>${movie.dateAdded}</td>
                                            <td>
                                                <a class="btn btn-default"
                                                   href="controller?command=delete_movie&movieId=${movie.movieId}"><fmt:message key="delete"/> </a>
                                                <a class="btn btn-default"
                                                   href="controller?command=goto_update_page&movieId=${movie.movieId}"><fmt:message key="update"/> </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-5">
                            <div class="row">
                                <h3 class="text-center"> <fmt:message key="genre"/> </h3>
                            </div>
                            <table class="table table-bordered">
                                <tr>
                                    <td><fmt:message key="id"/> </td>
                                    <td><fmt:message key="genre"/> </td>
                                    <td><fmt:message key="actions"/> </td>
                                </tr>
                                <c:forEach var="genre" items="${genres}">
                                    <tr>
                                        <td>${genre.genreId}</td>
                                        <td>${genre.genre}</td>
                                        <td>
                                            <a class="btn btn-default" href="controller?command=delete_genre&genreId=${genre.genreId}">
                                                <fmt:message key="delete"/> </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                            <div class="row">
                                <h3 class="text-center"> <fmt:message key="countries"/> </h3>
                            </div>
                            <table class="table table-bordered">
                                <tr>
                                    <td><fmt:message key="id"/> </td>
                                    <td><fmt:message key="country"/> </td>
                                    <td><fmt:message key="actions"/> </td>

                                </tr>
                                <c:forEach var="country" items="${countries}">
                                    <tr>
                                        <td>${country.countryId}</td>
                                        <td>${country.country}</td>
                                        <td>
                                            <a class="btn btn-default" href="controller?command=delete_country&countryId=${country.countryId}">
                                                <fmt:message key="delete"/> </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ctg:footer/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.js"></script>
    </body>
    </html>
</fmt:bundle>

