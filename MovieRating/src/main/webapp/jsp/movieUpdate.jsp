<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 18.08.2016
  Time: 19:50
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="movie.">
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><fmt:message key="title"/> </title>

        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/font-awesome.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <jsp:include page="include/header.jsp"/>
    <div class="row">
        <h2 class="text-center"> ${message}</h2>
    </div>
    <form action="controller" method="post" enctype="multipart/form-data">
        <input type="hidden" name="command" value="update_movie">
        <input type="hidden" name="movieId" value="${movie.movieId}">
        <div class="row">
            <div class="container">
                <div class="col-xs-3">
                    <img src="../${movie.cover}"  width="360" alt="">
                    <br>
                    <p><fmt:message key="change.cover"/></p>
                    <input class="form-control" type="file" accept="image/*" value="${movie.cover}"
                           placeholder="<fmt:message key="change.cover"/>" name="cover" id="cover">
                </div>
                <div class="col-xs-7 col-xs-offset-2">
                    <div class="row">
                        <div class="col-xs-4 pull-left">
                            <p><fmt:message key="name"/> </p>
                        </div>
                        <div class="col-xs-8">
                            <input class="form-control" type="text" value="${movie.name}" name="name"
                            pattern="^.{1,30}">
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-left">
                            <p><fmt:message key="year"/> </p>
                        </div>
                        <div class="col-xs-8">
                            <input class="form-control" type="text" value="${movie.year}" name="year"
                                   pattern="[12]{1}[90]{1}\d{2}}">
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-left">
                            <p><fmt:message key="description"/> </p>
                        </div>
                        <div class="col-xs-8">
                            <textarea class="form-control" value="${movie.description}"
                                      rows="4" name="description"></textarea>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-left">
                            <p><fmt:message key="director"/> </p>
                        </div>
                        <div class="col-xs-8">
                            <div class="form-horizontal">
                                <input type="text" value="${movie.director.firstName} ${movie.director.lastName}"
                                       name="director" pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-left">
                            <p><fmt:message key="starring"/> </p>
                        </div>
                        <div class="col-xs-8">
                            <c:forEach var="actor" items="${movie.actors}">
                                <p>${actor.firstName} ${actor.lastName}
                                    <a href="controller?command=delete_actor_from_movie&actorId=${actor.actorId}&movieId=${movie.movieId}">
                                        <i class="fa fa-times" aria-hidden="true"></i></a></p>
                            </c:forEach>
                            <input class="form-control" type="text" value="" placeholder="<fmt:message key="actor"/>" name="actors"
                                   pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})}">
                            <br>
                            <input class="form-control" type="text" value="" placeholder="<fmt:message key="actor"/>" name="actors"
                                   pattern="([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})">
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-left">
                            <p><fmt:message key="genres"/> </p>
                        </div>
                        <div class="col-xs-8">
                            <p class="pull-left">
                                <c:forEach var="genre" items="${movie.genres}">
                            <p> ${genre}
                                <a href="controller?command=delete_genre_from_movie&genreId=${genre.genreId}&movieId=${movie.movieId}">
                                    <i class="fa fa-times" aria-hidden="true"></i></a></p>
                            </c:forEach>
                            </p>
                            <input class="form-control" type="text" value="" placeholder="<fmt:message key="genre"/> " name="genre"
                                   pattern="[A-zА-яё \-]{2,20}">
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-left">
                            <p><fmt:message key="country"/> </p>
                        </div>
                        <div class="col-xs-8">
                            <p class="pull-left">
                                <input class="form-control" type="text" value="${movie.country.country}" name="country"
                                       pattern="[A-zА-яё ]{2,20}">
                            </p>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-left">
                            <p><fmt:message key="duration"/> </p>
                        </div>
                        <div class="col-xs-8">
                            <p class="pull-left">
                                <input class="form-control" type="text" value="${movie.duration}" name="duration"
                                       pattern="\d{2,3}">
                            </p>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 pull-left">
                            <p><fmt:message key="trailer.link"/> </p>
                        </div>
                        <div class="col-xs-8">
                            <p class="pull-left">
                                <input class="form-control form-inline" type="text" value="${movie.trailer}"
                                       name="trailerLink">
                            </p>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-offset-6">
                <button class="btn btn-default" type="submit"><fmt:message key="save"/> </button>
                <a class="btn btn-default" href="controller?command=goto_user_page"><fmt:message key="return"/> </a>
            </div>
        </div>
    </form>
    <ctg:footer/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.js"></script>
    </body>
    </html>
</fmt:bundle>

