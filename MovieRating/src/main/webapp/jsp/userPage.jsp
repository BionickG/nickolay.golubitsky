<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="user.">
    <html>
    <head>
        <title><fmt:message key="title"/> </title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/font-awesome.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <c:if test="${user.role != 'ADMINISTRATOR'}">
        <jsp:include page="include/header.jsp"/>
    </c:if>
        <div class="container">
            <div class="row">
                <c:if test="${user.role != 'ADMINISTRATOR'}">
                    <span class="text-center">${message}</span>
                </c:if>
            </div>
    <div class="row">
            <div class="col-xs-3 col-xs-offset-1">
                <img class="img-responsive" src="${user.avatar}" alt="">
                <form action="controller" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="command" value="add_avatar">
                    <input type="file" name="avatar" accept="image/*">
                    <button class="btn btn-success">Submit</button>
                </form>
            </div>
            <div class="col-xs-7">
                <table class="table table-striped">
                    <tr>
                        <td><fmt:message key="rating"/></td>
                        <td>${user.userRating}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="first.name"/></td>
                        <td>${user.firstName}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="last.name"/></td>
                        <td>${user.lastName}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="email"/></td>
                        <td>${user.email}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="status"/></td>
                        <c:choose>
                            <c:when test="${iser.banned}">
                                <td><fmt:message key="banned"/></td>
                            </c:when>
                            <c:otherwise>
                                <td><fmt:message key="ok"/></td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                    <tr>
                        <td><fmt:message key="login"/></td>
                        <td>${user.login}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="change.password"/></td>
                        <td>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><fmt:message key="change.password"/> </button>
                            <!-- Modal -->
                            <div id="myModal" class="modal fade" role="dialog">
                                <form class="form-horizontal" action="controller" method="post">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><fmt:message key="change.password"/> </h4>
                                            </div>
                                            <div class="modal-body">
                                                    <input type="hidden" name="command" value="change_password">
                                                    <div class="form-group">
                                                        <label for="password" class="col-sm-3 control-label"><fmt:message key="password"/> </label>
                                                        <div class="col-xs-9">
                                                            <input type="text" id="password" name="password" placeholder="<fmt:message key="password"/>" class="form-control" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="passwordNew" class="col-sm-3 control-label"><fmt:message key="password.new"/> </label>
                                                        <div class="col-xs-9">
                                                            <input type="password" id="passwordNew" name="passwordNew" placeholder="<fmt:message key="password.new"/> " class="form-control" autofocus>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-default"> Submit</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
    </div>
    </div><!-- /container -->
    <c:if test="${user.role != 'ADMINISTRATOR'}">
        <ctg:footer/>
    </c:if>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    </body>
    </html>
</fmt:bundle>