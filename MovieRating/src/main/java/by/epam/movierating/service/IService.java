package by.epam.movierating.service;

import by.epam.movierating.exception.ServiceException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code IService} abstract class enumerates all the necessary methods
 * for manipulation entity's data in database.
 */
interface IService<T> {

    /**
     * Method takes all instance of current entity
     *
     * @return list of entity
     * @throws ServiceException if {@code DAOException} vas cached
     */

        List<T> takeAll() throws ServiceException;

    /**
     * Method takes instance of entity by instance's
     * identification number
     *
     * @param id is instance's identification number
     * @return current instance of entity with such{@code id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

        T take(Long id) throws ServiceException;

        /**
         * Method adds current instance to database
         * @param t is instance which will be add
         * @throws ServiceException if {@code DAOException} vas cached
         */

        void add(T t) throws ServiceException;

    /**
     * Method deletes instance of entity form database
     * @param id is instance which will be delete
     * @throws ServiceException if {@code DAOException} vas cached
     */

    void delete(Long id) throws ServiceException;

    /**
     * Method takes new instance of entity and replace other one
     * @param id is instance's identification number
     * @param t new instance which will replace existing one
     * @throws ServiceException if {@code DAOException} vas cached
     */

    void update(Long id, T t) throws ServiceException;

    }

