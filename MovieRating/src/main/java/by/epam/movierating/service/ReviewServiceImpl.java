package by.epam.movierating.service;

import by.epam.movierating.dao.ReviewDAO;
import by.epam.movierating.entity.Review;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.ServiceException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code ReviewServiceImpl} class represents a layer-class between
 * the {@code ReviewDAO} class and servlet.
 */

public class ReviewServiceImpl implements IService<Review> {

    private ReviewDAO reviewDAO = new ReviewDAO();

    /**
     * Takes all reviews from database
     *
     * @return list of instances {@code Review}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public List<Review> takeAll() throws ServiceException {
        try {
            return reviewDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes rating from database by review's identification
     * number
     *
     * @param id is review's identification number
     * @return instance of {@code Review} with such {@code id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public Review take(Long id) throws ServiceException {
        try {
            return reviewDAO.take(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Adds instance of {@code Review} to database
     *
     * @param review is instance which will be added
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void add(Review review) throws ServiceException {
        try {
            reviewDAO.add(review);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Deletes rating from database by delete's identification number
     * @param id is identification number of instance which will be deleted
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            reviewDAO.delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Method takes new instance of {@code Review} and replace other one
     * @param id is review's identification number
     * @param review new instance of {@code Review} which will replace existing one
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void update(Long id, Review review) throws ServiceException {
        try {
            reviewDAO.update(id, review);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes all reviews of current movie
     * @param movieId is identification number of movie
     *                which reviews we searching for
     * @return list of {@code Review} of current movie
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public List<Review> takeAllReviewsByMovieId(Long movieId) throws ServiceException{
        try {
            return reviewDAO.takeByMovieId(movieId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Add mark to the existing mark of review
     * @param reviewId identification number of review wich fill with a mark
     * @param mark is mark which will be added
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void changeMark(Long reviewId, int mark) throws ServiceException{
        try {
            reviewDAO.changeMark(reviewId, mark);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes all marks of user's reviews
     * @param userId is identification number of user
     * @return summ of all marks
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public int takeAllMarks (Long userId) throws ServiceException{
        try {
            return reviewDAO.takeAllMarks(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Add link between user table and review table
     * that means user add his own review
     * @param userId is identification number of user who adding review
     * @param reviewId identification number of review which will be marked
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void addConnectionWithUser(Long userId, Long reviewId) throws ServiceException {
        try {
            reviewDAO.addConnectionWithUser(userId, reviewId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Method checks did user already marked movie
     * @param userId is user's identification number
     * @param reviewId is review's identification number
     * @return boolean value which marks user's choice
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public boolean isConnectionExist (Long userId, Long reviewId) throws ServiceException {
        try {
            return userId != null && reviewDAO.isConnectionExist(userId, reviewId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
