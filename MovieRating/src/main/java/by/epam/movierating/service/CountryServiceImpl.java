package by.epam.movierating.service;

import by.epam.movierating.dao.CountryDAO;
import by.epam.movierating.entity.Country;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.ServiceException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code CountryServiceImpl} class represents a layer-class between
 * the {@code CountryDAO} class and servlet.
 */

public class CountryServiceImpl implements IService<Country> {
    private CountryDAO countryDAO = new CountryDAO();

    /**
     * Takes all countries from database
     *
     * @return list of instances {@code Country}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public List<Country> takeAll() throws ServiceException {
        try {
            return countryDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes country from database by country identification number
     *
     * @param id is country's identification number
     * @return instance of {@code Country} with such {@code id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public Country take(Long id) throws ServiceException {
        try {
            return countryDAO.take(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Adds country to database
     *
     * @param country instance of {@code Country} which will be added
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void add(Country country) throws ServiceException {
        try {
            countryDAO.add(country);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Deletes country from database
     *
     * @param id is country's identification number which will be delete
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            countryDAO.delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * It's cap for update method
     *
     * @param id is country identification number
     * @param country is instance of {@code Country}
     * @throws ServiceException in the case of function call
     */

    @Override
    public void update(Long id, Country country) throws ServiceException {
        throw new ServiceException("no such method");
    }


    /**
     * Takes instance of {@code Country} by country's name from the database
     * if such country doesn't exist create new instance and add it to database
     * @param countryName is country name
     * @return instance of {@code Country} satisfying the condition
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public Country takeCountryByNameWithCreation(String countryName) throws ServiceException{
        try {
            Country country = countryDAO.takeByName(countryName);
            if (country == null){
                country = new Country(countryName);
                countryDAO.add(country);
                country.setCountryId((countryDAO.takeByName(countryName)).getCountryId());
            }
            return country;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
