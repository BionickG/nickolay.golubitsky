package by.epam.movierating.service;

import by.epam.movierating.dao.ActorDAO;
import by.epam.movierating.entity.Actor;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.ServiceException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code ActorServiceImpl} class represents a layer-class between
 * the {@code ActorDAO} class and servlet.
 */
public class ActorServiceImpl implements IService<Actor>{
    private ActorDAO actorDAO = new ActorDAO();

    /**
     * Takes all actors from database
     *
     * @return list of instances {@code Actor}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public List<Actor> takeAll() throws ServiceException {
        try {
            return actorDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes actor from database by actor identification number
     * @param id is actor's identification number
     * @return instance of {@code Actor} with such {@id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public Actor take(Long id) throws ServiceException {
        try {
            return actorDAO.take(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Adds instance of {@code Actor} to database
     *
     * @param actor is instance which will be added
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void add(Actor actor) throws ServiceException {
        try {
            actorDAO.add(actor);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    /** Deletes actor from database by actor
     * identification number
     * @param id is identification number of instance which will be deleted
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            actorDAO.delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    /**
     * It's cap for update method
     *
     * @param id    is actor identification number
     * @param actor is instance of {@code Actor}
     * @throws ServiceException in the case of function call
     */

    @Override
    public void update(Long id, Actor actor) throws ServiceException {
        throw new ServiceException("No such method");
    }

    /**
     * Takes actors from database which starred in movie with such
     * movie's id{@code id}
     *
     * @param movieId is movie ID
     * @return list of actors which starred in current movie
     * @throws ServiceException if {@code DAOException} vas cached
     */


    public List<Actor> takeActorsByMovieId(Long movieId)throws ServiceException{
        try {
            return actorDAO.takeActorsByMovieId(movieId);
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }

    /**
     * Takes actor from database by actor's full name
     * @param fullName is full name of an actor
     * @return instance of {@code Actor} satisfying the condition
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public Actor takeActorByFullName(String fullName) throws ServiceException {
        try {
            String[] strings = fullName.split(" ");
            String firstName = strings[0];
            String lastName = strings[1];
            Actor actor = actorDAO.takeActorByName(firstName, lastName);
            if (actor == null) {
                actor = new Actor(firstName, lastName);
                actorDAO.add(actor);
                actor.setActorId((actorDAO.takeActorByName(firstName, lastName)).getActorId());
            }
            return actor;

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


}
