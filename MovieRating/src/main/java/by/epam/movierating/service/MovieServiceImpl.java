package by.epam.movierating.service;


import by.epam.movierating.dao.MovieDAO;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.ServiceException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code MovieServiceImpl} class represents a layer-class between
 * the {@code MovieDAO} class and servlet.
 */

public class MovieServiceImpl implements IService<Movie> {
    private MovieDAO movieDAO = new MovieDAO();

    /**
     * Takes all movies from database
     *
     * @return list of instances {@code Movies}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public List<Movie> takeAll() throws ServiceException {
        try {
            return movieDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes movie from database by movie's identification
     * number
     *
     * @param id is movie's identification number
     * @return instance of {@code Movie} with such {@code id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public Movie take(Long id) throws ServiceException {
        try {
            return movieDAO.take(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Adds instance of {@code Movie} to database
     *
     * @param movie is instance which will be added
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void add(Movie movie) throws ServiceException {
        try {
            movieDAO.add(movie);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    /** Deletes movie from database by movie's identification number
     * @param id is identification number of instance which will be deleted
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            movieDAO.delete(id);
        } catch (DAOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Method takes new instance of {@code Movie} and replace other one
     * @param id is movie's identification number
     * @param movie new instance of {@code Movie} which will replace existing one
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void update(Long id, Movie movie) throws ServiceException {
        try {
            movieDAO.update(id, movie);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes movies by actor who starred in the movie
     * @param actorId is identification number of actor
     *           who starred in the movie
     * @return list of {@code Movie} with actor with {@code id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public List<Movie> takeMoviesByActorId(Long actorId)throws ServiceException{
        try {
            return movieDAO.takeMoviesByActorId(actorId);
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }

    /**
     * Takes movies by the director who shot the film
     *
     * @param actorId is director's identification number
     * @return list of {@code Movie} shooting by director
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public List<Movie> takeMoviesByDirectorId(Long actorId) throws ServiceException {
        try {
            return movieDAO.takeMoviesByDirectorId(actorId);
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }

    /**
     * Takes movies by the country where the film was shot
     * @param countryId is country's identification number
     * @return list of {@code Movie} with such country
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public List<Movie> takeMoviesByCountryId(Long countryId)throws ServiceException{
        try {
            return movieDAO.takeMoviesByCountryId(countryId);
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }

    /**
     * Takes all movies of a particular genre
     * @param genreId is identification number of genre
     * @return list of {@code Movie} with such genre
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public List<Movie> takeMovieByGenre(Long genreId)throws ServiceException{
        try {
            return movieDAO.takeAllMoviesByGenre(genreId);
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }

    /**
     * Takes last added to database movies
     * @return list of last added {@code Movie}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public List<Movie>takeLast() throws ServiceException{
        try {
            return movieDAO.takeLastAdded();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes movie by name and year from database
     * @param name is name of movie
     * @param year is year when movie was released
     * @return instance of {@code Movie} with such {@code name} and {@code year}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public Movie takeMovieByNameAndYear(String name, int year) throws ServiceException{
        try {
            return movieDAO.takeByNameAndYear(name, year);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes movies only by name from database
     * @param name is name of movie
     * @return list of {@code Movie} with such {@code name}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public List<Movie> takeMoviesByName (String name) throws ServiceException{
        try {
            String nameSearch = name + "%";
            return movieDAO.takeByName(nameSearch);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Add link between movie table and actor table
     * that means the actor starred in the film
     * @param movieId is identification number of actor starred in the film
     * @param actorId is identification number of film where actor was starred
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void addConnectionWithActorTable(Long movieId, Long actorId) throws ServiceException{
        try {
            movieDAO.addConnectionWithActorTable(movieId, actorId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Add link between movie table and genre table
     * that means add genre to the current film
     * @param movieId is identification number of movie which will be added
     * @param genreId is identification number of genre which should be added
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void addConnectionWithGenreTable(Long movieId, Long genreId) throws ServiceException{
        try {
            movieDAO.addConnectionWithGenreTable(movieId, genreId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Delete link between movie table and genre table
     * that means delete genre from the movie
     * @param movieId is identification number of movie
     * @param genreId is identification number of genre which should be deleted
     * @throws ServiceException if {@code DAOException} vas cached

     */

    public void deleteConnectionWithGenreTable(Long movieId, Long genreId) throws ServiceException{
        try {
            movieDAO.deleteConnectionWithGenreTable(movieId, genreId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Delete link between movie table and actor table
     * that means  delete actor from the staff of film
     * @param movieId is identification number of actor starred in the film
     * @param actorId is identification number of film where actor was starred
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void deleteConnectionWithActorTable(Long movieId, Long actorId) throws ServiceException{
        try {
            movieDAO.deleteConnectionWithActorTable(movieId, actorId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
