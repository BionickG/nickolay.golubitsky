package by.epam.movierating.service;

import by.epam.movierating.dao.GenreDAO;
import by.epam.movierating.entity.Genre;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.ServiceException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code GenreServiceImpl} class represents a layer-class between
 * the {@code GenreDAO} class and servlet.
 */

public class GenreServiceImpl implements IService<Genre>{

    private GenreDAO genreDAO = new GenreDAO();

    /**
     * Takes all genres from database
     *
     * @return list of instances {@code Genre}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public List<Genre> takeAll() throws ServiceException {
        try {
            return genreDAO.takeAll() ;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes genre from database by genre's identification number
     *
     * @param id is genre's identification number
     * @return instance of {@code Genre} with such {@code id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public Genre take(Long id) throws ServiceException {
        try {
            return genreDAO.take(id);
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }

    /**
     * Adds genre to database
     *
     * @param genre instance of {@code Genre} which will be added
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void add(Genre genre) throws ServiceException {
        try {
            genreDAO.add(genre);
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }

    /**
     * Deletes genre from database
     *
     * @param id is genre's identification number which will be delete
     * @throws ServiceException if {@code DAOException} vas cached
     */


    @Override
    public void delete(Long id) throws ServiceException {
        try {
            genreDAO.delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * It's cap for update method
     *
     * @param id    is genre's identification number
     * @param genre is instance of {@code Genre}
     * @throws ServiceException in the case of function call
     */

    @Override
    public void update(Long id, Genre genre) throws ServiceException {
        throw new ServiceException("no such method");
    }

    /**
     * Returns all genres of current movie
     *
     * @param movieId is identification number of movie which genres will be returned
     * @return list of instances {@code Genres} of current movie
     * @throws ServiceException in the case of function call
     */

    public List<Genre> takeAllGenresOfMovie(Long movieId) throws ServiceException {
        try {
            return genreDAO.takeAllGenresOfMovie(movieId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes genre from database by genre's name if in database
     * no such genre create new instance of {@code Genre} and
     * add it to database
     *
     * @param genreName is name of genre
     * @return instance of {@code Genre} with such {@code name}
     * @throws ServiceException in the case of function call
     */

    public Genre takeByNameWithCreation(String genreName) throws ServiceException {
        try {
            Genre genre = genreDAO.takeByName(genreName);
            if (genre == null) {
                genre = new Genre(genreName);
                genreDAO.add(genre);
                genre.setGenreId((genreDAO.take(genre.getGenreId())).getGenreId());
            }
            return genre;
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }
}
