package by.epam.movierating.service;


import by.epam.movierating.dao.UserDAO;
import by.epam.movierating.encryption.EncryptionData;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.ServiceException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code UserServiceImpl} class represents a layer-class between
 * the {@code UserDAO} class and servlet.
 */

public class UserServiceImpl implements IService<User> {
    private UserDAO userDAO = new UserDAO();

    /**
     * Defines user by user login and password
     * @param login is user's login string
     * @param password is user encrypted password
     * @return instanse of{@code User} wuth such {@code login} and
     * {@code password} if he exists
     * @throws ServiceException if {@code DAOException} vas cached

     */
    public User defineUser(String login, String password) throws ServiceException {
        String passwordEnc = EncryptionData.getMD5(password);
        try {
            return userDAO.defineUser(login, passwordEnc);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    /**
     * Takes all users from database
     *
     * @return list of instances {@code User}
     * @throws ServiceException if {@code DAOException} vas cached
     */
    @Override
    public List<User> takeAll() throws ServiceException {
        try {
            return userDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes rating from database by user's identification
     * number
     *
     * @param id is user's identification number
     * @return instance of {@code Rating} with such {@code id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public User take(Long id) throws ServiceException {
        try {
            return userDAO.take(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Adds instance of {@code User} to database
     *
     * @param user is instance which will be added
     * @throws ServiceException if {@code DAOException} vas cached

     */
    @Override
    public void add(User user) throws ServiceException {
        try {
            userDAO.add(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    /**
     * Deletes user from database by user's identification number
     * @param id is identification number of instance which will be deleted
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            userDAO.delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    /**
     * Updates user's information in database by user's identification number
     * @param id is identification number of user
     * @param user new instance of {@code User}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void update(Long id, User user) throws ServiceException {
        try {
            userDAO.update(id, user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Logic operation required for determine user
     * (is user exists in the system)
     * @param login is user's login string
     * @return return true if user exists
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public boolean takeUserByLogin (String login) throws ServiceException{
        try {
            return userDAO.takeUserByLogin(login);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * This method serve for ban user if he unbanned
     * and unban if he is banned
     * @param id is user's identification number
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void banUnban(Long id) throws ServiceException{
        try {
            User user = userDAO.take(id);
            if (user.isBanned()){
                user.setBanned(false);
            } else {
                user.setBanned(true);
            }
            userDAO.ban(id, user.isBanned());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Add avatar image to user profile
     * @param id user's identification number
     * @param avatar is url string with image path
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void addAvatar(Long id, String avatar) throws ServiceException{
        try {
            userDAO.addAvatar(id, avatar);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Change user's password in database
     * @param userId is identification number of user who will be changed password
     * @param password is user's password
     * @param passwordNew is new user's password
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void changePassword(Long userId, String password, String passwordNew) throws ServiceException{
        if (password != null && passwordNew != null){
            String passwordMD5 = EncryptionData.getMD5(password);
            String passwordNewMD5 = EncryptionData.getMD5(passwordNew);
            if (passwordMD5.equals(passwordNewMD5)){
                try {
                    userDAO.changePassword(userId, passwordNewMD5);
                } catch (DAOException e) {
                    throw new ServiceException(e);
                }
            }
        }
    }
}
