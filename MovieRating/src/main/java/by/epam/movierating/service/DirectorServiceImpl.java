package by.epam.movierating.service;

import by.epam.movierating.dao.DirectorDAO;
import by.epam.movierating.entity.Director;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.ServiceException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code DirectorServiceImpl} class represents a layer-class between
 * the {@code DirectorDAO} class and servlet.
 */

public class DirectorServiceImpl implements IService<Director> {

    private DirectorDAO directorDAO = new DirectorDAO();


    /**
     * Takes all directors from database
     *
     * @return list of instances {@code Director}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public List<Director> takeAll() throws ServiceException {
        try {
            return directorDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Takes director from database by director identification number
     *
     * @param id is directors's identification number
     * @return instance of {@code Director} with such {@code id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public Director take(Long id) throws ServiceException {
        try {
            return directorDAO.take(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Adds instance of {@code Director} to database
     *
     * @param director is instance which will be added
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void add(Director director) throws ServiceException {
        try {
            directorDAO.add(director);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Deletes actor from database by director
     * identification number
     *
     * @param id is identification number of instance which will be deleted
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            directorDAO.delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * It's cap for update method
     *
     * @param id is country identification number
     * @param director is instance of {@code Country}
     *
     * @throws ServiceException in the case of function call
     */

    @Override
    public void update(Long id, Director director) throws ServiceException {
        throw new ServiceException("no such method");
    }

    /**
     * Takes actor from database by actor's full name
     * @param fullName is director's full name
     * @return instance of {@code Director} satisfying the condition
     * @throws ServiceException in the case of function call
     */

    public Director takeDirectorByFullName(String fullName) throws ServiceException {
        try {
            String[] strings = fullName.split(" ");
            String firstName = strings[0];
            String lastName = strings[1];
            Director director = directorDAO.takeDirectorByName(firstName, lastName);
            if (director == null){
                director = new Director(firstName, lastName);
                directorDAO.add(director);
                director.setDirectorId((directorDAO.takeDirectorByName(firstName, lastName)).getDirectorId());
            }
            return director;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


}
