package by.epam.movierating.service;

import by.epam.movierating.dao.RatingDAO;
import by.epam.movierating.entity.Rating;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.ServiceException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code RatingServiceImpl} class represents a layer-class between
 * the {@code RatingDAO} class and servlet.
 */

public class RatingServiceImpl implements IService<Rating>{
    RatingDAO ratingDAO = new RatingDAO();

    /**
     * Takes all ratings from database
     *
     * @return list of instances {@code Rating}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public List<Rating> takeAll() throws ServiceException {
        try {
            return ratingDAO.takeAll();
        } catch (DAOException e) {
            throw  new ServiceException();
        }
    }

    /**
     * Takes rating from database by rating's identification
     * number
     *
     * @param id is ratings's identification number
     * @return instance of {@code Rating} with such {@code id}
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public Rating take(Long id) throws ServiceException {
        try {
            return ratingDAO.take(id);
        } catch (DAOException e) {
            throw  new ServiceException();
        }
    }

    /**
     * Adds instance of {@code Rating} to database
     *
     * @param rating is instance which will be added
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void add(Rating rating) throws ServiceException {
        try {
             ratingDAO.add(rating);
        } catch (DAOException e) {
            throw  new ServiceException();
        }
    }

    /**
     * Deletes rating from database by rating's identification number
     * @param id is identification number of instance which will be deleted
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void delete(Long id) throws ServiceException {
        try {
             ratingDAO.delete(id);
        } catch (DAOException e) {
            throw  new ServiceException();
        }
    }

    /**
     * Method takes new instance of {@code Rating} and replace other one
     * @param id is rating's identification number
     * @param rating new instance of {@code Rating} which will replace existing one
     * @throws ServiceException if {@code DAOException} vas cached
     */

    @Override
    public void update(Long id, Rating rating) throws ServiceException {
        try {
            ratingDAO.update(id, rating);
        } catch (DAOException e) {
            throw  new ServiceException();
        }
    }

    /**
     * Takes average rating of current movie
     * @param rating is instance of{@code Rating}
     * @return instance of {@code Rating} with such {@code id}
     */

    public double getAvgRatingOfMovie(Rating rating){
        double avg = 0;
        if (rating != null) {
             avg = ((double) rating.getSumOfRatings()) / rating.getNumberOfUsers();
        }
        return avg;
    }

    /**
     * Adds user rating to movie
     *
     * @param movieId is movie's identification number
     * @param userMark is user mark
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void addUserRating(Long movieId, int userMark) throws ServiceException{
        try {
            Rating rating = ratingDAO.take(movieId);
            if (rating != null){
                rating.setNumberOfUsers(rating.getNumberOfUsers()+1);
                rating.setSumOfRatings(rating.getSumOfRatings() + userMark);
                ratingDAO.update(movieId, rating);
            } else {
                rating = new Rating();
                rating.setMovieId(movieId);
                rating.setSumOfRatings(userMark);
                rating.setNumberOfUsers(1);
                ratingDAO.add(rating);
            }

        } catch (DAOException e) {
            throw  new ServiceException();
        }
    }

    /**
     * Add user mark to database and add link between
     * user table and movie table
     * @param userId is identification number of user who added mark
     * @param movieId is identification number of movie which was added to the review
     * @param rating is user mark
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public void addRatingWithConnection(Long userId, Long movieId, int rating) throws ServiceException{
        try {
            ratingDAO.addRatingWithConnection(userId, movieId, rating);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Takes user's mark of current movie
     *
     * @param userId is user's identification number
     * @param movieId is movie's identification number
     * @throws ServiceException if {@code DAOException} vas cached
     */

    public double takeUserMark(Long userId, Long movieId) throws ServiceException {
        try {
            return ratingDAO.findRatingByUserMovieId(userId, movieId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
