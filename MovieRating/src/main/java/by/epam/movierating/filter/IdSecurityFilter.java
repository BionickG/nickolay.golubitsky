package by.epam.movierating.filter;

import by.epam.movierating.config.ConfigurationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * @author Nickolay Golubitsky
 *
 * The class {@code IdSecurityFilter} is an implementation of
 * {@code Filter} that realizes all its necessary methods that
 * forms a life-cycle of a filter. Secure from invalid identification
 * numbers.
 */

@WebFilter(urlPatterns = {"/*"})
public class IdSecurityFilter implements Filter {
    private static final String OOPS_PAGE = "path.page.oops";
    private final static String ID = "Id";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Enumeration attributeNames = request.getParameterNames();
        while (attributeNames.hasMoreElements()) {
            String current = (String) attributeNames.nextElement();
            if (current.contains(ID)) {
                String digit = request.getParameter(current);
                if (!isLong(digit)) {
                    RequestDispatcher dispatcher =
                            request.getServletContext().getRequestDispatcher(ConfigurationManager.getProperty(OOPS_PAGE));
                    dispatcher.forward(request, response);
                    return;
                }
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }

    private boolean isLong(String string) {
        return string.matches("\\d*");
    }
}
