package by.epam.movierating.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Random;

/**
 * @author Nickolay Golubitsky
 *
 * The class {@code F5ProtectionFilter} is an implementation of
 * {@code Filter} that realizes all its necessary methods that
 * forms a life-cycle of a filter. Protect forms from updating.
 */

@WebFilter(urlPatterns = {"/controller"})
public class F5ProtectionFilter implements Filter{
    private static final String LAST_GO_TO_COMMAND = "lastCommand";
    private static final String CONTROLLER_COMMAND = "/controller?command=";
    private static final String CHECK_VALUE = "check";
    private static Random rnd = new Random();
    private String check;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        String oldCheck = request.getParameter(CHECK_VALUE);
        String lastCommand = (String) session.getAttribute(LAST_GO_TO_COMMAND);
        if (oldCheck != null && !check.equals(oldCheck)) {
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(CONTROLLER_COMMAND + lastCommand);
            dispatcher.forward(request, response);
            return;
        }
        check = String.valueOf(rnd.nextLong());
        session.setAttribute(CHECK_VALUE, check);
        filterChain.doFilter(request, response);
    }
    @Override
    public void destroy() {

    }
}
