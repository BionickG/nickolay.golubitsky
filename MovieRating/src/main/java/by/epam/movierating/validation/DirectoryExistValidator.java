package by.epam.movierating.validation;

import java.io.File;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code DirectoryExistValidator} class represents the way of validation
 * existing of upload directory
 */

public class DirectoryExistValidator {
    /**
     * Valid existing of upload directory
     *
     * @param appPath   is application path
     * @param directory is upload directory
     * @return saving path
     */
    public static String isDirectoryExist(String appPath, String directory){
        String savePath = appPath + directory;
        File fileSaveDir = new File(savePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }
        return savePath;
    }
}
