package by.epam.movierating.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code RegExpValidator} class represents the way of validation
 * of inputted data, such as first name, last name, login, password and
 * others, using regular expressions. Note, that the same regular expression
 * will be used for the first name and for the last name validation.
 */

public class RegExpValidator {
    private static final String LOGIN_REG_EXP = "[\\w\\d]{4,20}";
    private static final String FIRST_NAME_REG_EXP = "[A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20}";
    private static final String LAST_NAME_REG_EXP = "[A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20}";
    private static final String FIRST_LAST_NAME_REG_EXP = "([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})\\s([A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20})";
    private static final String PASSWORD_REG_EXP = "(^[\\w0-9_-]{6,18}$)";
    private static final String EMAIL_REG_EXP = "([\\w\\._]+@[\\w_]+?\\.[\\w]{2,6})";
    private static final String MOVIE_NAME_REG_EXP = "^.{1,30}";
    private static final String YEAR_REG_EXP = "[12]{1}[90]{1}\\d{2}";
    private static final String DURATION_REG_EXP = "\\d{2,3}";
    private static final String COUNTRY_REG_EXP = "[A-zА-яё ]{2,20}";
    private static final String GENRE_REG_EXP = "[A-zА-яё \\-]{2,20}";

    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_LAST_NAME = "firstLastName";
    private static final String EMAIL = "email";
    private static final String MOVIE_NAME = "name";
    private static final String YEAR = "year";
    private static final String DURATION = "duration";
    private static final String COUNTRY = "country";
    private static final String GENRE = "genre";
    private static final int CHECK_COUNT = 1;

    /**
     * Valid input data
     *
     * @param field is type of input data
     * @param data  is input data
     * @return true if data valid
     */

    public boolean checkData(String field, String data) {
        int count = 0;
        String regexpPattern = null;
        switch (field) {
            case LOGIN:
                regexpPattern = LOGIN_REG_EXP;
                break;
            case PASSWORD:
                regexpPattern = PASSWORD_REG_EXP;
                break;
            case FIRST_NAME:
                regexpPattern = FIRST_NAME_REG_EXP;
                break;
            case LAST_NAME:
                regexpPattern = LAST_NAME_REG_EXP;
                break;
            case FIRST_LAST_NAME:
                regexpPattern = FIRST_LAST_NAME_REG_EXP;
                break;
            case EMAIL:
                regexpPattern = EMAIL_REG_EXP;
                break;
            case MOVIE_NAME:
                regexpPattern = MOVIE_NAME_REG_EXP;
                break;
            case YEAR:
                regexpPattern = YEAR_REG_EXP;
                break;
            case DURATION:
                regexpPattern = DURATION_REG_EXP;
                break;
            case COUNTRY:
                regexpPattern = COUNTRY_REG_EXP;
                break;
            case GENRE:
                regexpPattern = GENRE_REG_EXP;
                break;
        }
        Pattern pattern = Pattern.compile(regexpPattern);
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            count++;
        }
        return count == CHECK_COUNT;
    }
}
