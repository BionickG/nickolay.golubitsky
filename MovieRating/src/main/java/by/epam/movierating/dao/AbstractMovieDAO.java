package by.epam.movierating.dao;

import by.epam.movierating.entity.Movie;
import by.epam.movierating.exception.DAOException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code AbstractMovieDAO} abstract class enumerates all the necessary methods
 * for manipulation with movie's data in database.
 */

public abstract class AbstractMovieDAO implements IDAO<Movie> {

    /**
     * Takes movie by name and year from database
     *
     * @param name is name of movie
     * @param year is year when movie was released
     * @return instance of {@code Movie} with such {@code name} and {@code year}
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public abstract Movie takeByNameAndYear(String name, int year) throws DAOException;

    /**
     * Takes movies only by name from database
     * @param name is name of movie
     * @return list of {@code Movie} with such {@code name}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract List<Movie> takeByName(String name) throws DAOException;

    /**
     * Takes movies by actor who starred in the movie
     * @param id is identification number of actor
     *           who starred in the movie
     * @return list of {@code Movie} with actor with {@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract List<Movie> takeMoviesByActorId(Long id) throws DAOException;

    /**
     * Takes movies by the director who shot the film
     * @param id is director's identification number
     * @return list of {@code Movie} shooting by director
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract List<Movie> takeMoviesByDirectorId(Long id) throws DAOException;

    /**
     * Takes movies by the country where the film was shot
     * @param countryId is country's identification number
     * @return list of {@code Movie} with such country
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract List<Movie> takeMoviesByCountryId(Long countryId) throws DAOException;

    /**
     * Add link between movie table and actor table
     * that means the actor starred in the film
     * @param movieId is identification number of actor starred in the film
     * @param actorId is identification number of film where actor was starred
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract void addConnectionWithActorTable(Long movieId, Long actorId) throws DAOException;

    /**
     * Delete link between movie table and actor table
     * that means  delete actor from the staff of film
     *
     * @param movieId is identification number of actor starred in the film
     * @param actorId is identification number of film where actor was starred
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    public abstract void deleteConnectionWithActorTable(Long movieId, Long actorId) throws DAOException;

    /**
     * Takes all movies of a particular genre
     * @param genreId is identification number of genre
     * @return list of {@code Movie} witn such genre
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract List<Movie> takeAllMoviesByGenre(Long genreId) throws DAOException;

    /**
     * Add link between movie table and genre table
     * that means add genre to the current film
     * @param movieId is identification number of movie which will be added
     * @param genreId is identification number of genre which should be added
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract void addConnectionWithGenreTable(Long movieId, Long genreId) throws DAOException;

    /**
     * Delete link between movie table and genre table
     * that means delete genre from the movie
     * @param movieId is identification number of movie
     * @param genreId is identification number of genre wich should be deleted
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract void deleteConnectionWithGenreTable(Long movieId, Long genreId) throws DAOException;

    /**
     * Takes last added to database movies
     * @return list of last added {@code Movie}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract List<Movie> takeLastAdded() throws DAOException;
}
