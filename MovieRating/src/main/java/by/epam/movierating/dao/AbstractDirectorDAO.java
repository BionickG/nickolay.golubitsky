package by.epam.movierating.dao;

import by.epam.movierating.entity.Director;
import by.epam.movierating.exception.DAOException;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code AbstractDirectorDAO} abstract class enumerates all the necessary methods
 * for manipulation with director's data in database.
 */

public abstract class AbstractDirectorDAO implements IDAO<Director> {

    /**
     * It's cap for update method from {@code IDAO<T>} interface
     *
     * @param id       is director's identification number
     * @param director is instance of {@code Director}
     * @throws DAOException in the case of function call
     */

    @Override
    public void update(Long id, Director director) throws DAOException {
        throw new DAOException("No such function");
    }

    /**
     * Takes actor from database by director's full name
     * @param firstName is first name of director
     * @param lastName is last name of director
     * @return instance of {@code Director} satisfying the condition
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    public abstract Director takeDirectorByName(String firstName, String lastName) throws DAOException;
}
