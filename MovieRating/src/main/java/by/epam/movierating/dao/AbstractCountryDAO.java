package by.epam.movierating.dao;

import by.epam.movierating.entity.Country;
import by.epam.movierating.exception.DAOException;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code AbstractCountryDAO} abstract class enumerates all the necessary methods
 * for manipulation with country's data in database.
 */

public abstract class AbstractCountryDAO implements IDAO<Country> {

    /**
     * It's cap for update method from {@code IDAO<T>} interface
     *
     * @param id      is country identification number
     * @param country is instance of {@code Country}
     * @throws DAOException in the case of function call
     */

    @Override
    public void update(Long id, Country country) throws DAOException {
        throw new DAOException("No such function");
    }

    /**
     * Takes instance of {@code Country} by country's name
     * from the database
     * @param name is country name
     * @return instance of {@code Country} satisfying the condition
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract Country takeByName(String name) throws DAOException;
}
