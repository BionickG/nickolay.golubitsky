package by.epam.movierating.dao;

import by.epam.movierating.entity.User;
import by.epam.movierating.exception.DAOException;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code AbstractUserDAO} abstract class enumerates all the necessary methods
 * for manipulation user's data in database.
 */

public abstract class AbstractUserDAO implements IDAO<User> {
    /**
     * This method serve for ban user if he unbanned
     * and unban if he is banned
     *
     * @param id  is user's identification number
     * @param ban is boolean value of
     *            operation(true-ban, false-unban)
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    public abstract void ban(Long id, boolean ban) throws DAOException;

    /**
     * Defines user by user login and password
     * @param login is user's login string
     * @param password is user encrypted password
     * @return instanse of{@code User} with such {@code login} and
     * {@password} if he exists
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract User defineUser(String login, String password) throws DAOException;

    /**
     * Add avatar image to user profile
     * @param userId user's identification number
     * @param avatar is url string with image path
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract void addAvatar(Long userId, String avatar) throws DAOException;

    /**
     *  Change user's password in database
     * @param userId is identification number of user who will be changed password
     * @param newPassword is new user's password
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract void changePassword(Long userId, String newPassword) throws DAOException;

    /**
     * Logic operation required for determine user
     * (is user exists in the system)
     *
     * @param login is user's login string
     * @return return true if user exists
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    public abstract boolean takeUserByLogin(String login) throws DAOException;

    /**
     * It's cap for update method from {@code IDAO<T>} interface
     * @param id is user identification number
     * @param user is instance of {@code User}
     * @throws DAOException if this method called
     */
    @Override
    public void update(Long id, User user) throws DAOException {
        throw new DAOException("No such command");
    }
}
