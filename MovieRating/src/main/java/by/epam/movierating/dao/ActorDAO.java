package by.epam.movierating.dao;

import by.epam.movierating.connection.ConnectionPool;
import by.epam.movierating.entity.Actor;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.DatabaseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code ActorDAO} class implements all the necessary methods
 * that were inherited from {@code AbstractMovieDAO} class and {@code IDAO}
 * interface for manipulation with movie's data in database.
 */

public class ActorDAO extends AbstractActorDAO {
    private final static String FIRST_NAME = "first_name";
    private final static String LAST_NAME = "last_name";
    private final static String ID = "actor_id";

    private static final String SQL_FIND_ALL = "SELECT actor_id, first_name, last_name " +
            "FROM movie_rating.actor ORDER BY last_name";
    private static final String SQL_TAKE_ACTOR = "SELECT actor_id, first_name, last_name " +
            "FROM movie_rating.actor  WHERE actor_id = ?";
    private static final String SQL_TAKE_ACTOR_BY_NAME = "SELECT actor_id, first_name, last_name " +
            "FROM movie_rating.actor  WHERE first_name = ? AND last_name=?";
    private static final String SQL_ADD_ACTOR = "INSERT INTO movie_rating.actor (first_name, last_name)" +
            " VALUES (?, ?)";
    private static final String SQL_DELETE_ACTOR = "DELETE FROM movie_rating.actor WHERE actor_id=?";

    private final static String SQL_TAKE_ALL_ACTORS_BY_MOVIE = "SELECT actor_id, first_name, last_name " +
            "FROM movie_rating.actor WHERE actor_id IN " +
            "(SELECT actor_id FROM movie_rating.movie_actor WHERE movie_id = ?)";

    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Adds instance of {@code Actor} to database
     *
     * @param actor is instance which will be added
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public void add(Actor actor) throws DAOException {

        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_ACTOR)) {
            statement.setString(1, actor.getFirstName());
            statement.setString(2, actor.getLastName());
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during taking actor", e);
        }
    }

    /**
     * Takes actor from database by actor identification
     * number
     * @param id is actor's identification number
     * @return instance of {@code Actor} with such {@id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Actor take(Long id) throws DAOException {
        Actor actor = null;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_ACTOR)) {
            statement.setLong(1, id);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                actor = createFromResultSet(set);
            }
            return actor;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during taking actor", e);
        }
    }

    /** Deletes actor from database by actor
     * identification number
     * @param id is identification number of instance which will be deleted
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void delete(Long id) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_ACTOR)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during actor deleting", e);
        }
    }

    /**
     * Takes all actors from database
     * @return list of instances {@code Actor}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<Actor> takeAll() throws DAOException {
        List<Actor> actors = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                actors.add(createFromResultSet(resultSet));
            }
            return actors;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during taking all actors", e);
        }
    }

    /**
     * Takes actor from database by actor's full name
     * @param firstName is first name of actor
     * @param lastName is last name of actor
     * @return instance of {@code Actor} satisfying the condition
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Actor takeActorByName(String firstName, String lastName) throws DAOException {
        Actor actor = null;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_ACTOR_BY_NAME)) {
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                actor = createFromResultSet(set);
            }
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during taking actor by name", e);
        }
        return actor;
    }

    /**
     * Takes actors from database which starred in movie with such
     * movie's id{@code id}
     * @param id is movie ID
     * @return list of actors which starred in current movie
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<Actor> takeActorsByMovieId(Long id) throws DAOException {
        List<Actor> actors = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_ALL_ACTORS_BY_MOVIE)) {
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Actor actor = new Actor();
                actor.setActorId(resultSet.getLong(1));
                actor.setFirstName(resultSet.getString(2));
                actor.setLastName(resultSet.getString(3));
                actors.add(actor);
            }
            return actors;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during taking actor by ID", e);
        }
    }

    /**
     * Creates instance of {@code Actor} from  resultSet
     * @param resultSet is instance of {@code ResultSet}
     * @return already filled instance of {@code Actor}
     * @throws SQLException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    private Actor createFromResultSet(ResultSet resultSet) throws SQLException {
        Actor actor = new Actor();
        actor.setActorId(resultSet.getLong(ID));
        actor.setFirstName(resultSet.getString(FIRST_NAME));
        actor.setLastName(resultSet.getString(LAST_NAME));
        return actor;
    }

}
