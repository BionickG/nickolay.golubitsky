package by.epam.movierating.dao;

import by.epam.movierating.connection.ConnectionPool;
import by.epam.movierating.entity.Director;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.DatabaseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code DirectorDAO} class implements all the necessary methods
 * that were inherited from {@code AbstractMovieDAO} class and {@code IDAO}
 * interface for manipulation with movie's data in database.
 */

public class DirectorDAO extends AbstractDirectorDAO {

    private final static String FIRST_NAME = "first_name";
    private final static String LAST_NAME = "last_name";
    private final static String ID = "director_id";

    private static final String SQL_FIND_ALL = "SELECT director_id, first_name, last_name " +
            "FROM movie_rating.director";
    private static final String SQL_TAKE_DIRECTOR = "SELECT director_id, first_name, last_name" +
            "FROM movie_rating.director  WHERE director_id = ?";
    private static final String SQL_TAKE_DIRECTOR_BY_NAME = "SELECT director_id, first_name, last_name " +
            "FROM movie_rating.director  WHERE first_name=? AND last_name=?";
    private static final String SQL_ADD_DIRECTOR = "INSERT INTO movie_rating.director (first_name, last_name)" +
            " VALUES (?, ?)";
    private static final String SQL_DELETE_DIRECTOR = "DELETE FROM movie_rating.actor WHERE actor_id=?";

    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Adds instance of {@code Director} to database
     *
     * @param director is instance which will be added
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public void add(Director director) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_DIRECTOR)){
            statement.setString(1, director.getFirstName());
            statement.setString(2, director.getLastName());
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during director adding", e);
        }
    }

    /**
     * Takes director from database by director identification number
     * @param id is directors's identification number
     * @return instance of {@codeDirector} with such {@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Director take(Long id) throws DAOException {
        Director director = null;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_DIRECTOR)){
            statement.setLong(1, id);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                director = createFromResultSet(set);
            }
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during director taking", e);
        }
        return director;
    }

    /** Deletes actor from database by director
     * identification number
     * @param id is identification number of instance which will be deleted
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void delete(Long id) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_DIRECTOR)){
            statement.setLong(1, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during director deleting", e);
        }
    }

    /**
     * Takes all directors from database
     * @return list of instances {@code Director}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<Director> takeAll() throws DAOException {
        List<Director> directors = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                directors.add(createFromResultSet(resultSet));
            }
            return directors;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all directors taking", e);
        }
    }

    /**
     * Takes actor from database by actor's full name
     * @param firstName is first name of director
     * @param lastName is last name of director
     * @return instance of {@code Director} satisfying the condition
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Director takeDirectorByName(String firstName, String lastName) throws DAOException {
        Director director = null;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_DIRECTOR_BY_NAME)){
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                director = createFromResultSet(set);
            }
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during director taking by name", e);
        }
        return director;
    }

    /**
     * Creates instance of {@code Director} from  resultSet
     * @param resultSet is instance of {@code ResultSet}
     * @return already filled instance of {@code Director}
     * @throws SQLException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    private Director createFromResultSet(ResultSet resultSet) throws SQLException {
        Director director = new Director();
        director.setDirectorId(resultSet.getLong(ID));
        director.setFirstName(resultSet.getString(FIRST_NAME));
        director.setLastName(resultSet.getString(LAST_NAME));
        return director;
    }
}
