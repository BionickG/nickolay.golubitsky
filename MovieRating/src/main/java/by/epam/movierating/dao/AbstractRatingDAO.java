package by.epam.movierating.dao;

import by.epam.movierating.entity.Rating;
import by.epam.movierating.exception.DAOException;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code AbstractMovieDAO} abstract class enumerates all the necessary methods
 * for manipulation with movie's data in database.
 */

public abstract class AbstractRatingDAO implements IDAO<Rating> {
    /**
     * Add user mark to database and add link between
     * user table and movie table
     *
     * @param userId  is identification number of user who added mark
     * @param movieId is identification number of movie which was added to the review
     * @param rating  is user mark
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    public abstract void addRatingWithConnection(Long userId, Long movieId, int rating) throws DAOException;

    /**
     * Finds user rating by {@code userId} and{@code movieId}
     * @param userId is identification number of user
     * @param movieId is identification number of movie
     * @return rating of movie
     * @throws DAOException
     */
    public abstract double findRatingByUserMovieId(Long userId, Long movieId) throws DAOException;
}
