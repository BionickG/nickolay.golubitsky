package by.epam.movierating.dao;

import by.epam.movierating.entity.Actor;
import by.epam.movierating.exception.DAOException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code AbstractActorDAO} abstract class enumerates all the necessary methods
 * for manipulation with actors's data in database.
 */

public abstract class AbstractActorDAO implements IDAO<Actor> {

    /**
     * It's cap for update method from {@code IDAO<T>} interface
     *
     * @param id    is actor identification number
     * @param actor is instance of {@code Actor}
     * @throws DAOException in the case of function call
     */

    @Override
    public void update(Long id, Actor actor) throws DAOException {
        throw new DAOException("No such function");
    }

    /**
     * Takes actor from database by actor's full name
     * @param firstName is first name of actor
     * @param lastName is last name of actor
     * @return instance of {@code Actor} satisfying the condition
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract Actor takeActorByName(String firstName, String lastName) throws DAOException;

    /**
     * Takes actors from database which starred in movie with such
     * movie's id{@code id}
     * @param id is movie ID
     * @return list of actors which starred in current movie
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    public abstract List<Actor> takeActorsByMovieId(Long id) throws DAOException;
}
