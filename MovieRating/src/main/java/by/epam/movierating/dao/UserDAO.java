package by.epam.movierating.dao;

import by.epam.movierating.connection.ConnectionPool;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.DatabaseException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Nickolay Golubitsky
 *
 * The {@code UserDAO} class implements all the necessary methods
 * that were inherited from {@code AbstractMovieDAO} class and {@code IDAO}
 * interface for manipulation with movie's data in database.
 */

public class UserDAO extends AbstractUserDAO{

    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String PASSWORD = "password";
    private static final String AVATAR = "avatar";
    private static final String BAN = "is_banned";
    private static final String EMAIL = "email";
    private static final String LOGIN = "login";
    private static final String ID = "user_id";
    private static final String ROLE = "role";

    private static final String SQL_FIND_ALL = "SELECT user_id, login, password, first_name, last_name, email, role, is_banned, avatar " +
            "FROM movie_rating.user JOIN movie_rating.role ON user.role_id=role.role_id";

    private static final String SQL_FIND_USER = "SELECT user_id, login, password, first_name, last_name, email, role, is_banned, avatar " +
            "FROM movie_rating.user JOIN movie_rating.role ON user.role_id = role.role_id WHERE login=? AND password=?";

    private static final String SQL_FIND_USER_BY_LOGIN = "SELECT user_id, login, password, first_name, last_name, email, role, is_banned, avatar " +
            "FROM movie_rating.user JOIN movie_rating.role ON user.role_id = role.role_id WHERE login=?";

    private static final String SQL_ADD_USER = "INSERT INTO movie_rating.user (first_name, last_name, " +
            "email, login, password, role_id, is_banned, avatar) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String SQL_TAKE_USER = "SELECT user_id, first_name, login, password, last_name, email, role, is_banned, avatar " +
            "FROM movie_rating.user JOIN movie_rating.role ON user.role_id = role.role_id WHERE user_id = ?";

    private static final String SQL_DELETE_USER = "DELETE FROM movie_rating.user WHERE user_id=?";

    private static final String SQL_UPDATE_USER = "UPDATE movie_rating.user SET is_banned = ? WHERE user_id = ?";

    private static final String SQL_ADD_AVATAR = "UPDATE  movie_rating.user SET avatar = ? WHERE user_id = ?";

    private static final String SQL_CHANGE_PASSWORD = "UPDATE  movie_rating.user SET password = ? WHERE user_id = ?";

    /**
     * Adds instance of {@code User} to database
     *
     * @param user is instance which will be added
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public void add(User user) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_USER)){
            fillInStatement(user, statement);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user adding", e);
        }
    }

    /**
     * Takes rating from database by user's identification
     * number
     * @param id is user's identification number
     * @return instance of {@code Rating} with such {@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public User take(Long id) throws DAOException {
        User user = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_USER)){
            statement.setLong(1, id);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                user = fillInUser(set);
            }
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user taking", e);
        }
        return user;
    }

    /**
     * This method serve for ban user if he unbanned
     * and unban if he is banned
     * @param id is user's identification number
     * @param ban is boolean value of
     *            operation(true-ban, false-unban)
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void ban(Long id, boolean ban) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER)){
            statement.setBoolean(1, ban);
            statement.setLong(2, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user banning", e);
        }
    }

    /**
     * Deletes user from database by user's identification number
     * @param id is identification number of instance which will be deleted
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void delete(Long id) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_USER)){
            statement.setLong(1, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user deleting", e);
        }
    }

    /**
     * Defines user by user login and password
     * @param login is user's login string
     * @param password is user encrypted password
     * @return instanse of{@code User} wuth such {@code login} and
     * {@code password} if he exists
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public User defineUser (String login, String password) throws DAOException{
        User user = null;
        ResultSet resultSet;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_USER)){
            statement.setString(1, login);
            statement.setString(2, password);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                user = fillInUser(resultSet);
            }
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user defining", e);
        }
        return user;
    }

    /**
     * Takes all users from database
     * @return list of instances {@code User}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<User> takeAll() throws DAOException {
        List<User> users = new ArrayList<>();
        ResultSet resultSet;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                users.add(fillInUser(resultSet));
            }
            return users;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all users taking", e);
        }
    }

    /**
     * Add avatar image to user profile
     * @param userId user's identification number
     * @param avatar is url string with image path
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void addAvatar(Long userId, String avatar) throws DAOException{
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_AVATAR)){
            statement.setString(1, avatar);
            statement.setLong(2, userId);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user's avatar adding", e);
        }
    }

    /**
     * Change user's password in database
     * @param userId is identification number of user who will be changed password
     * @param newPassword is new user's password
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void changePassword(Long userId, String newPassword) throws DAOException{
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_CHANGE_PASSWORD)){
            statement.setString(1, newPassword);
            statement.setLong(2, userId);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user's password changing", e);
        }
    }

    /**
     * Logic operation required for determine user
     * (is user exists in the system)
     * @param login is user's login string
     * @return return true if user exists
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public boolean takeUserByLogin(String login) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_USER_BY_LOGIN)){
            statement.setString(1, login);
            ResultSet set = statement.executeQuery();
            return set.next();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during user taking", e);
        }
    }

    /**
     * Creates instance of {@code User} from resultSet
     * @param resultSet is instance of {@code ResultSet}
     * @return already filled instance of {@code User}
     * @throws SQLException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    private User fillInUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setUserId(resultSet.getLong(ID));
        user.setFirstName(resultSet.getString(FIRST_NAME));
        user.setLastName(resultSet.getString(LAST_NAME));
        user.setLogin(resultSet.getString(LOGIN));
        user.setPassword(resultSet.getString(PASSWORD));
        user.setEmail(resultSet.getString(EMAIL));
        user.setBanned(resultSet.getBoolean(BAN));
        Role role = Role.valueOf(resultSet.getString(ROLE));
        user.setAvatar(resultSet.getString(AVATAR));
        user.setRole(role);
        return user;
    }

    /**
     * Fills in statement using instance of {@code User} data
     * @param user instance of {@code User} which data will be used
     * @param statement instance of {@code PrePreparedStatement} which will be fill in
     * @return instance of {@code PreparedStatement}
     * @throws SQLException
     */

    private Statement fillInStatement(User user, PreparedStatement statement) throws SQLException{
        statement.setString(1, user.getFirstName());
        statement.setString(2, user.getLastName());
        statement.setString(3, user.getEmail());
        statement.setString(4, user.getLogin());
        statement.setString(5, user.getPassword());
        statement.setLong(6, user.getRole().getRoleId());
        statement.setBoolean(7, user.isBanned());
        statement.setString(8, user.getAvatar());
        return statement;
    }

}
