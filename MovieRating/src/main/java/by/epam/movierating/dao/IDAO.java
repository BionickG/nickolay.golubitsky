package by.epam.movierating.dao;

import by.epam.movierating.exception.DAOException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code IDAO} abstract class enumerates all the necessary methods
 * for manipulation entity's data in database.
 */

public interface IDAO<T> {
    /**
     * Method takes all instance of current entity
     *
     * @return list of entity
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    List<T> takeAll() throws DAOException;

    /**
     * Method takes instance of entity by instance's
     * identification number
     * @param id is instance's identification number
     * @return current instance of entity with such{@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    T take(Long id) throws DAOException;

    /**
     * Method adds current instance to database
     * @param t is instance which will be add
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    void add(T t) throws DAOException;

    /**
     * Method deletes instance of entity form database
     * @param id is instance which will be delete
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    void delete(Long id) throws DAOException;

    /**
     * Method takes new instance of entity and replace other one
     * @param id is instance's identification number
     * @param t new instance which will replace existing one
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    void update(Long id, T t) throws DAOException;

}
