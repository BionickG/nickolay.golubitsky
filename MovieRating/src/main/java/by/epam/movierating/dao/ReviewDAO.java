package by.epam.movierating.dao;

import by.epam.movierating.connection.ConnectionPool;
import by.epam.movierating.entity.Review;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.DatabaseException;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code ReviewDAO} class implements all the necessary methods
 * that were inherited from {@code AbstractMovieDAO} class and {@code IDAO}
 * interface for manipulation with movie's data in database.
 */


public class ReviewDAO extends AbstractReviewDAO {

    private static final String USER_NAME = "user.first_name";
    private static final String REVIEW_ID = "review_id";
    private static final String AVATAR = "user.avatar";
    private static final String MOVIE_ID = "movie_id";
    private static final String DATE = "review_date";
    private static final String USER_ID = "user_id";
    private static final String REVIEW = "review";
    private static final String MARK = "mark";


    private static final String SQL_FIND_ALL = "SELECT review_id, review, movie_id, review.user_id, review_date, mark" +
            " user.first_name, user.avatar FROM movie_rating.review " +
            "JOIN movie_rating.user ON review.user_id = user.user_id ORDER BY review_id DESC ";

    private static final String SQL_FIND_REVIEW_BY_MOVIE_ID = "SELECT review_id, review, movie_id, review.user_id, mark, " +
            "review_date, user.first_name, user.avatar FROM movie_rating.review " +
            "JOIN movie_rating.user ON review.user_id = user.user_id WHERE movie_id=? ORDER BY review_id DESC";

    private static final String SQL_TAKE_ALL_MARKS = "SELECT SUM(mark) FROM movie_rating.review  WHERE user_id=?";

    private static final String SQL_DELETE_REVIEW = "DELETE FROM movie_rating.review WHERE review_id = ?";
    private static final String SQL_ADD_REVIEW = "INSERT INTO movie_rating.review(review, movie_id, user_id," +
            "review_date, mark) " +
            "VALUE (?,?,?,?,?)";
    private static final String SQL_FIND_REVIEW = "SELECT review_id, review, movie_id, review.user_id, user.first_name, mark " +
            "review_date FROM movie_rating.review JOIN movie_rating.user ON review.user_id = user.user_id " +
            "WHERE review_id = ?";
    private static final String SQL_UPDATE_REVIEW = "UPDATE movie_rating.review SET review = ? WHERE review_id = ?";
    private static final String SQL_CHANGE_MARK = "UPDATE movie_rating.review SET mark = mark+? WHERE review_id = ?";

    private final static String SQL_ADD_CONNECTION_WITH_USER_TABLE = "INSERT INTO movie_rating.user_review(user_id, review_id) " +
            "VALUES (?, ?)";

    private final static String SQL_TAKE_CONNECTION_WITH_USER_TABLE = "SELECT user_id, review_id FROM movie_rating.user_review " +
            "WHERE user_id = ? AND review_id = ?";

    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Takes all reviews from database
     *
     * @return list of instances {@code Review}
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Review> takeAll() throws DAOException {
        List<Review> reviews = new LinkedList<>();
        ResultSet resultSet;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                reviews.add(fillInReview(resultSet));
            }
            return reviews;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all review taking", e);
        }
    }

    /**
     * Takes all reviews of current movie
     * @param movieId is identification number of movie
     *                which reviews we searching for
     * @return list of {@code Review} of current movie
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    public List<Review> takeByMovieId(Long movieId) throws DAOException {
        List<Review> reviews = new LinkedList<>();
        ResultSet resultSet;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_REVIEW_BY_MOVIE_ID)) {
            statement.setLong(1, movieId);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                reviews.add(fillInReview(resultSet));
            }
            return reviews;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during review taking by movie ID", e);
        }
    }

    /**
     * Takes all marks of user's reviews
     * @param userId is identification number of user
     * @return summ of all marks
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    public int takeAllMarks(Long userId) throws DAOException {
        ResultSet resultSet;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_ALL_MARKS)) {
            statement.setLong(1, userId);
            resultSet = statement.executeQuery();
            int rating = 0;
            while (resultSet.next()){
                rating = resultSet.getInt(1);
            }
            return rating;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all marks of review taking", e);
        }
    }

    /**
     * Takes rating from database by review's identification
     * number
     * @param id is review's identification number
     * @return instance of {@code Review} with such {@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Review take(Long id) throws DAOException {
        Review review = new Review();
        ResultSet resultSet;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_REVIEW)) {
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                review = fillInReview(resultSet);
            }
            return review;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during review taking", e);
        }
    }

    /**
     *  Adds instance of {@code Review} to database
     * @param review is instance which will be added
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void add(Review review) throws DAOException {

        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_REVIEW)) {
            statement.setString(1, review.getReview());
            statement.setLong(2, review.getMovieId());
            statement.setLong(3, review.getUser().getUserId());
            statement.setDate(4, Date.valueOf(review.getReviewDate()));
            statement.setInt(5, review.getMark());
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during review adding", e);
        }
    }

    /**
     * Deletes rating from database by delete's identification number
     * @param id is identification number of instance which will be deleted
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void delete(Long id) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_REVIEW)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during review deleting", e);
        }
    }

    /**
     * Method takes new instance of {@code Review} and replace other one
     * @param id is review's identification number
     * @param review new instance of {@code Review} which will replace existing one
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void update(Long id, Review review) throws DAOException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_REVIEW)) {
            statement.setString(1, review.getReview());
            statement.setLong(2, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during review updating", e);
        }

    }

    /**
     * Add mark to the existing mark of review
     * @param reviewId identification number of review wich fill with a mark
     * @param mark is mark which will be added
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void changeMark (Long reviewId, int mark) throws DAOException{
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_CHANGE_MARK)) {
            statement.setInt(1, mark);
            statement.setLong(2, reviewId);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during review's mark changing", e);
        }
    }

    /**
     * Add link between user table and review table
     * that means user add his own review
     * @param userId is identification number of user who adding review
     * @param reviewId identification number of review which will be marked
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void addConnectionWithUser(Long userId, Long reviewId) throws DAOException{
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_CONNECTION_WITH_USER_TABLE)) {
            statement.setLong(1, userId);
            statement.setLong(2, reviewId);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during" +
                    " connection with user table adding", e);
        }
    }

    /**
     * Method checks did user already marked movie
     * @param userId is user's identification number
     * @param reviewId is review's identification number
     * @return boolean value which marks user's choice
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public boolean isConnectionExist(Long userId, Long reviewId) throws DAOException {
        ResultSet resultSet;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_CONNECTION_WITH_USER_TABLE)) {
            statement.setLong(1,userId);
            statement.setLong(2, reviewId);
            resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during connection with user table checking", e);
        }
    }

    /**
     * Creates instance of {@code Review} from  resultSet
     * @param resultSet is instance of {@code ResultSet}
     * @return already filled instance of {@code Review}
     * @throws SQLException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    private Review fillInReview(ResultSet resultSet) throws SQLException {
        Review review = new Review();
        review.setReviewId(resultSet.getLong(REVIEW_ID));
        User user = new User();
        user.setUserId(resultSet.getLong(USER_ID));
        user.setFirstName(resultSet.getString(USER_NAME));
        user.setAvatar(resultSet.getString(AVATAR));
        review.setUser(user);
        review.setMovieId(resultSet.getLong(MOVIE_ID));
        review.setReview(resultSet.getString(REVIEW));
        review.setReviewDate(resultSet.getDate(DATE).toLocalDate());
        review.setMark(resultSet.getInt(MARK));
        return review;
    }
}
