package by.epam.movierating.dao;

import by.epam.movierating.connection.ConnectionPool;
import by.epam.movierating.entity.Country;
import by.epam.movierating.entity.Director;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.DatabaseException;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code MovieDAO} class implements all the necessary methods
 * that were inherited from {@code AbstractMovieDAO} class and {@code IDAO}
 * interface for manipulation with movie's data in database.
 */

public class MovieDAO extends AbstractMovieDAO {

    private final static String DIRECTOR_FIRST_NAME = "director.first_name";
    private final static String DIRECTOR_LAST_NAME = "director.last_name";
    private final static String DIRECTOR_ID = "movie.director_id";
    private final static String DESCRIPTION = "description";
    private final static String MOVIE_NAME = "movie_name";
    private final static String DATE_ADDED = "date_added";
    private final static String TRAILER = "trailer_link";
    private final static String DURATION = "duration";
    private final static String COUNTRY = "country";
    private final static String ID = "movie_id";
    private final static String COVER = "cover";
    private final static String YEAR = "year";

    private final static String SQL_FIND_ALL = "SELECT movie_id, movie_name, year, description, country, cover, " +
            "trailer_link, movie.director_id, director.first_name, director.last_name, duration, date_added " +
            "FROM movie_rating.movie " +
            "JOIN movie_rating.country ON country.country_id = movie.country_id " +
            "JOIN movie_rating.director ON director.director_id = movie.director_id";

    private final static String SQL_TAKE_LAST_TEN_ADDED = "SELECT movie_id, movie_name, year, description, country, cover, " +
            "trailer_link, movie.director_id, director.first_name, director.last_name, duration, date_added " +
            "FROM movie_rating.movie " +
            "JOIN movie_rating.country ON country.country_id = movie.country_id " +
            "JOIN movie_rating.director ON director.director_id = movie.director_id " +
            "ORDER BY date_added DESC";

    private final static String SQL_TAKE_MOVIE = "SELECT movie_id, movie_name, year, description, country, cover, " +
            "trailer_link, movie.director_id, director.first_name, director.last_name, duration, date_added " +
            "FROM movie_rating.movie " +
            "JOIN movie_rating.country ON country.country_id = movie.country_id " +
            "JOIN movie_rating.director ON director.director_id = movie.director_id WHERE movie_id=?";

    private final static String SQL_ADD_MOVIE = "INSERT INTO movie_rating.movie(movie_name, year, description, " +
            "country_id, cover, director_id, duration, trailer_link, date_added) VALUES (?,?,?,?,?,?,?,?,?)";

    private final static String SQL_DELETE_MOVIE = "DELETE FROM movie_rating.movie WHERE movie_id = ?";

    private final static String SQL_UPDATE_MOVIE = "UPDATE movie_rating.movie SET  movie_name = ?, " +
            "year = ?, description = ?, country_id = ?, cover = ?, trailer_link = ?, movie.director_id = ?, " +
            "duration = ?  WHERE movie_id = ?";

    private final static String SQL_TAKE_MOVIE_BY_NAME_YEAR = "SELECT movie_id, movie_name, year, description, country, cover, " +
            "trailer_link, movie.director_id, director.first_name, director.last_name, duration, date_added " +
            "FROM movie_rating.movie " +
            "JOIN movie_rating.country ON country.country_id = movie.country_id " +
            "JOIN movie_rating.director ON director.director_id = movie.director_id WHERE movie_name=? AND year = ?";

    private final static String SQL_TAKE_MOVIE_BY_NAME = "SELECT movie_id, movie_name, year, description, country, cover, " +
            "trailer_link, movie.director_id, director.first_name, director.last_name, duration, date_added " +
            "FROM movie_rating.movie " +
            "JOIN movie_rating.country ON country.country_id = movie.country_id " +
            "JOIN movie_rating.director ON director.director_id = movie.director_id WHERE movie_name LIKE ?";

    private final static String SQL_GET_ALL_MOVIES_BY_ACTOR = "SELECT movie_id, movie_name, year, description, country, cover, " +
            "trailer_link, movie.director_id, director.first_name, director.last_name, duration, date_added " +
            "FROM movie_rating.movie " +
            "JOIN movie_rating.country ON country.country_id = movie.country_id " +
            "JOIN movie_rating.director ON director.director_id = movie.director_id " +
            "WHERE movie_id IN (SELECT movie_id FROM movie_rating.movie_actor WHERE actor_id = ?)";

    private final static String SQL_GET_ALL_MOVIES_BY_DIRECTOR = "SELECT movie_id, movie_name, year, description, country, cover, " +
            "trailer_link, movie.director_id, director.first_name, director.last_name, duration, date_added " +
            "FROM movie_rating.movie " +
            "JOIN movie_rating.country ON country.country_id = movie.country_id " +
            "JOIN movie_rating.director ON director.director_id = movie.director_id " +
            "WHERE movie.director_id = ?";

    private final static String SQL_GET_ALL_MOVIES_BY_COUNTRY = "SELECT movie_id, movie_name, year, description, country, cover, " +
            "trailer_link, movie.director_id, director.first_name, director.last_name, duration, date_added " +
            "FROM movie_rating.movie " +
            "JOIN movie_rating.country ON country.country_id = movie.country_id " +
            "JOIN movie_rating.director ON director.director_id = movie.director_id " +
            "WHERE movie.country_id = ?";

    private final static String SQL_ADD_CONNECTION_WITH_ACTOR_TABLE = "INSERT INTO movie_rating.movie_actor (movie_id, actor_id) " +
            "VALUES (?, ?)";

    private final static String SQL_DELETE_CONNECTION_WITH_ACTOR_TABLE = "DELETE FROM movie_rating.movie_actor WHERE " +
            "movie_id = ? AND actor_id = ?";

    private final static String SQL_TAKE_ALL_FILMS_BY_GENRE = "SELECT movie_id, movie_name, year, description, country, cover, " +
            "trailer_link, movie.director_id, director.first_name, director.last_name, duration, date_added " +
            "FROM movie_rating.movie " +
            "JOIN movie_rating.country ON country.country_id = movie.country_id " +
            "JOIN movie_rating.director ON director.director_id = movie.director_id " +
            "WHERE movie_id IN (SELECT movie_id FROM movie_rating.movie_genre WHERE genre_id = ?)";

    private final static String SQL_ADD_CONNECTION_WITH_GENRE_TABLE = "INSERT INTO movie_rating.movie_genre(movie_id, genre_id) " +
            "VALUES (?, ?)";

    private final static String SQL_DELETE_CONNECTION_WITH_GENRE_TABLE = "DELETE FROM movie_rating.movie_genre WHERE " +
            "movie_id = ? AND genre_id = ?";

    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Takes all movies from database
     *
     * @return list of instances {@code Movies}
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Movie> takeAll() throws DAOException {
        List<Movie> movies = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                movies.add(createMovie(resultSet));
            }
            return movies;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all movies taking", e);
        }
    }

    /**
     * Takes movie from database by movie's identification
     * number
     * @param id is movie's identification number
     * @return instance of {@code Movie} with such {@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Movie take(Long id) throws DAOException {
        Movie movie = null;
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_MOVIE)) {
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                movie = createMovie(resultSet);
            }
            return movie;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movie taking", e);
        }
    }

    /**
     * Takes movie by name and year from database
     * @param name is name of movie
     * @param year is year when movie was released
     * @return instance of {@code Movie} with such {@code name} and {@code year}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Movie takeByNameAndYear(String name, int year) throws DAOException {
        Movie movie = null;
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_MOVIE_BY_NAME_YEAR)) {
            statement.setString(1, name);
            statement.setInt(2, year);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                movie = createMovie(resultSet);
            }
            return movie;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movie taking by name and year", e);
        }
    }

    /**
     * Takes movies only by name from database
     * @param name is name of movie
     * @return list of {@code Movie} with such {@code name}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<Movie> takeByName(String name) throws DAOException {
        ResultSet resultSet;
        List<Movie> movies = new LinkedList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_MOVIE_BY_NAME)) {
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                movies.add(createMovie(resultSet));
            }
            return movies;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movie taking only by name", e);
        }
    }

    /**
     *  Adds instance of {@code Movie} to database
     * @param movie is instance which will be added
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void add(Movie movie) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_MOVIE)) {
            System.out.println(movie);
            statement.setString(1, movie.getName());
            statement.setInt(2, movie.getYear());
            statement.setString(3, movie.getDescription());
            statement.setLong(4, movie.getCountry().getCountryId());
            statement.setString(5, movie.getCover());
            statement.setLong(6, movie.getDirector().getDirectorId());
            statement.setInt(7, movie.getDuration());
            statement.setString(8, movie.getTrailer());
            statement.setTimestamp(9, Timestamp.valueOf(movie.getDateAdded()));
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movie adding", e);
        }
    }

    /** Deletes movie from database by movie's identification number
     * @param id is identification number of instance which will be deleted
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void delete(Long id) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_MOVIE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movie deleting", e);
        }

    }

    /**
     * Method takes new instance of {@code Movie} and replace other one
     * @param id is movie's identification number
     * @param movie new instance of {@code Movie} which will replace existing one
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void update(Long id, Movie movie) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_MOVIE)) {
            statement.setString(1, movie.getName());
            statement.setInt(2, movie.getYear());
            statement.setString(3, movie.getDescription());
            statement.setLong(4, movie.getCountry().getCountryId());
            statement.setString(5, movie.getCover());
            statement.setString(6, movie.getTrailer());
            statement.setLong(7, movie.getDirector().getDirectorId());
            statement.setInt(8, movie.getDuration());
            statement.setLong(9, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movie updating", e);
        }

    }

    /**
     * Takes movies by actor who starred in the movie
     * @param id is identification number of actor
     *           who starred in the movie
     * @return list of {@code Movie} with actor with {@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<Movie> takeMoviesByActorId(Long id) throws DAOException {
        List<Movie> movies = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL_MOVIES_BY_ACTOR)) {
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                movies.add(createMovie(resultSet));
            }
            return movies;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movies taking by actor ID", e);
        }
    }

    /**
     * Takes movies by the director who shot the film
     * @param id is director's identification number
     * @return list of {@code Movie} shooting by director
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<Movie> takeMoviesByDirectorId(Long id) throws DAOException {
        List<Movie> movies = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL_MOVIES_BY_DIRECTOR)) {
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                movies.add(createMovie(resultSet));
            }
            return movies;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movies taking by director ID", e);
        }
    }

    /**
     * Takes movies by the country where the film was shot
     * @param countryId is country's identification number
     * @return list of {@code Movie} with such country
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<Movie> takeMoviesByCountryId(Long countryId) throws DAOException{
        List<Movie> movies = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL_MOVIES_BY_COUNTRY)) {
            statement.setLong(1, countryId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                movies.add(createMovie(resultSet));
            }
            return movies;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movies taking by country ID", e);
        }
    }

    /**
     * Add link between movie table and actor table
     * that means the actor starred in the film
     * @param movieId is identification number of actor starred in the film
     * @param actorId is identification number of film where actor was starred
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void addConnectionWithActorTable(Long movieId, Long actorId) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_CONNECTION_WITH_ACTOR_TABLE)) {
            statement.setLong(1, movieId);
            statement.setLong(2, actorId);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during connection with actor table adding", e);
        }
    }

    /**
     * Delete link between movie table and actor table
     * that means  delete actor from the staff of film
     * @param movieId is identification number of actor starred in the film
     * @param actorId is identification number of film where actor was starred
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void deleteConnectionWithActorTable(Long movieId, Long actorId) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_CONNECTION_WITH_ACTOR_TABLE)) {
            statement.setLong(1, movieId);
            statement.setLong(2, actorId);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during connection with actor table deleting", e);
        }
    }

    /**
     * Takes all movies of a particular genre
     * @param genreId is identification number of genre
     * @return list of {@code Movie} with such genre
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<Movie> takeAllMoviesByGenre(Long genreId) throws DAOException {
        List<Movie> movies = new ArrayList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_ALL_FILMS_BY_GENRE)) {
            statement.setLong(1, genreId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Movie movie = createMovie(resultSet);
                movies.add(movie);
            }
            return movies;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during movies taking by genre", e);
        }
    }

    /**
     * Add link between movie table and genre table
     * that means add genre to the current film
     * @param movieId is identification number of movie which will be added
     * @param genreId is identification number of genre which should be added
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void addConnectionWithGenreTable(Long movieId, Long genreId) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_CONNECTION_WITH_GENRE_TABLE)) {
            statement.setLong(1, movieId);
            statement.setLong(2, genreId);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during connection with genre table adding", e);
        }
    }

    /**
     * Delete link between movie table and genre table
     * that means delete genre from the movie
     * @param movieId is identification number of movie
     * @param genreId is identification number of genre which should be deleted
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void deleteConnectionWithGenreTable(Long movieId, Long genreId) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_CONNECTION_WITH_GENRE_TABLE)) {
            statement.setLong(1, movieId);
            statement.setLong(2, genreId);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during connection with genre table deleting", e);
        }
    }

    /**
     * Takes last added to database movies
     * @return list of last added {@code Movie}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public List<Movie> takeLastAdded() throws DAOException {
        List<Movie> movies = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_LAST_TEN_ADDED)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Movie movie = createMovie(resultSet);
                movies.add(movie);
            }
            return movies;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during last added movies taking", e);
        }
    }

    /**
     * Creates instance of {@code Movie} from resultSet
     * @param resultSet is instance of {@code ResultSet}
     * @return already filled instance of {@code Movie}
     * @throws SQLException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    private Movie createMovie(ResultSet resultSet) throws SQLException {
        Movie movie = new Movie();
        Director director = new Director();
        director.setDirectorId(resultSet.getLong(DIRECTOR_ID));
        director.setFirstName(resultSet.getString(DIRECTOR_FIRST_NAME));
        director.setLastName(resultSet.getString(DIRECTOR_LAST_NAME));
        movie.setDirector(director);
        movie.setMovieId(resultSet.getLong(ID));
        movie.setName(resultSet.getString(MOVIE_NAME));
        movie.setDescription(resultSet.getString(DESCRIPTION));
        Country country = new Country();
        country.setCountry(resultSet.getString(COUNTRY));
        movie.setCountry(country);
        movie.setYear(resultSet.getInt(YEAR));
        movie.setCover(resultSet.getString(COVER));
        LocalDate date = resultSet.getDate(DATE_ADDED).toLocalDate();
        LocalTime time = resultSet.getTime(DATE_ADDED).toLocalTime();
        LocalDateTime dateAdded = LocalDateTime.of(date, time);
        movie.setDateAdded(dateAdded);
        movie.setDuration(resultSet.getInt(DURATION));
        movie.setTrailer(resultSet.getString(TRAILER));

        return movie;
    }

}
