package by.epam.movierating.dao;

import by.epam.movierating.connection.ConnectionPool;
import by.epam.movierating.entity.Rating;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.DatabaseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code RatingDAO} class implements all the necessary methods
 * that were inherited from {@code AbstractMovieDAO} class and {@code IDAO}
 * interface for manipulation with movie's data in database.
 */

public class RatingDAO extends AbstractRatingDAO{
    private static final String MOVIE_ID= "movie_id";
    private static final String NUMBER_OF_USERS= "user_sum";
    private static final String SUM_OF_RATINGS= "rating_sum";

    private static final String SQL_TAKE_ALL = "SELECT movie_id, rating_sum, user_sum " +
            "FROM movie_rating.rating";

    private static final String SQL_TAKE_RATING_BY_ID = "SELECT movie_id, rating_sum, user_sum " +
            "FROM movie_rating.rating WHERE movie_id = ?";

    private static final String SQL_ADD_RATING = "INSERT INTO movie_rating.rating(movie_id, rating_sum, user_sum) " +
            "VALUE (?,?,?)";

    private static final String SQL_DELETE_RATING = "DELETE FROM movie_rating.review WHERE review_id = ?";

    private static final String SQL_UPDATE_RATING = "UPDATE movie_rating.rating SET rating_sum = ?, " +
            "user_sum = ? WHERE movie_id = ?";

    private final static String SQL_ADD_CONNECTION_WITH_USER = "INSERT INTO movie_rating.user_movie_rating(user_id, movie_id, rating) " +
            "VALUES (?, ?, ?)";

    private final static String SQL_FIND_USER_RATING = "SELECT rating FROM movie_rating.user_movie_rating WHERE " +
            "movie_id = ? AND user_id = ?";

    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Takes all ratings from database
     *
     * @return list of instances {@code Rating}
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Rating> takeAll() throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_ALL)) {
            ResultSet set = statement.executeQuery();
            List<Rating> ratings = new ArrayList<>();
            while (set.next()) {
                Rating rating = new Rating();
                rating.setMovieId(set.getLong(MOVIE_ID));
                rating.setNumberOfUsers(set.getInt(NUMBER_OF_USERS));
                rating.setSumOfRatings(set.getLong(SUM_OF_RATINGS));
                ratings.add(rating);
            }
            return ratings;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all ratings taking", e);
        }
    }

    /**
     * Takes rating from database by rating's identification
     * number
     * @param id is ratings's identification number
     * @return instance of {@code Rating} with such {@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Rating take(Long id) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_TAKE_RATING_BY_ID)) {
            statement.setLong(1, id);
            Rating rating = null;
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                rating = new Rating();
                rating.setMovieId(id);
                rating.setNumberOfUsers(set.getInt(NUMBER_OF_USERS));
                rating.setSumOfRatings(set.getLong(SUM_OF_RATINGS));
            }
            return rating;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during rating taking", e);
        }
    }

    /**
     *  Adds instance of {@code Rating} to database
     * @param rating is instance which will be added
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void add(Rating rating) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_RATING)) {
            statement.setLong(1, rating.getMovieId());
            statement.setLong(2, rating.getSumOfRatings());
            statement.setLong(3, rating.getNumberOfUsers());
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during rating adding", e);
        }

    }

    /**
     * Deletes rating from database by rating's identification number
     * @param id is identification number of instance which will be deleted
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void delete(Long id) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_RATING)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during rating deleting", e);
        }
    }

    /**
     * Method takes new instance of {@code Rating} and replace other one
     * @param id is rating's identification number
     * @param rating new instance of {@code Rating} which will replace existing one
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void update(Long id, Rating rating) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_RATING)) {
            statement.setLong(1, rating.getSumOfRatings());
            statement.setInt(2, rating.getNumberOfUsers());
            statement.setLong(3, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during rating updating", e);
        }

    }

    /**
     * Add user mark to database and add link between
     * user table and movie table
     * @param userId is identification number of user who added mark
     * @param movieId is identification number of movie which was added to the review
     * @param rating is user mark
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void addRatingWithConnection(Long userId, Long movieId, int rating) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_CONNECTION_WITH_USER)) {
            statement.setLong(1, userId);
            statement.setLong(2, movieId);
            statement.setInt(3, rating);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during " +
                    "rating and connection with user table adding", e);
        }
    }

    /**
     * Finds user rating by {@code userId} and{@code movieId}
     * @param userId is identification number of user
     * @param movieId is identification number of movie
     * @return rating of movie
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public double findRatingByUserMovieId(Long userId, Long movieId) throws DAOException {
        double rating = 0;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_USER_RATING)) {
            statement.setLong(1, movieId);
            statement.setLong(2, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                rating = resultSet.getDouble(1);
            }
            return rating;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during rating " +
                    "taking by user ID and movie ID", e);
        }
    }

}
