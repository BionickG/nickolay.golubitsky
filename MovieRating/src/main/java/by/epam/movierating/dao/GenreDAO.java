package by.epam.movierating.dao;

import by.epam.movierating.connection.ConnectionPool;
import by.epam.movierating.entity.Genre;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.DatabaseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code GenreDAO} class implements all the necessary methods
 * that were inherited from {@code AbstractMovieDAO} class and {@code IDAO}
 * interface for manipulation with movie's data in database.
 */

public class GenreDAO extends AbstractGenreDAO{

    private final static String SQL_FIND_ALL = "SELECT genre_id, genre FROM movie_rating.genre";
    private final static String SQL_FIND_GENRE = "SELECT genre_id, genre FROM movie_rating.genre WHERE genre_id=?";
    private final static String SQL_FIND_GENRE_BY_NAME = "SELECT genre_id, genre FROM movie_rating.genre WHERE genre=?";
    private final static String SQL_ADD_GENRE = "INSERT INTO movie_rating.genre(genre) VALUE (?)";
    private final static String SQL_DELETE_GENRE = "DELETE FROM movie_rating.genre WHERE genre_id = ?";
    private final static String SQL_GET_ALL_GENRES_OF_FILM = "SELECT genre_id, genre FROM movie_rating.genre " +
            "WHERE genre_id IN (SELECT genre_id FROM movie_rating.movie_genre WHERE movie_id = ?)";

    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Takes all genres from database
     *
     * @return list of instances {@code Genre}
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Genre> takeAll() throws DAOException {
        List<Genre> genres = new LinkedList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                Genre genre = new Genre();
                genre.setGenreId(resultSet.getLong(1));
                genre.setGenre(resultSet.getString(2));
                genres.add(genre);
            }
            return genres;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all genres taking", e);
        }
    }

    /**
     * Takes genre from database by genre's identification number
     * @param id is genre's identification number
     * @return instance of {@code Genre} with such {@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Genre take(Long id) throws DAOException {
        Genre genre = new Genre();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_GENRE)) {
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                genre.setGenreId(resultSet.getLong(1));
                genre.setGenre(resultSet.getString(2));
            }
            return genre;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during genre taking", e);
        }
    }

    /**
     * Adds genre to database
     * @param genre instance of {@code Genre} which will be added
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void add(Genre genre) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_GENRE)) {
            statement.setString(1, genre.getGenre());
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during genre adding", e);
        }
    }

    /**
     * Deletes genre from database
     * @param id is genre's identification number which will be delete
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void delete(Long id) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_GENRE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during genre deleting", e);
        }
    }

    /**
     * Takes genre from database by genre's name
     * @param name is name of genre
     * @return instance of {@code Genre} with such {@code name}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    public Genre takeByName(String name) throws DAOException {
        Genre genre = null;
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_GENRE_BY_NAME)) {
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                genre = createGenreFromResultSet(resultSet);
            }
            return genre;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during taking by name", e);
        }
    }

    /**
     * Returns all genres of current movie
     * @param movieId is identification number of movie which genres will be returned
     * @return list of instances {@code Genres} of current movie
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    public List<Genre> takeAllGenresOfMovie(Long movieId) throws DAOException {
        List<Genre> genres = new LinkedList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL_GENRES_OF_FILM)) {
            statement.setLong(1, movieId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                genres.add(createGenreFromResultSet(resultSet));
            }
            return genres;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during all genres of movie taking", e);
        }
    }

    /**
     * Creates instance of {@code Genre} from resultSet
     *
     * @param resultSet is instance of {@code ResultSet}
     * @return already filled instance of {@code Genre}
     * @throws SQLException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    private Genre createGenreFromResultSet(ResultSet resultSet) throws SQLException {
        Genre genre = new Genre();
        genre.setGenreId(resultSet.getLong(1));
        genre.setGenre(resultSet.getString(2));
        return genre;

    }
}
