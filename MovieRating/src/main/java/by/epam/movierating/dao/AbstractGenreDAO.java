package by.epam.movierating.dao;

import by.epam.movierating.entity.Genre;
import by.epam.movierating.exception.DAOException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code AbstractGenreDAO} abstract class enumerates all the necessary methods
 * for manipulation with director's data in database.
 */

public abstract class AbstractGenreDAO implements IDAO<Genre> {

    /**
     * It's cap for update method from {@code IDAO<T>} interface
     *
     * @param id    is genre's identification number
     * @param genre is instance of {@code Genre}
     * @throws DAOException in the case of function call
     */

    @Override
    public void update(Long id, Genre genre) throws DAOException {
        throw new DAOException("No such function");
    }

    /**
     * Takes actor from database by genre's name
     * @param name is name of genre
     * @return instance of {@code Genre} with such {@name}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    public abstract Genre takeByName(String name) throws DAOException;

    /**
     * Returns all genres of current movie
     *
     * @param movieId is identification number of movie which genres will be returned
     * @return list of instances {@code Genres} of current movie
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    public abstract List<Genre> takeAllGenresOfMovie(Long movieId) throws DAOException;
}
