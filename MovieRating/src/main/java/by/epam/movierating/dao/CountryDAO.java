package by.epam.movierating.dao;

import by.epam.movierating.connection.ConnectionPool;
import by.epam.movierating.entity.Country;
import by.epam.movierating.exception.DAOException;
import by.epam.movierating.exception.DatabaseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code CountryDAO} class implements all the necessary methods
 * that were inherited from {@code AbstractMovieDAO} class and {@code IDAO}
 * interface for manipulation with movie's data in database.
 */
public class CountryDAO extends AbstractCountryDAO {
    private final static String SQL_FIND_ALL = "SELECT country_id, country FROM movie_rating.country";
    private final static String SQL_FIND_COUNTRY = "SELECT country_id, country FROM movie_rating.country WHERE country.country_id = ?";
    private final static String SQL_FIND_COUNTRY_BY_NAME = "SELECT country_id, country FROM movie_rating.country WHERE country.country = ?";
    private final static String SQL_ADD_COUNTRY = "INSERT INTO movie_rating.country(country) VALUE (?)";
    private final static String SQL_DELETE_COUNTRY = "DELETE FROM movie_rating.country WHERE country_id = ?";

    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Takes all countries from database
     *
     * @return list of instances {@code Country}
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Country> takeAll() throws DAOException {
        List<Country> countries = new LinkedList<>();
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                Country country = new Country();
                country.setCountryId(resultSet.getLong(1));
                country.setCountry(resultSet.getString(2));
                countries.add(country);
            }
            return countries;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during taking all countries",e);
        }
    }

    /**
     * Takes country from database by country identification number
     * @param id is country's identification number
     * @return instance of {@code Country} with such {@code id}
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Country take(Long id) throws DAOException {
        Country country = null;
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_COUNTRY)) {
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                country.setCountryId(resultSet.getLong(1));
                country.setCountry(resultSet.getString(2));
            }
            return country;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during country taking", e);
        }
    }

    /**
     * Adds country to database
     * @param country instance of {@code Country} which will be added
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void add(Country country) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_ADD_COUNTRY)) {
            statement.setString(1, country.getCountry());
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during country adding", e);
        }
    }

    /**
     * Deletes country from database
     * @param id is country's identification number which will be delete
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public void delete(Long id) throws DAOException {
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_COUNTRY)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during country deleting", e);
        }
    }

    /**
     * Takes instance of {@code Country} by country's name from the database
     * @param name is country name
     * @return instance of {@code Country} satisfying the condition
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */

    @Override
    public Country takeByName(String name) throws DAOException {
        Country country = null;
        ResultSet resultSet;
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_COUNTRY_BY_NAME)) {
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                country = new Country();
                country.setCountryId(resultSet.getLong(1));
                country.setCountry(resultSet.getString(2));
            }
            return country;
        } catch (DatabaseException | SQLException e) {
            throw new DAOException("Invalid database operation occurred during country taking by name", e);
        }
    }
}
