package by.epam.movierating.dao;

import by.epam.movierating.entity.Review;
import by.epam.movierating.exception.DAOException;

import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code AbstractReviewDAO} abstract class enumerates all the necessary methods
 * for manipulation review's data in database.
 */

public abstract class AbstractReviewDAO implements IDAO<Review> {

    /**
     * Takes all reviews of current movie
     *
     * @param movieId is identification number of movie
     *                which reviews we searching for
     * @return list of {@code Review} of current movie
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */
    public abstract List<Review> takeByMovieId(Long movieId) throws DAOException;

    /**
     * Takes all marks of user's reviews
     * @param userId is identification number of user
     * @return summ of all marks
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract int takeAllMarks(Long userId) throws DAOException;

    /**
     * Add mark to the existing mark of review
     * @param reviewId identification number of review which fill with a mark
     * @param mark is mark which will be added
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract void changeMark(Long reviewId, int mark) throws DAOException;

    /**
     * Add link between user table and review table
     * that means user add his own review
     * @param userId is identification number of user who adding review
     * @param reviewId identification number of review which will be marked
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract void addConnectionWithUser(Long userId, Long reviewId) throws DAOException;

    /**
     * Method checks did user already marked movie
     * @param userId is user's identification number
     * @param reviewId is review's identification number
     * @return boolean value which marks user's choice
     * @throws DAOException if unable to get connection to database or
     *          if there are any troubles with query execution.
     */
    public abstract boolean isConnectionExist(Long userId, Long reviewId) throws DAOException;
}
