package by.epam.movierating.listener;

import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Nickolay Golubitsky
 */

@WebListener
public class ListenerImpl implements HttpSessionListener{
    private static final String LOCALE = "locale";
    private static final String LINK_RU = "messages";
    private static final String MESSAGE_BUNDLE = "bundle";
    private static final String PAGE = "page";
    private static final String RETURN_PAGE = "path.page.index";
    private static final String USER = "user";


    @Override
    public void sessionCreated(HttpSessionEvent se) {
        se.getSession().setAttribute(MESSAGE_BUNDLE, ResourceBundle.getBundle(LINK_RU));
        se.getSession().setAttribute(LOCALE, Locale.getDefault().toString());
        String page = ConfigurationManager.getProperty(RETURN_PAGE);
        se.getSession().setAttribute(PAGE, page);
        User user = new User();
        user.setRole(Role.GUEST);
        se.getSession().setAttribute(USER, user);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
    }

}
