package by.epam.movierating.command.navigation;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.entity.Genre;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.GenreServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Nikolay Golubitsky
 * The {@code GoToMainPageCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class GoToMainPageCommand implements ActionCommand {
    private static final String GENRES = "genres";

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Forwards you to main page and fill in list of genres,
     * which will be displayed in the navigation menu
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page = "/controller?command=take_last";
        HttpSession session = request.getSession();
        GenreServiceImpl genreService = new GenreServiceImpl();
        List<Genre> genres;
        try {
            genres = genreService.takeAll();
            session.setAttribute(GENRES, genres);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
        }

        return page;
    }
}
