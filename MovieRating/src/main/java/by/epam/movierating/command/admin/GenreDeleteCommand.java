package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.GenreServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;


/**
 * @author Nikolay Golubitsky
 * The {@code GenreDeleteCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class GenreDeleteCommand implements ActionCommand{

    private final static String SUCCESS_MESSAGE = "message.genre.delete.success";
    private final static String FAIL_MESSAGE = "message.genre.delete.fail";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String GENRE_ID = "genreId";
    private final static String MESSAGE = "message";
    private final static String GENRES = "genres";
    private final static String PAGE = "page";
    private final static String USER = "user";

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Delete the genre from the system.
     * @param request is an instance of {@code HttpServletRequest}.
     * @return admin page after deleting the genre
     */

    @Override
    public String execute(HttpServletRequest request) {
        GenreServiceImpl genreService = new GenreServiceImpl();
        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        if (((User)session.getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR))
        {
            try {
                Long genreId = Long.valueOf(request.getParameter(GENRE_ID));
                genreService.delete(genreId);
                session.setAttribute(GENRES, genreService.takeAll());
                request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS_MESSAGE,
                        (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            } catch (ServiceException e) {
                request.setAttribute(MESSAGE, FAIL_MESSAGE);
                LOGGER.error(e.getMessage());
            }
        }
        return page;
    }
}
