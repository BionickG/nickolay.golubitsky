package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.UserServiceImpl;
import by.epam.movierating.util.ImageUploading;
import by.epam.movierating.validation.DirectoryExistValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Nikolay Golubitsky
 * The {@code AvatarAddCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class AvatarAddCommand implements ActionCommand {

    private static final String AVATAR_UPDATE_FAIL = "message.avatar.update.fail";
    private static final String UPLOAD_DIRECTORY = "path.upload.avatars";
    private static final String OOPS_PAGE = "path.page.oops";
    private static final String MESSAGE = "message";
    private static final String AVATAR = "avatar";
    private static final String PAGE = "page";
    private static final String USER = "user";

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Add or update user's avatar
     * @param request is an instance of {@code HttpServletRequest}.
     * @return user page after user's avatar has been added
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page = (String) request.getSession().getAttribute(PAGE);
        User user = (User) request.getSession().getAttribute(USER);
        UserServiceImpl userService = new UserServiceImpl();
        Long userId = user.getUserId();
        String appPath = request.getServletContext().getRealPath("");
        String directory = ConfigurationManager.getProperty(UPLOAD_DIRECTORY);
        DirectoryExistValidator.isDirectoryExist(appPath, directory);
        String fileName = userId.toString() + ".jpg";
        String path = DirectoryExistValidator.isDirectoryExist(appPath, directory)
                + File.separator + fileName;
        try(InputStream in = request.getPart(AVATAR).getInputStream();
            FileOutputStream fos = new FileOutputStream(path)) {
            ImageUploading.upload(in,fos,directory,fileName);
            String avatar = directory + "/" + fileName;
            user.setAvatar(avatar);
            userService.addAvatar(userId,avatar);
        } catch (IOException | ServletException e) {
            LOGGER.error(e.getMessage());
            request.setAttribute(MESSAGE, AVATAR_UPDATE_FAIL);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);

        }
        return page;
    }

}
