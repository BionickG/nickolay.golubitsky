package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Review;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.ReviewServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Nikolay Golubitsky
 * The {@code ReviewMarkDecCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class ReviewMarkDecCommand implements ActionCommand {

    private static final String REVIEW_ID = "reviewId";
    private static final String MOVIE = "movie";
    private static final String PAGE = "page";
    private static final String USER = "user";

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Decrease user review rating
     * @param request is an instance of {@code HttpServletRequest}.
     * @return movie page with new list of reviews with new ratings
     */

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        ReviewServiceImpl reviewService = new ReviewServiceImpl();
        String page = (String) session.getAttribute(PAGE);
        Long reviewId = Long.valueOf(request.getParameter(REVIEW_ID));
        Long userId = ((User)session.getAttribute(USER)).getUserId();
        Movie movie = (Movie) session.getAttribute(MOVIE);
        try {
            reviewService.changeMark(reviewId, -1);
            reviewService.addConnectionWithUser(userId, reviewId);

            List<Review> reviews = reviewService.takeAllReviewsByMovieId(movie.getMovieId());
            for (Review review : reviews){
                review.setRated(reviewService.isConnectionExist(userId, reviewId));
            }
            movie.setReviews(reviews);
            session.setAttribute(MOVIE, movie);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
        }
        return page;
    }
}
