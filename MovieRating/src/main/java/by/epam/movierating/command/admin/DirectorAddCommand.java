package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Director;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.DirectorServiceImpl;
import by.epam.movierating.validation.RegExpValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

/**
 * Created by Nickolay Golubitsky on 29.09.2016.
 */
public class DirectorAddCommand implements ActionCommand {
    private final static String SUCCESS_MESSAGE = "message.director.create.success";
    private final static String FAIL_MESSAGE = "message.director.create.fail";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String FIRST_NAME = "firstName";
    private final static String LAST_NAME = "lastName";
    private final static String DIRECTORS = "directors";
    private final static String MESSAGE = "message";
    private final static String PAGE = "page";
    private final static String USER = "user";

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Add new director to the system
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return admin page after adding an director
     */
    @Override
    public String execute(HttpServletRequest request) {
        DirectorServiceImpl directorService = new DirectorServiceImpl();
        RegExpValidator valid = new RegExpValidator();
        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        String firstName = request.getParameter(FIRST_NAME);
        String lastName = request.getParameter(LAST_NAME);
        if (((User) session.getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR)) {
            try {
                if (valid.checkData(FIRST_NAME, firstName) && valid.checkData(LAST_NAME, lastName)) {
                    Director director = new Director();
                    director.setFirstName(firstName);
                    director.setLastName(lastName);
                    directorService.add(director);
                } else {
                    request.setAttribute(MESSAGE, FAIL_MESSAGE);
                }
                session.setAttribute(DIRECTORS, directorService.takeAll());
                request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            } catch (ServiceException e) {
                request.setAttribute(MESSAGE, FAIL_MESSAGE);
                LOGGER.error(e.getMessage());
            }
        }
        return page;
    }
}
