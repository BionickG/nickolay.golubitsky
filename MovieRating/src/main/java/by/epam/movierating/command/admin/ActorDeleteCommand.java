package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.ActorServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code ActorDeleteCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class ActorDeleteCommand implements ActionCommand {
    private final static String SUCCESS_MESSAGE = "message.actor.delete.success";
    private final static String FAIL_MESSAGE = "message.actor.delete.fail";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String ACTOR_ID = "actorId";
    private final static String MESSAGE = "message";
    private final static String ACTORS = "actors";
    private final static String USER = "user";
    private final static String PAGE = "page";


    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Delete an actor from the system.
     * @param request is an instance of {@code HttpServletRequest}.
     * @return admin page after deleting an actor.
     */

    @Override
    public String execute(HttpServletRequest request) {
        ActorServiceImpl actorService = new ActorServiceImpl();
        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        if (((User)session.getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR))
        {
            try {
                Long actorId = Long.valueOf(request.getParameter(ACTOR_ID));
                actorService.delete(actorId);
                session.setAttribute(ACTORS, actorService.takeAll());
                request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS_MESSAGE,
                        (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            } catch (ServiceException e) {
                request.setAttribute(MESSAGE, FAIL_MESSAGE);
                LOGGER.error(e.getMessage());
            }
        }
        return page;
    }
}
