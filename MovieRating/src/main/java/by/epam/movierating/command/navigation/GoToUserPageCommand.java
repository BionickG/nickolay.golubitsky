package by.epam.movierating.command.navigation;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.Actor;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Nikolay Golubitsky
 * The {@code GoToUserPageCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class GoToUserPageCommand implements ActionCommand {

    private static final String ADMIN_PAGE = "path.page.adminPage";
    private static final String USER_PAGE = "path.page.userPage";
    private static final String OOPS_PAGE = "path.page.oops";
    private static final String COUNTRIES = "countries";
    private static final String DIRECTORS = "directors";
    private static final String LAST_GO_TO_COMMAND = "lastCommand";
    private static final String COMMAND = "command";
    private static final String ACTORS = "actors";
    private static final String GENRES = "genres";
    private static final String MOVIES = "movies";
    private static final String USERS = "users";
    private final static String USER = "user";
    private final static String PAGE = "page";
    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Forwards you to user page, if you have administrator's rights
     * you wil be forwarded to administrator's page
     * @param request is an instance of {@code HttpServletRequest}.
     * @return user page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        ReviewServiceImpl reviewService = new ReviewServiceImpl();
        MovieServiceImpl movieService = new MovieServiceImpl();
        ActorServiceImpl actorService = new ActorServiceImpl();
        UserServiceImpl userService = new UserServiceImpl();
        DirectorServiceImpl directorService = new DirectorServiceImpl();
        GenreServiceImpl genreService = new GenreServiceImpl();
        CountryServiceImpl countryService = new CountryServiceImpl();
        HttpSession session = request.getSession();
        session.setAttribute(LAST_GO_TO_COMMAND, request.getParameter(COMMAND));
        User user = (User) session.getAttribute(USER);
        try {
            user.setUserRating(reviewService.takeAllMarks(user.getUserId()));
            session.setAttribute(USER, user);
            List<User> users = userService.takeAll();
            for (User loopUser : users) {
                loopUser.setUserRating(reviewService.takeAllMarks(loopUser.getUserId()));
            }
            if (user.getRole().equals(Role.ADMINISTRATOR)) {
                List<Movie> movies = movieService.takeAll();
                List<Actor> actors = actorService.takeAll();
                session.setAttribute(MOVIES, movies);
                session.setAttribute(USERS, users);
                session.setAttribute(ACTORS, actors);
                session.setAttribute(DIRECTORS, directorService.takeAll());
                session.setAttribute(GENRES, genreService.takeAll());
                session.setAttribute(COUNTRIES, countryService.takeAll());
                page = ConfigurationManager.getProperty(ADMIN_PAGE);
                session.setAttribute(PAGE, page);
            } else {

                page = ConfigurationManager.getProperty(USER_PAGE);
                session.setAttribute(PAGE, page);
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }

        return page;
    }
}
