package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.encryption.EncryptionData;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.UserServiceImpl;
import by.epam.movierating.validation.RegExpValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code RegistrationCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class RegistrationCommand implements ActionCommand {

    private static final String PASSWORD_MATCH_FAIL_MESSAGE = "message.register.password";
    private static final String LOGIN_FAIL_MESSAGE = "message.login.valid.fail";
    private static final String EMAIL_FAIL_MESSAGE = "message.email.valid.fail";
    private static final String NAME_FAIL_MESSAGE = "message.name.valid.fail";
    private static final String REGISTRATION_PAGE = "path.page.registration";
    private static final String PASSWORD_MATCH_FAIL = "passwordMatchFail";
    private static final String CONFIRM_PASSWORD = "confirmPassword";
    private static final String MAIN_PAGE = "path.page.index";
    private final static String OOPS_PAGE = "path.page.oops";
    private static final String MESSAGE_BUNDLE = "bundle";
    private static final String EMAIL_FAIL = "emailFail";
    private static final String LOGIN_FAIL = "loginFail";
    private static final String FIRST_NAME = "firstName";
    private static final String NAME_FAIL = "nameFail";
    private static final String LAST_NAME = "lastName";
    private static final String PASSWORD = "password";
    private static final String LOGIN = "login";
    private static final String EMAIL = "email";
    private static final String USER = "user";
    private static final String PAGE = "page";

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Registers user in the system after validation
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page with current user
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(MAIN_PAGE);
        UserServiceImpl userService = new UserServiceImpl();
        RegExpValidator valid = new RegExpValidator();
        HttpSession session = request.getSession();
        User user = new User();
        String firstName = request.getParameter(FIRST_NAME);
        String lastName = request.getParameter(LAST_NAME);
        if (valid.checkData(FIRST_NAME, firstName) && valid.checkData(LAST_NAME, lastName)){
            user.setFirstName(firstName);
            user.setLastName(lastName);
        } else {
            page = ConfigurationManager.getProperty(REGISTRATION_PAGE);
            request.setAttribute(NAME_FAIL, MessageManager.getProperty(NAME_FAIL_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
        }

        String email = request.getParameter(EMAIL);
        if (valid.checkData(EMAIL, email)){
            user.setEmail(email);
        } else {
            page = ConfigurationManager.getProperty(REGISTRATION_PAGE);
            request.setAttribute(EMAIL_FAIL, MessageManager.getProperty(EMAIL_FAIL_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
        }
        user.setRole(Role.USER);
        String login = request.getParameter(LOGIN);
        try {
            if (valid.checkData(LOGIN, login) && !userService.takeUserByLogin(login)){
                user.setLogin(login);
            } else {
                page = ConfigurationManager.getProperty(REGISTRATION_PAGE);
                request.setAttribute(LOGIN_FAIL, MessageManager.getProperty(LOGIN_FAIL_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
                return page;
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }
        String confirmPassword = request.getParameter(CONFIRM_PASSWORD);
        String password = request.getParameter(PASSWORD);
        if (password != null && confirmPassword != null && password.equals(confirmPassword)) {
            String passwordEnc = EncryptionData.getMD5(password);
            user.setPassword(passwordEnc);
            
            try {
                userService.add(user);
                user = userService.defineUser(login, password);
                session.setAttribute(USER, user);
                session.setAttribute(PAGE, page);
                LOGGER.info("User " + user + "has been created");
            } catch (ServiceException e) {
                LOGGER.error(e.getMessage());
                page = ConfigurationManager.getProperty(OOPS_PAGE);
            }
        } else {
            page = ConfigurationManager.getProperty(REGISTRATION_PAGE);
            request.setAttribute(PASSWORD_MATCH_FAIL,
                    MessageManager.getProperty(PASSWORD_MATCH_FAIL_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
        }
        return page;
    }
}
