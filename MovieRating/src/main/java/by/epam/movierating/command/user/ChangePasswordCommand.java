package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.UserServiceImpl;
import by.epam.movierating.validation.RegExpValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;


/**
 * @author Nikolay Golubitsky
 * The {@code ChangePasswordCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class ChangePasswordCommand implements ActionCommand {

    private static final String SUCCESS = "message.password.success";
    private static final String USER_PAGE = "path.page.userPage";
    private static final String ERROR = "message.password.fail";
    private static final String PASSWORD_NEW = "passwordNew";
    private static final String MESSAGE_BUNDLE = "bundle";
    private static final String PASSWORD = "password";
    private static final String MESSAGE = "message";
    private static final String USER = "user";

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Change user's password
     * @param request is an instance of {@code HttpServletRequest}.
     * @return user page after user's password has been changed
     */

    @Override
    public String execute(HttpServletRequest request) {
        UserServiceImpl userService = new UserServiceImpl();
        HttpSession session = request.getSession();
        RegExpValidator valid = new RegExpValidator();
        User user = (User) session.getAttribute(USER);
        Long userId = user.getUserId();
        String password = request.getParameter(PASSWORD);
        String passwordNew = request.getParameter(PASSWORD_NEW);
        if (valid.checkData(PASSWORD, passwordNew)){
            try {
                userService.changePassword(userId, password, passwordNew);
                request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS,
                        (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            } catch (ServiceException e) {
                LOGGER.error(e.getMessage());
                request.setAttribute(MESSAGE, MessageManager.getProperty(ERROR,
                        (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            }
        } else {
            request.setAttribute(MESSAGE, MessageManager.getProperty(ERROR,
                    (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
        }
        return ConfigurationManager.getProperty(USER_PAGE);
    }
}
