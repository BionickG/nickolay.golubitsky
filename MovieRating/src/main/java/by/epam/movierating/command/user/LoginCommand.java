package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;


/**
 * @author Nikolay Golubitsky
 * The {@code ActionCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class LoginCommand implements ActionCommand {

    private static final String ERROR_MESSAGE = "message.login.error";
    private static final String LOGIN_PAGE = "path.page.login";
    private static final String MAIN_PAGE = "path.page.index";
    private static final String ERROR_ATTRIBUTE = "loginFail";
    private final static String OOPS_PAGE = "path.page.oops";
    private static final String MESSAGE_BUNDLE = "bundle";
    private static final String PASSWORD = "password";
    private static final String LOGIN = "login";
    private static final String USER = "user";
    private static final String PAGE = "page";

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Log in user to the system
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        HttpSession session = request.getSession();
        UserServiceImpl userService = new UserServiceImpl();
        User user;
        try {
            user = userService.defineUser(login, password);
            if (user != null)
            {
                page = ConfigurationManager.getProperty(MAIN_PAGE);
                session.setAttribute(USER, user);
                session.setAttribute(PAGE, page);
            }else{
                request.setAttribute(ERROR_ATTRIBUTE,
                        MessageManager.getProperty(ERROR_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
                page = ConfigurationManager.getProperty(LOGIN_PAGE);
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }
        return page;
    }
}
