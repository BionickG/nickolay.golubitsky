package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Actor;
import by.epam.movierating.entity.Genre;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.*;
import by.epam.movierating.util.ImageUploading;
import by.epam.movierating.validation.DirectoryExistValidator;
import by.epam.movierating.validation.RegExpValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code MovieUpdateCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class MovieUpdateCommand implements ActionCommand {
    private final static String SUCCESS_MESSAGE = "message.movie.update.success";
    private final static String COVER_ADD_FAIL_MESSAGE = "message.cover.update.fail";
    private final static String FAIL_MESSAGE = "message.movie.update.fail";
    private final static String UPDATE_PAGE = "path.page.updatePage";
    private static final String FIRST_LAST_NAME = "firstLastName";
    private final static String TRAILER_LINK = "trailerLink";
    private final static String DESCRIPTION = "description";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String DIRECTOR = "director";
    private final static String DURATION = "duration";
    private final static String MESSAGE = "message";
    private final static String COUNTRY = "country";
    private final static String MOVIE_NAME = "name";
    private final static String ACTORS = "actors";
    private final static String MOVIE = "movie";
    private final static String COVER = "cover";
    private final static String GENRE = "genre";
    private final static String ID = "movieId";
    private final static String YEAR = "year";
    private final static String PAGE = "page";
    private final static Logger LOGGER = LogManager.getLogger();
    private final String UPLOAD_DIRECTORY = "path.upload.covers";

    /**
     * Update the movie.
     * @param request is an instance of {@code HttpServletRequest}.
     * @return admin page after updating the movie
     */

    @Override
    public String execute(HttpServletRequest request) {
        MovieServiceImpl movieService = new MovieServiceImpl();
        CountryServiceImpl countryService = new CountryServiceImpl();
        DirectorServiceImpl directorService = new DirectorServiceImpl();
        ActorServiceImpl actorService = new ActorServiceImpl();
        GenreServiceImpl genreService = new GenreServiceImpl();
        RegExpValidator valid = new RegExpValidator();
        HttpSession session = request.getSession();

        String page = ConfigurationManager.getProperty(UPDATE_PAGE);
        Long movieId = Long.valueOf(request.getParameter(ID));
        try {
            Movie movieOld = movieService.take(movieId);
            Movie movieNew = movieOld;

            String name = request.getParameter(MOVIE_NAME);
            if (!name.equals("") && valid.checkData(MOVIE_NAME, name)) {
                movieNew.setName(name);
            }

            String trailerLink = request.getParameter(TRAILER_LINK);
            if (!trailerLink.equals("")){
                movieNew.setTrailer(trailerLink);
            } 
            
            String durationString = request.getParameter(DURATION);
            if (!"".equals(durationString) && valid.checkData(DURATION, durationString)) {
                int duration = Integer.parseInt(durationString);
                movieNew.setDuration(duration);
            } 

            String description = request.getParameter(DESCRIPTION);
            if (!description.equals("")) {
                movieNew.setDescription(description);
            } 
            
            String yearString = request.getParameter(YEAR);
            if (!"".equals(yearString) && valid.checkData(YEAR, yearString)) {
                int year = Integer.parseInt(yearString);
                movieNew.setYear(year);
            }

            String countryName = request.getParameter(COUNTRY);
            if (!"".equals(countryName)|| valid.checkData(COUNTRY, countryName)){
                movieNew.setCountry(countryService.takeCountryByNameWithCreation(countryName));
            }

            String directorName = request.getParameter(DIRECTOR);
            if (valid.checkData(FIRST_LAST_NAME, directorName))
            {
                movieNew.setDirector(directorService.takeDirectorByFullName(directorName));
            }

            if (request.getParameterValues(ACTORS) != null) {
                List<String> actorNames = Arrays.asList(request.getParameterValues(ACTORS));
                for (String actorName : actorNames){
                    if (valid.checkData(FIRST_LAST_NAME, actorName)) {
                        Actor actor = actorService.takeActorByFullName(actorName);
                        movieService.addConnectionWithActorTable(movieId, actor.getActorId());
                    }
                }
            }

            List<String> genreNames = new LinkedList<>();
            genreNames.add(request.getParameter(GENRE));
            for (String genreName : genreNames) {
                if (!genreName.equals("")) {
                    Genre genre = genreService.takeByNameWithCreation(genreName);
                    movieService.addConnectionWithGenreTable(movieId, genre.getGenreId());
                }
            }

            String fileName = movieId.toString() + ".jpg";
            String appPath = request.getServletContext().getRealPath("");
            String directory = ConfigurationManager.getProperty(UPLOAD_DIRECTORY);
            String path = DirectoryExistValidator.isDirectoryExist(appPath, directory);
            try(InputStream in = request.getPart(COVER).getInputStream();
                FileOutputStream fos = new FileOutputStream(path)) {
                movieNew.setCover(ImageUploading.upload(in, fos, directory, fileName));

            } catch (IOException | ServletException e) {
                LOGGER.error(e.getMessage());
                request.setAttribute(MESSAGE, MessageManager.getProperty(COVER_ADD_FAIL_MESSAGE,
                        (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            }

            movieService.update(movieId, movieNew);
            session.setAttribute(PAGE, page);
            session.setAttribute(MOVIE, movieService.take(movieId));
            request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS_MESSAGE,
                    (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));

        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            request.setAttribute(MESSAGE, MessageManager.getProperty(FAIL_MESSAGE,
                    (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
        }
        return page;
    }
}
