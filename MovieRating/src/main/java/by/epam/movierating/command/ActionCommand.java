package by.epam.movierating.command;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nikolay Golubitsky
 * The {@code ActorsTakeAllCommand} interface enumerates all the necessary methods
 * for commands
 */
public interface ActionCommand {
    String execute(HttpServletRequest request);
}
