package by.epam.movierating.command.navigation;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Nikolay Golubitsky
 * The {@code GoToRegisterPageCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class GoToRegisterPageCommand implements ActionCommand {
    private static final String REGISTER_PAGE = "path.page.registration";
    private static final String PAGE = "page";

    /**
     * Forwards you to registration page
     * @param request is an instance of {@code HttpServletRequest}.
     * @return registration page page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(REGISTER_PAGE);
        HttpSession session = request.getSession();
        session.setAttribute(PAGE, page);
        return page;
    }
}
