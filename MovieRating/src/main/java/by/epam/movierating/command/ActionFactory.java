package by.epam.movierating.command;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nikolay Golubitsky
 * The {@code ActionFactory} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class ActionFactory {

    /**
     * Finds and creates command.
     * @param request is an instance of {@code HttpServletRequest}.
     * @return instance of {@code ActionCommand}.
     */

    public ActionCommand defineCommand(HttpServletRequest request){
        ActionCommand current;
        String action = request.getParameter("command");
        try {
            CommandEnum commandEnum = CommandEnum.valueOf(action.toUpperCase());
            current = commandEnum.getCurrentCommand();
            return current;
        } catch (IllegalArgumentException e) {
            return CommandEnum.GOTO_MAIN_PAGE.getCurrentCommand();
        }



    }


}
