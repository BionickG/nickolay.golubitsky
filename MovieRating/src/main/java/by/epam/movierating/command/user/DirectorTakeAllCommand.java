package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.Director;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.DirectorServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Nikolay Golubitsky
 * The {@code DirectorsTakeAllCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class DirectorTakeAllCommand implements ActionCommand {
    private static final String NUMBER_OF_PAGES = "numberOfPages";
    private final static String OOPS_PAGE = "path.page.oops";
    private final static String MAIN_PAGE = "path.page.main";
    private static final String PAGE_NUMBER = "pageNumber";
    private final static String DIRECTORS = "directors";
    private final static String ACTORS = "actors";
    private final static String MOVIES = "movies";
    private final static String PAGE = "page";
    private static final int CURRENT_PAGE = 0;

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Forwards you to main page and show all directors in the system
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page with all directors
     */

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String page = ConfigurationManager.getProperty(MAIN_PAGE);
        DirectorServiceImpl directorService = new DirectorServiceImpl();
        List<Director> directors;
        try {
            directors = directorService.takeAll();
            int numberOfPages = directors.size()/10;
            if (directors.size()%10 !=0){
                ++numberOfPages;
            }
            session.setAttribute(DIRECTORS, directors);
            session.setAttribute(MOVIES, null);
            session.setAttribute(ACTORS, null);
            session.setAttribute(PAGE, page);
            session.setAttribute(PAGE_NUMBER, CURRENT_PAGE);
            session.setAttribute(NUMBER_OF_PAGES, numberOfPages);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }

        return page;

    }
}
