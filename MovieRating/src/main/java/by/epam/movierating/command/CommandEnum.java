package by.epam.movierating.command;

import by.epam.movierating.command.admin.*;
import by.epam.movierating.command.navigation.*;
import by.epam.movierating.command.user.*;

/**
 * @author Nikolay Golubitsky
 * The enumeration class {@code CommandEnum} includes all the command
 * names. Each command name conducts to a definite class, that
 * realizes its own actions.
 */
 enum CommandEnum {
    LOGIN{
        /**
         * Conducts to {@code LoginCommand} class.
         * create an instance of {@code LoginCommand} class.
         */
        {
            this.command = new LoginCommand();
        }
    },
    REGISTER{
        /**
         * Conducts to {@code RegistrationCommand} class.
         * create an instance of {@code RegistrationCommand} class.
         */
        {
            this.command = new RegistrationCommand();
        }
    },
    LOGOUT{
        /**
         * Conducts to {@code LogoutCommand} class.
         * create an instance of {@code LogoutCommand} class.
         */
        {
            this.command = new LogoutCommand();
        }
    },
    LANGUAGE{
        /**
         * Conducts to {@code LanguageCommand} class.
         * create an instance of {@code LanguageCommand} class.
         */
        {
            this.command = new LanguageCommand();
        }
    },
    RETURN{
        /**
         * Conducts to {@code ReturnCommand} class.
         * create an instance of {@code ReturnCommand} class.
         */
        {
            this.command = new ReturnCommand();
        }
    },
    TAKE_LAST {
        /**
         * Conducts to {@code TakeLastMoviesCommand} class.
         * create an instance of {@code TakeLastMoviesCommand} class.
         */
        {
            this.command = new TakeLastMoviesCommand();
        }
    },
    TAKE_BY_GENRE{
        /**
         * Conducts to {@code MoviesByGenreCommand} class.
         * create an instance of {@code MoviesByGenreCommand} class.
         */
        {
            this.command = new MoviesByGenreCommand();
        }
    },
    TAKE_BY_COUNTRY{
        /**
         * Conducts to {@code MoviesByCountryCommand} class.
         * create an instance of {@code MoviesByCountryCommand} class.
         */
        {
            this.command = new MoviesByCountryCommand();
        }
    },
    TAKE_ALL_ACTORS{
        /**
         * Conducts to {@code ActorsTakeAllCommand} class.
         * create an instance of {@code ActorsTAkeAllCommand} class.
         */
        {
            this.command = new ActorsTakeAllCommand();
        }
    },
    TAKE_ALL_DIRECTORS{
        /**
         * Conducts to {@code DirectorsTakeAllCommand} class.
         * create an instance of {@code DirectorsTakeAllCommand} class.
         */
        {
            this.command = new DirectorTakeAllCommand();
        }
    },
    GOTO_USER_PAGE {
        /**
         * Conducts to {@code GoToUserPageCommand} class.
         * create an instance of {@code GoToUserPageCommand} class.
         */
        {
            this.command = new GoToUserPageCommand();
        }
    },
    CHANGE_PASSWORD {
        /**
         * Conducts to {@code ChangePasswordCommand} class.
         * create an instance of {@code ChangePasswordCommand} class.
         */
        {
            this.command = new ChangePasswordCommand();
        }
    },
    GOTO_UPDATE_PAGE {
        /**
         * Conducts to {@code GoToMovieUpdatePageCommand} class.
         * create an instance of {@code GoToMovieUpdatePageCommand} class.
         */
        {
            this.command = new GoToMovieUpdatePageCommand();
        }
    },
    GOTO_LOGIN_PAGE {
        /**
         * Conducts to {@code GoToLoginPageCommand} class.
         * create an instance of {@code GoToLoginPageCommand} class.
         */
        {
            this.command = new GoToLoginPageCommand();
        }
    },
    GOTO_REGISTER_PAGE {
        /**
         * Conducts to {@code GoToRegisterPageCommand} class.
         * create an instance of {@code GoToRegisterPageCommand} class.
         */
        {
            this.command = new GoToRegisterPageCommand();
        }
    },
    GOTO_MAIN_PAGE {
        /**
         * Conducts to {@code GoToMainPageCommand} class.
         * create an instance of {@code GoToMainPageCommand} class.
         */
        {
            this.command = new GoToMainPageCommand();
        }
    },
    ADD_ACTOR {
        /**
         * Conducts to {@code ActorAddCommand} class.
         * create an instance of {@code ActorAddCommand} class.
         */
        {
            this.command = new ActorAddCommand();
        }
    },
    ADD_DIRECTOR {
        /**
         * Conducts to {@code ActorAddCommand} class.
         * create an instance of {@code ActorAddCommand} class.
         */ {
            this.command = new DirectorAddCommand();
        }
    },
    DELETE_ACTOR {
        /**
         * Conducts to {@code ActorDeleteCommand} class.
         * create an instance of {@code ActorDeleteCommand} class.
         */
        {
            this.command = new ActorDeleteCommand();
        }
    },
    DELETE_GENRE {
        /**
         * Conducts to {@code GenreDeleteCommand} class.
         * create an instance of {@code GEnreDeleteCommand} class.
         */
        {
            this.command = new GenreDeleteCommand();
        }
    },
    DELETE_COUNTRY {
        /**
         * Conducts to {@code CountryDeleteCommand} class.
         * cretae an instance of {@code CountryDeleteCommand} class.
         */
        {
            this.command = new CountryDeleteCommand();
        }
    },
    DELETE_MOVIE {
        /**
         * Conducts to {@code MovieDeleteCommand} class.
         * create an instance of {@code MovieDeleteCommand} class.
         */
        {
            this.command = new MovieDeleteCommand();
        }
    },
    DELETE_GENRE_FROM_MOVIE {
        /**
         * Conducts to {@code GenreMovieDeleteCommand} class.
         * create an instance of {@code GenreMovieDeleteCommand} class.
         */
        {
            this.command = new GenreMovieDeleteCommand();
        }
    },
    DELETE_ACTOR_FROM_MOVIE {
        /**
         * Conducts to {@code ActorMovieDeleteCommand} class.
         * create an instance of {@code ActorMovieDeleteCommand} class.
         */
        {

            this.command = new ActorMovieDeleteCommand();
        }
    },
    UPDATE_MOVIE {
        /**
         * Conducts to {@code MovieUpdateCommand} class.
         * create an instance of {@code MovieUpdateCommand} class.
         */
        {
            this.command = new MovieUpdateCommand();
        }
    },
    BAN_UNBAN {
        /**
         * Conducts to {@code BanUnbanCommand} class.
         * create an instance of {@code BanUnbanComman} class.
         */
        {
            this.command = new BanUnbanCommand();
        }
    },
    ADD_AVATAR {
        /**
         * Conducts to {@code AvatarAddCommand} class.
         * create an instance of {@code AvatarAddCommand} class.
         */
        {
            this.command = new AvatarAddCommand();
        }
    },
    ADD_MOVIE_RATING{
        /**
         * Conducts to {@code MovieRatingAddCommand} class.
         * create an instance of {@code MovieRatingAddCommand} class.
         */
        {
            this.command = new MovieRatingAddCommand();
        }
    },
    TAKE_TOP{
        /**
         * Conducts to {@code TakeTopCommand} class.
         * create an instance of {@code TakeTopCommand} class.
         */
        {
            this.command = new TakeTopCommand();
        }
    },
    MOVIE {
        /**
         * Conducts to {@code GoToMoviePageCommand} class.
         * create an instance of {@code MovieUpdateCommand} class.
         */
        {

            this.command = new GoToMoviePageCommand();
        }
    },
    SEARCH {
        /**
         * Conducts to {@code SearchCommand} class.
         * create an instance of {@code SearchCommand} class.
         */
        {
            this.command = new SearchCommand();
        }
    },
    MOVIE_BY_ACTOR {
        /**
         * Conducts to {@code MovieByActorCommand} class.
         * create an instance of {@code MovieByActorCommand} class.
         */
        {
            this.command = new MoviesByActorCommand();
        }
    },
    MOVIE_BY_DIRECTOR {
        /**
         * Conducts to {@code MoviesByDirectorCommand} class.
         * create an instance of {@code MoviesByDirectorCommand} class.
         */
        {
            this.command = new MoviesByDirectorCommand();
        }
    },
    MOVIE_ADD {
        /**
         * Conducts to {@code MovieAddCommand} class.
         * create an instance of {@code MovieAddCommand} class.
         */
        {
            this.command = new MovieAddCommand();
        }
    },
    REVIEW_ADD {
        /**
         * Conducts to {@code ReviewAddCommand} class.
         * create an instance of {@code ReviewAddCommand} class.
         */
        {
            this.command = new ReviewAddCommand();
        }
    },
    REVIEW_MARK_DEC {
        /**
         * Conducts to {@code ReviewMarkDecCommand} class.
         * create an instance of {@code ReviewMarkDecCommand} class.
         */
        {
            this.command = new ReviewMarkDecCommand();
        }
    },
    REVIEW_MARK_INC {
        /**
         * Conducts to {@code ReviewMarkIncCommand} class.
         * create an instance of {@code ReviewMarkIncCommand} class.
         */
        {
            this.command = new ReviewMarkIncCommand();
        }
    },
    NEXT_PAGE {
        /**
         * Conducts to {@code NextPageCommand} class.
         * create an instance of {@code NextPageCommand} class.
         */
        {
            this.command = new NextPageCommand();
        }
    },
    PREVIOUS_PAGE {
        /**
         * Conducts to {@code PreviousPageCommand} class.
         * create an instance of {@code PreviousPageCommand} class.
         */
        {
            this.command = new PreviousPageCommand();
        }
    },
    REVIEW_DELETE {
        /**
         * Conducts to {@code ReviewDeleteCommand} class.
         * create an instance of {@code ReviewDeleteCommand} class.
         */
        {
            this.command = new ReviewDeleteCommand();
        }
    };
    ActionCommand command;

    /**
     * @return instance of required command
     */
    public ActionCommand getCurrentCommand() {
        return command;
    }
}

