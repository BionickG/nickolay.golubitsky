package by.epam.movierating.command.navigation;

import by.epam.movierating.command.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Nikolay Golubitsky
 * The {@code NextPageCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class NextPageCommand implements ActionCommand {
    private final static String PAGE = "page";
    private static final String PAGE_NUMBER = "pageNumber";
    private static final String NUMBER_OF_PAGES = "numberOfPages";

    /**
     * Forwards you to next page when you use paginator
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession();
        page = (String) session.getAttribute(PAGE);
        int current = (int) session.getAttribute(PAGE_NUMBER);
        int numberOfPages = (int) session.getAttribute(NUMBER_OF_PAGES);
        if (current + 1 != numberOfPages) {
            current++;
        }
        session.setAttribute(PAGE_NUMBER, current);
        return page;
    }
}
