package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.ActorServiceImpl;
import by.epam.movierating.service.MovieServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code ActorMovieDeleteCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */
public class ActorMovieDeleteCommand implements ActionCommand {
    private final static String DELETE_ACTOR_FAIL = "message.actor.movie.delete.fail";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String MOVIE_ID = "movieId";
    private final static String ACTOR_ID = "actorId";
    private final static String MESSAGE = "message";
    private final static String MOVIE = "movie";
    private final static String PAGE = "page";
    private final static String USER = "user";

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Delete the actor from the movie
     * @param request is an instance of {@code HttpServletRequest}.
     * @return update movie page after deleting actor from the staff
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession();
        page = (String) session.getAttribute(PAGE);
        Long movieId = Long.valueOf(request.getParameter(MOVIE_ID));
        Long actorId = Long.valueOf(request.getParameter(ACTOR_ID));
        MovieServiceImpl movieService = new MovieServiceImpl();
        ActorServiceImpl actorService = new ActorServiceImpl();
        if (((User)session.getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR))
        {
            try {
                movieService.deleteConnectionWithActorTable(movieId, actorId);
                Movie movie = (Movie) request.getSession().getAttribute(MOVIE);
                movie.setActors(actorService.takeActorsByMovieId(movieId));
                page = (String) request.getSession().getAttribute(PAGE);
            } catch (ServiceException e) {
                request.setAttribute(MESSAGE, MessageManager.getProperty(DELETE_ACTOR_FAIL, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
                LOGGER.error(e.getMessage());
            }
        }
        return page;
    }
}
