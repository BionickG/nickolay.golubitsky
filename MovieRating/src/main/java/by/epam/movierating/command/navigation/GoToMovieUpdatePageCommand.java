package by.epam.movierating.command.navigation;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.ActorServiceImpl;
import by.epam.movierating.service.GenreServiceImpl;
import by.epam.movierating.service.MovieServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Nikolay Golubitsky
 * The {@code GoToMovieUpdatePageCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class GoToMovieUpdatePageCommand implements ActionCommand {

    private static final String UPDATE_PAGE = "path.page.updatePage";
    private static final String OOPS_PAGE = "path.page.oops";
    private static final String LAST_GO_TO_COMMAND = "lastCommand";
    private static final String PARAM_MOVIE_ID = "movieId";
    private static final String COMMAND = "command";
    private static final String MOVIE = "movie";
    private static final String PAGE = "page";

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Forwards you to movie update page and fill in list of movie's genres
     * and actors
     * @param request is an instance of {@code HttpServletRequest}.
     * @return movie update page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page;

        MovieServiceImpl movieService = new MovieServiceImpl();
        ActorServiceImpl actorService = new ActorServiceImpl();
        GenreServiceImpl genreService = new GenreServiceImpl();
        page = ConfigurationManager.getProperty(UPDATE_PAGE);
        HttpSession session = request.getSession();
        session.setAttribute(LAST_GO_TO_COMMAND, request.getParameter(COMMAND));
        Long movieId;
        String movieIdString = request.getParameter(PARAM_MOVIE_ID);
        if (movieIdString != null) {
            movieId = Long.valueOf(movieIdString);
        } else {
            return page;
        }
        try {
            Movie movie = movieService.take(movieId);
            movie.setActors(actorService.takeActorsByMovieId(movieId));
            movie.setGenres(genreService.takeAllGenresOfMovie(movieId));

            session.setAttribute(MOVIE, movie);
            session.setAttribute(PAGE, page);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }
        return page;
    }
}
