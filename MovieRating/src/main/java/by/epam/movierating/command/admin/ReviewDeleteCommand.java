package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Review;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.ReviewServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code ReviewDeleteCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class ReviewDeleteCommand implements ActionCommand {
    private static final String REVIEW_DELETE_FAIL_MESSAGE = "message.review.delete.fail";
    private static final String REVIEW_DELETE_MESSAGE = "message.review.delete";
    private static final String MOVIE_PAGE = "path.page.movie";
    private static final String MESSAGE_BUNDLE = "bundle";
    private static final String REVIEW_ID = "reviewId";
    private static final String MESSAGE = "message";
    private static final String MOVIE = "movie";
    private static final String USER = "user";

    /**
     * Delete review from the movie page.
     * @param request is an instance of {@code HttpServletRequest}.
     * @return movie page after deleting the review
     */

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String page = ConfigurationManager.getProperty(MOVIE_PAGE);
        ReviewServiceImpl reviewService = new ReviewServiceImpl();
        Long reviewId = Long.valueOf(request.getParameter(REVIEW_ID));
        if (((User)request.getSession().getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR)) {
            try {
                reviewService.delete(reviewId);
                Movie movie = (Movie) session.getAttribute(MOVIE);
                List<Review> reviews = reviewService.takeAllReviewsByMovieId(movie.getMovieId());
                movie.setReviews(reviews);
                session.setAttribute(MOVIE, movie);
                request.setAttribute(MESSAGE, MessageManager.getProperty(REVIEW_DELETE_MESSAGE,
                        (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            } catch (ServiceException e) {
                e.printStackTrace();
                request.setAttribute(MESSAGE, MessageManager.getProperty(REVIEW_DELETE_FAIL_MESSAGE,
                        (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            }
        }
        return page;
    }
}
