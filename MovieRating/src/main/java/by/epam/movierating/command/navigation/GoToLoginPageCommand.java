package by.epam.movierating.command.navigation;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Nikolay Golubitsky
 * The {@code GoToLoginPageCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class GoToLoginPageCommand implements ActionCommand {
    private static final String LOGIN_PAGE = "path.page.login";
    private static final String PAGE = "page";

    /**
     * Forwards you to login page
     * @param request is an instance of {@code HttpServletRequest}.
     * @return login page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(LOGIN_PAGE);
        HttpSession session = request.getSession();
        session.setAttribute(PAGE, page);
        return page;
    }
}
