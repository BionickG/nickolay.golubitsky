package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Rating;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.MovieServiceImpl;
import by.epam.movierating.service.RatingServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

/**
 * @author Nikolay Golubitsky
 * The {@code TakeTopCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class TakeTopCommand implements ActionCommand {
    private static final String NUMBER_OF_PAGES = "numberOfPages";
    private static final String MAIN_PAGE = "path.page.main";
    private static final String OOPS_PAGE = "path.page.oops";
    private static final String PAGE_NUMBER = "pageNumber";
    private final static String DIRECTORS = "directors";
    private static final String MOVIES = "movies";
    private final static String ACTORS = "actors";
    private static final String PAGE = "page";
    private static final int CURRENT_PAGE = 0;

    private static final Logger LOG = LogManager.getLogger();
    
    /**
     * Return list of top 20 movies
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page with result list of movies
     */
    
    @Override
    public String execute(HttpServletRequest request) {
        MovieServiceImpl movieService = new MovieServiceImpl();
        RatingServiceImpl ratingService = new RatingServiceImpl();
        String page = ConfigurationManager.getProperty(MAIN_PAGE);
        HttpSession session = request.getSession();

        List<Movie> top = null;
        try {
            top = movieService.takeAll();
            for (Movie movie : top) {
                Rating rating = ratingService.take(movie.getMovieId());
                double ratingAvg = ratingService.getAvgRatingOfMovie(rating);
                movie.setRating(ratingAvg);
            }
            int numberOfPages = top.size() / 10;
            if (top.size() % 10 != 0) {
                ++numberOfPages;
            }
            session.setAttribute(PAGE_NUMBER, CURRENT_PAGE);
            session.setAttribute(NUMBER_OF_PAGES, numberOfPages);
        } catch (ServiceException e) {
            LOG.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }
        Collections.sort(top, (o1, o2) -> {
            double rating1 = o1.getRating();
            double rating2 = o2.getRating();
            return Double.compare(rating2, rating1);

        });
        if (top.size() > 20) {
            top = top.subList(0, 19);
        }

        session.setAttribute(MOVIES, top);
        session.setAttribute(PAGE, page);
        session.setAttribute(ACTORS, null);
        session.setAttribute(DIRECTORS, null);
        return page;
    }
}
