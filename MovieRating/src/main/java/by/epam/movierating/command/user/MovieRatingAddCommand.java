package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Rating;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.RatingServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Nikolay Golubitsky
 * The {@code MovieRatingAddCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class MovieRatingAddCommand implements ActionCommand {

    private final static String OOPS_PAGE = "path.page.oops";
    private static final String USER_RATING = "userRating";
    private static final String MOVIE_ID = "movieId";
    private static final String RATING = "rating";
    private static final String MOVIE = "movie";
    private static final String PAGE = "page";
    private static final String USER = "user";

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Add user mark to the movie
     * @param request is an instance of {@code HttpServletRequest}.
     * @return the page you were redirected
     */

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        User user = (User) session.getAttribute(USER);
        Movie movie = (Movie) session.getAttribute(MOVIE);
        Long userId = user.getUserId();
        RatingServiceImpl ratingService = new RatingServiceImpl();
        int rating = Integer.parseInt(request.getParameter(RATING));
        Long movieId = Long.valueOf(request.getParameter(MOVIE_ID));
        try {
            ratingService.addUserRating(movieId, rating);
            ratingService.addRatingWithConnection(userId, movieId, rating);
            double userRating = ratingService.takeUserMark(user.getUserId(), movieId);
            Rating ratingNew = ratingService.take(movie.getMovieId());
            movie.setRating(ratingService.getAvgRatingOfMovie(ratingNew));
            session.setAttribute(MOVIE, movie);
            session.setAttribute(USER_RATING, userRating);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }
        return page;
    }
}
