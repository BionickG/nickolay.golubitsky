package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Review;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.ReviewServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Nikolay Golubitsky
 * The {@code ReviewAddCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class ReviewAddCommand implements ActionCommand {
    private final static String OOPS_PAGE = "path.page.oops";
    private static final String COMMENT = "comment";
    private static final String MOVIE = "movie";
    private static final String PAGE = "page";
    private static final String USER = "user";

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Add review to the current movie
     * @param request is an instance of {@code HttpServletRequest}.
     * @return movie page with new list of reviews
     */

    @Override
    public String execute(HttpServletRequest request) {
        ReviewServiceImpl reviewService = new ReviewServiceImpl();
        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        Review review = new Review();
        User user = (User) session.getAttribute(USER);
        review.setUser(user);
        review.setReviewDate(LocalDate.now());
        Movie movie = (Movie) session.getAttribute(MOVIE);
        review.setMovieId(movie.getMovieId());
        String comment = request.getParameter(COMMENT);
        if (!"".equals(comment)){
            review.setReview(comment);
            try {
                reviewService.add(review);
                List<Review> reviews = reviewService.takeAllReviewsByMovieId(movie.getMovieId());
                movie.setReviews(reviews);
                session.setAttribute(MOVIE, movie);
            } catch (ServiceException e) {
                LOGGER.error(e.getMessage());
                page = ConfigurationManager.getProperty(OOPS_PAGE);
            }
        }

        return page;
    }
}
