package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.MovieServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code MovieDeleteCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */
public class MovieDeleteCommand implements ActionCommand {
    private final static String SUCCESS_MESSAGE = "message.movie.delete.success";
    private final static String FAIL_MESSAGE = "message.movie.delete.fail";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String MOVIE_ID = "movieId";
    private final static String MESSAGE = "message";
    private final static String MOVIES = "movies";
    private final static String PAGE = "page";
    private final static String USER = "user";

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Delete the movie.
     * @param request is an instance of {@code HttpServletRequest}.
     * @return admin page after deleting the movie
     */

    @Override
    public String execute(HttpServletRequest request) {
        MovieServiceImpl movieService = new MovieServiceImpl();
        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        if (((User)request.getSession().getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR))
        {
            try {
                Long movieId = Long.valueOf(request.getParameter(MOVIE_ID));
                movieService.delete(movieId);
                session.setAttribute(MOVIES, movieService.takeAll());
                request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS_MESSAGE,
                        (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
                LOGGER.info("Movie" + movieId + "has been deleted");
            } catch (ServiceException e) {
                request.setAttribute(MESSAGE, FAIL_MESSAGE);
                LOGGER.error(e.getMessage());
            }
        }
        return page;
    }

}
