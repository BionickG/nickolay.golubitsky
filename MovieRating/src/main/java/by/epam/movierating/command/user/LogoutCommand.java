package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nikolay Golubitsky
 * The {@code LogoutCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class LogoutCommand implements ActionCommand {
    private final static String INDEX_PAGE = "path.page.index";

    /**
     * Log out from the system and invalidate session
     * @param request is an instance of {@code HttpServletRequest}.
     * @return index page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(INDEX_PAGE);
        request.getSession().invalidate();
        return page;
    }
}
