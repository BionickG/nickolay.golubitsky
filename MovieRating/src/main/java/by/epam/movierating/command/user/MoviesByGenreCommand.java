package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.MovieServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code MoviesByGenreCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class MoviesByGenreCommand implements ActionCommand {
    private static final String NUMBER_OF_PAGES = "numberOfPages";
    private static final String NO_RESULTS = "message.no.results";
    private final static String MAIN_PAGE = "path.page.main";
    private final static String OOPS_PAGE = "path.page.oops";
    private static final String PAGE_NUMBER = "pageNumber";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String GENRE_ID = "genreId";
    private final static String MESSAGE = "message";
    private final static String MOVIES = "movies";
    private final static String ACTORS = "actors";
    private final static String PAGE = "page";
    private static final int CURRENT_PAGE = 0;


    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Take all movies of current genre
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page with current genre movies
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(MAIN_PAGE);
        HttpSession session = request.getSession();
        MovieServiceImpl movieService = new MovieServiceImpl();
        Long genreId = Long.valueOf(request.getParameter(GENRE_ID));
        List<Movie> movies;
        try {
            movies = movieService.takeMovieByGenre(genreId);
            if (movies.size() == 0) {
                request.setAttribute(MESSAGE,
                        MessageManager.getProperty(NO_RESULTS, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            }
            int numberOfPages = movies.size()/10;
            if (movies.size()%10 !=0){
                ++numberOfPages;
            }
            session.setAttribute(MOVIES, movies);
            session.setAttribute(ACTORS, null);
            session.setAttribute(PAGE, page);
            session.setAttribute(PAGE_NUMBER, CURRENT_PAGE);
            session.setAttribute(NUMBER_OF_PAGES, numberOfPages);

        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }
        return page;
    }
}
