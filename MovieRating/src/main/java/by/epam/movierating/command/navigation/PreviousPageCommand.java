package by.epam.movierating.command.navigation;

import by.epam.movierating.command.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Nikolay Golubitsky
 * The {@code PreviousPageCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class PreviousPageCommand implements ActionCommand {
    private final static String PAGE = "page";
    private static final String PAGE_NUMBER = "pageNumber";

    /**
     * Forwards you to previous page when you use paginator
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession();
        page = (String) session.getAttribute(PAGE);
        int current = (int) session.getAttribute(PAGE_NUMBER);
        if (current > 0) {
            current--;
        }

        session.setAttribute(PAGE_NUMBER, current);
        return page;
    }
}
