package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Rating;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.MovieServiceImpl;
import by.epam.movierating.service.RatingServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code MoviesByActorCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class MoviesByActorCommand implements ActionCommand {


    private static final String NUMBER_OF_PAGES = "numberOfPages";
    private static final String NO_RESULTS = "message.no.results";
    private static final String MAIN_PAGE = "path.page.main";
    private final static String OOPS_PAGE = "path.page.oops";
    private static final String PAGE_NUMBER = "pageNumber";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String DIRECTORS = "directors";
    private static final String ACTOR_ID = "actorId";
    private final static String MESSAGE = "message";
    private static final String MOVIES = "movies";
    private final static String ACTORS = "actors";
    private static final String PAGE = "page";
    private static final int CURRENT_PAGE = 0;

    private static final Logger LOGGER = LogManager.getLogger();

     /**
     * Take all movies of current actor
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page with current actor movies
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession();
        page = ConfigurationManager.getProperty(MAIN_PAGE);
        MovieServiceImpl movieService = new MovieServiceImpl();
        RatingServiceImpl ratingService = new RatingServiceImpl();
        Long actorId = Long.valueOf(request.getParameter(ACTOR_ID));

        try {
            List<Movie> movies = movieService.takeMoviesByActorId(actorId);
            if (movies.size() == 0) {
                request.setAttribute(MESSAGE,
                        MessageManager.getProperty(NO_RESULTS, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            }
            for (Movie movie : movies){
                Rating rating = ratingService.take(movie.getMovieId());
                double ratingAvg = ratingService.getAvgRatingOfMovie(rating);
                movie.setRating(ratingAvg);
            }
            int numberOfPages = movies.size()/10;
            if (movies.size()%10 !=0 || numberOfPages == 0){
                ++numberOfPages;
            }
            session.setAttribute(PAGE, page);
            session.setAttribute(MOVIES, movies);
            session.setAttribute(ACTORS, null);
            session.setAttribute(DIRECTORS, null);
            session.setAttribute(PAGE_NUMBER, CURRENT_PAGE);
            session.setAttribute(NUMBER_OF_PAGES, numberOfPages);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }

        return page;
    }
}
