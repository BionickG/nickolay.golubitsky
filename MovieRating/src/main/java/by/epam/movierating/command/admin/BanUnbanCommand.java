package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code BanUnbanCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */
public class BanUnbanCommand implements ActionCommand {

    private final static String SUCCESS_MESSAGE = "message.user.ban.success";
    private final static String FAIL_MESSAGE = "message.user.ban.fail";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String MESSAGE = "message";
    private final static String USER_ID = "userId";
    private final static String USERS = "users";
    private final static String PAGE = "page";
    private final static String USER = "user";

    private final static Logger LOGGER = LogManager.getLogger();
    /**
     * Ban the user or unban if he(she) has been already banned
     * @param request is an instance of {@code HttpServletRequest}.
     * @return admin page after bar or unban user
     */

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        UserServiceImpl userService = new UserServiceImpl();
        if (((User)session.getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR))
        {
            try {
                Long userId = Long.valueOf(request.getParameter(USER_ID));
                userService.banUnban(userId);
                session.setAttribute(USERS, userService.takeAll());
                request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            } catch (ServiceException e) {
                request.setAttribute(MESSAGE, FAIL_MESSAGE);
                LOGGER.error(e.getMessage());
            }
        }
        return page;

    }
}
