package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.*;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.*;
import by.epam.movierating.util.ImageUploading;
import by.epam.movierating.validation.DirectoryExistValidator;
import by.epam.movierating.validation.RegExpValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code MovieAddCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class MovieAddCommand implements ActionCommand {

    private final static String CANT_ADD_GENRE_MESSAGE = "message.movie.genre.add.fail";
    private final static String CANT_ADD_COVER_MESSAGE = "message.movie.cover.add.fail";
    private final static String MOVIE_ADD_FAIL_MESSAGE = "message.movie.add.fail";
    private final static String SUCCESS_MESSAGE = "message.movie.create.success";
    private final static String ACTOR_VALID_MESSAGE = "message.valid.actor.fail";
    private final static String NO_DIRECTOR_MESSAGE = "message.no.director";
    private final static String NOT_VALID_MESSAGE = "message.valid.fail";
    private final static String UPLOAD_DIRECTORY = "path.upload.covers";
    private final static String FIRST_LAST_NAME = "firstLastName";
    private final static String ERROR_MESSAGE = "errorMessage";
    private final static String TRAILER_LINK = "trailerLink";
    private final static String DESCRIPTION = "description";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String DURATION = "duration";
    private final static String DIRECTOR = "director";
    private final static String GENRES = "genres[]";
    private final static String ACTORS = "actors[]";
    private final static String COUNTRY = "country";
    private final static String MESSAGE = "message";
    private final static String MOVIES = "movies";
    private final static String GENRE = "genre";
    private final static String NAME = "name";
    private final static String YEAR = "year";
    private final static String PAGE = "page";
    private final static String USER = "user";
    private final static String COVER = "cover";

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Add new movie to the system
     * @param request is an instance of {@code HttpServletRequest}.
     * @return admin page after addnig the movie
     */

    @Override
    public String execute(HttpServletRequest request) {
        MovieServiceImpl movieService = new MovieServiceImpl();
        CountryServiceImpl countryService = new CountryServiceImpl();
        DirectorServiceImpl directorService = new DirectorServiceImpl();
        ActorServiceImpl actorService = new ActorServiceImpl();
        GenreServiceImpl genreService = new GenreServiceImpl();

        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        Movie movie = new Movie();
        List<String> errorMessages = new LinkedList<>();
        RegExpValidator valid = new RegExpValidator();

        String name = request.getParameter(NAME);
        String trailerLink = request.getParameter(TRAILER_LINK);
        String duration = request.getParameter(DURATION);
        String description = request.getParameter(DESCRIPTION);
        String countryName = request.getParameter(COUNTRY);
        String year = request.getParameter(YEAR);
        String directorName = request.getParameter(DIRECTOR);

        if (valid.checkData(NAME, name) && valid.checkData(YEAR, year) && valid.checkData(DURATION, duration)
                && valid.checkData(COUNTRY, countryName) && (((User)request.getSession().getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR))){
            movie.setName(name);
            movie.setTrailer(trailerLink);
            int durationInt = Integer.parseInt(duration);
            movie.setDuration(durationInt);
            movie.setDescription(description);
            int yearInt = Integer.parseInt(year);
            movie.setYear(yearInt);
            try {
                movie.setCountry(countryService.takeCountryByNameWithCreation(countryName));
                if (valid.checkData(FIRST_LAST_NAME, directorName)) {
                    movie.setDirector(directorService.takeDirectorByFullName(directorName));
                } else {
                    errorMessages.add(MessageManager.getProperty(NO_DIRECTOR_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
                }

                movie.setDateAdded(LocalDateTime.now());
                movieService.add(movie);
                Long movieId = (movieService.takeMovieByNameAndYear(movie.getName(), movie.getYear())).getMovieId();
                movie.setMovieId(movieId);
                List<String> actorNames = Arrays.asList(request.getParameterValues(ACTORS));
                for (String actorName : actorNames){
                    if (valid.checkData(FIRST_LAST_NAME, actorName)) {
                        Actor actor = actorService.takeActorByFullName(actorName);
                        movieService.addConnectionWithActorTable(movieId, actor.getActorId());
                    } else {
                        if (!"".equals(actorName)){
                            errorMessages.add(MessageManager.getProperty(ACTOR_VALID_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
                        }
                    }
                }
                List<String> genreNames = Arrays.asList(request.getParameterValues(GENRES));
                for (String genreName : genreNames) {
                    if (valid.checkData(GENRE, genreName)) {
                        Genre genre = genreService.takeByNameWithCreation(genreName);
                        movieService.addConnectionWithGenreTable(movieId, genre.getGenreId());
                    } else {
                        if (!"".equals(genreName)){
                            errorMessages.add(MessageManager.getProperty(CANT_ADD_GENRE_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
                        }
                    }
                }
                String appPath = request.getServletContext().getRealPath("");
                String directory = ConfigurationManager.getProperty(UPLOAD_DIRECTORY);
                String fileName = movieId.toString() + ".jpg";
                String path = DirectoryExistValidator.isDirectoryExist(appPath, directory) + File.separator + fileName;
                try(InputStream in = request.getPart(COVER).getInputStream();
                    FileOutputStream fos = new FileOutputStream(path)) {
                    movie.setCover(ImageUploading.upload(in, fos, directory, fileName));
                } catch (IOException | ServletException e) {
                    LOGGER.error(e.getMessage());
                    errorMessages.add(MessageManager.getProperty(CANT_ADD_COVER_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
                }
                movieService.update(movieId, movie);
                session.setAttribute(MOVIES, movieService.takeAll());
                request.setAttribute(ERROR_MESSAGE, errorMessages);
                request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
                LOGGER.info("Movie" + movie + "has been created");
            } catch (ServiceException e) {
                LOGGER.error(e.getMessage());
                page = (String) session.getAttribute(PAGE);
                request.setAttribute(MESSAGE, MessageManager.getProperty(MOVIE_ADD_FAIL_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            }
        }else {
            page = (String) session.getAttribute(PAGE);
            request.setAttribute(MESSAGE, MessageManager.getProperty(NOT_VALID_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
        }
        return page;
    }
}
