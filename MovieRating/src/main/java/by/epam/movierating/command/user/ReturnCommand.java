package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nikolay Golubitsky
 * The {@code ReturnCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class ReturnCommand implements ActionCommand {
    private static final String MAIN_PAGE = "path.page.main";
    private static final String PAGE = "page";

    /**
     * Returns to main page
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page =ConfigurationManager.getProperty(MAIN_PAGE);
        request.getSession().setAttribute(PAGE, page);
        return page;
    }
}
