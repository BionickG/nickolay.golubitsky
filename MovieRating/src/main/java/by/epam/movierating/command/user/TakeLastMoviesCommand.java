package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Rating;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.MovieServiceImpl;
import by.epam.movierating.service.RatingServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Nikolay Golubitsky
 * The {@code TakeLastMoviesCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class TakeLastMoviesCommand implements ActionCommand {
    
    private static final String NUMBER_OF_PAGES = "numberOfPages";
    private static final String MAIN_PAGE = "path.page.main";
    private static final String PAGE_NUMBER = "pageNumber";
    private final static String DIRECTORS = "directors";
    private final static String ACTORS = "actors";
    private static final String MOVIES = "movies";
    private static final String PAGE = "page";
    private static final int CURRENT_PAGE = 0;
    
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Return list of last added movies
     * @param request is an instance of {@code HttpServletRequest}.
     * @return main page with result list of movies
     */
    
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        MovieServiceImpl movieService = new MovieServiceImpl();

        RatingServiceImpl ratingService = new RatingServiceImpl();
        String page = ConfigurationManager.getProperty(MAIN_PAGE);
        List<Movie> movies;

        try {
            movies = movieService.takeLast();
            for (Movie movie : movies){
                Rating rating = ratingService.take(movie.getMovieId());
                double ratingAvg = ratingService.getAvgRatingOfMovie(rating);
                movie.setRating(ratingAvg);
            }
            int numberOfPages = movies.size()/10;
            if (movies.size()%10 !=0){
                ++numberOfPages;
            }

            session.setAttribute(PAGE, page);
            session.setAttribute(ACTORS, null);
            session.setAttribute(DIRECTORS, null);
            session.setAttribute(MOVIES, movies);

            session.setAttribute(PAGE_NUMBER, CURRENT_PAGE);
            session.setAttribute(NUMBER_OF_PAGES, numberOfPages);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
        }
        return page;
    }
}
