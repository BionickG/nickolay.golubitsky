package by.epam.movierating.command.user;

import by.epam.movierating.command.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code LanguageCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class LanguageCommand implements ActionCommand {

    private static final String LINK_EN = "messages_en_US";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String RUS_LOCALE = "ru_RU";
    private final static String ENG_LOCALE = "en_US";
    private static final String LINK_RU = "messages";
    private final static String LANGUAGE = "lang";
    private final static String LOCALE = "locale";
    private final static String PAGE = "page";
    private final static String EN = "en";
    private final static String RU = "ru";

    /**
     * Change language of the interface
     * @param request is an instance of {@code HttpServletRequest}.
     * @return the page you were redirected
     */

    @Override
    public String execute(HttpServletRequest request) {
        String language = request.getParameter(LANGUAGE);
        String pageAttr = (String) request.getSession().getAttribute(PAGE);
        HttpSession session = request.getSession();
        switch (language) {
            case EN:
                session.setAttribute(LOCALE, ENG_LOCALE);
                session.setAttribute(MESSAGE_BUNDLE, ResourceBundle.getBundle(LINK_EN));
                break;
            case RU:
                session.setAttribute(LOCALE, RUS_LOCALE);
                session.setAttribute(MESSAGE_BUNDLE, ResourceBundle.getBundle(LINK_RU));
                break;
        }
        return pageAttr;
    }
}
