package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Movie;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.GenreServiceImpl;
import by.epam.movierating.service.MovieServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code GenreMovieDeleteCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class GenreMovieDeleteCommand implements ActionCommand{
    private final static String DELETE_GENRE_FAIL = "message.genre.movie.delete.fail";
    private final static String ADMIN_PAGE = "path.page.adminPage";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String MESSAGE = "message";
    private final static String MOVIE_ID = "movieId";
    private final static String GENRE_ID = "genreId";
    private final static String MOVIE = "movie";
    private final static String PAGE = "page";
    private final static String USER = "user";

    private final static Logger LOGGER = LogManager.getLogger();

     /**
     * Delete the genre from the movie.
     * @param request is an instance of {@code HttpServletRequest}.
     * @return movie update page after deleting the genre from the movie
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page = (String) request.getSession().getAttribute(PAGE);
        Long movieId = Long.valueOf(request.getParameter(MOVIE_ID));
        Long genreId = Long.valueOf(request.getParameter(GENRE_ID));
        MovieServiceImpl movieService = new MovieServiceImpl();
        GenreServiceImpl genreService = new GenreServiceImpl();
        if (((User)request.getSession().getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR)) {
            try {
                movieService.deleteConnectionWithGenreTable(movieId, genreId);
                Movie movie = (Movie) request.getSession().getAttribute(MOVIE);
                movie.setGenres(genreService.takeAllGenresOfMovie(movieId));

            } catch (ServiceException e) {
                page = ConfigurationManager.getProperty(ADMIN_PAGE);
                request.getSession().setAttribute(PAGE, page);
                request.setAttribute(MESSAGE, MessageManager.getProperty(DELETE_GENRE_FAIL,
                        (ResourceBundle) request.getSession().getAttribute(MESSAGE_BUNDLE)));
                LOGGER.error(e.getMessage());
            }
        }
        return page;
    }
}
