package by.epam.movierating.command.navigation;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.entity.*;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * @author Nikolay Golubitsky
 * The {@code GoToMoviePageCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class GoToMoviePageCommand implements ActionCommand {

    private static final String LAST_GO_TO_COMMAND = "lastCommand";
    private static final String MOVIE_PAGE = "path.page.movie";
    private static final String OOPS_PAGE = "path.page.oops";
    private static final String USER_RATING = "userRating";
    private static final String PARAM_MOVIE_ID = "movieId";
    private static final String COMMAND = "command";
    private static final String MOVIE = "movie";
    private static final String PAGE = "page";
    private static final String USER = "user";

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Forwards you to movie page and fill in list of movie's genres,
     * actors, reviews, mark of current user and average mark of movie
     * @param request is an instance of {@code HttpServletRequest}.
     * @return movie page
     */

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession();
        MovieServiceImpl movieService = new MovieServiceImpl();
        ActorServiceImpl actorService = new ActorServiceImpl();
        GenreServiceImpl genreService = new GenreServiceImpl();
        ReviewServiceImpl reviewService = new ReviewServiceImpl();
        RatingServiceImpl ratingService = new RatingServiceImpl();
        CountryServiceImpl countryService = new CountryServiceImpl();
        session.setAttribute(LAST_GO_TO_COMMAND, request.getParameter(COMMAND));
        page = ConfigurationManager.getProperty(MOVIE_PAGE);
        Long movieId;
        String movieIdString = request.getParameter(PARAM_MOVIE_ID);
        if (movieIdString != null) {
            movieId = Long.valueOf(movieIdString);
        } else {
            return page;
        }
        User user = (User)session.getAttribute(USER);
        Movie movie;
        double userRating = 0;
        try {
            if (user.getUserId() != null){
                userRating = ratingService.takeUserMark(user.getUserId(), movieId);
            }
            session.setAttribute(USER_RATING, userRating);
            movie = movieService.take(movieId);
            if (movie == null) {
                return ConfigurationManager.getProperty(OOPS_PAGE);
            }
            movie.setActors(actorService.takeActorsByMovieId(movieId));
            Rating rating = ratingService.take(movie.getMovieId());
            double ratingAvg = ratingService.getAvgRatingOfMovie(rating);
            double roundRating = new BigDecimal(ratingAvg).setScale(2, RoundingMode.UP).doubleValue();
            movie.setRating(roundRating);
            List<Review>reviews = reviewService.takeAllReviewsByMovieId(movieId);
            for (Review review : reviews){
                review.setRated(reviewService.isConnectionExist(user.getUserId(), review.getReviewId()));
            }
            Country country = countryService.takeCountryByNameWithCreation(movie.getCountry().getCountry());
            movie.setCountry(country);
            movie.setReviews(reviews);
            movie.setGenres(genreService.takeAllGenresOfMovie(movieId));
            session.setAttribute(MOVIE, movie);
            session.setAttribute(PAGE, page);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = ConfigurationManager.getProperty(OOPS_PAGE);
        }
        return page;
    }

}
