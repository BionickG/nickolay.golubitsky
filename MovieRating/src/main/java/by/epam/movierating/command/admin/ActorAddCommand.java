package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Actor;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.ActorServiceImpl;
import by.epam.movierating.validation.RegExpValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code ActorAddCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */
public class ActorAddCommand implements ActionCommand {
    private final static String SUCCESS_MESSAGE = "message.actor.create.success";
    private final static String FAIL_MESSAGE = "message.actor.create.fail";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String FIRST_NAME = "firstName";
    private final static String LAST_NAME = "lastName";
    private final static String MESSAGE = "message";
    private final static String ACTORS = "actors";
    private final static String PAGE = "page";
    private final static String USER = "user";

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Add new actor to the system
     * @param request is an instance of {@code HttpServletRequest}.
     * @return admin page after adding an actor
     */
    @Override
    public String execute(HttpServletRequest request) {
        ActorServiceImpl actorService = new ActorServiceImpl();
        RegExpValidator valid = new RegExpValidator();
        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        String firstName = request.getParameter(FIRST_NAME);
        String lastName = request.getParameter(LAST_NAME);
        if (((User)session.getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR))
        {
            try {

                if (valid.checkData(FIRST_NAME, firstName) && valid.checkData(LAST_NAME, lastName)) {
                    Actor actor = new Actor();
                    actor.setFirstName(firstName);
                    actor.setLastName(lastName);
                    actorService.add(actor);
                } else {
                    request.setAttribute(MESSAGE, FAIL_MESSAGE);
                }
                session.setAttribute(ACTORS, actorService.takeAll());
                request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS_MESSAGE, (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            } catch (ServiceException e) {
                request.setAttribute(MESSAGE, FAIL_MESSAGE);
                LOGGER.error(e.getMessage());
            }
        }
        return page;
    }
}
