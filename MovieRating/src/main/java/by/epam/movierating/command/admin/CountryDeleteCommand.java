package by.epam.movierating.command.admin;

import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.config.MessageManager;
import by.epam.movierating.entity.Role;
import by.epam.movierating.entity.User;
import by.epam.movierating.exception.ServiceException;
import by.epam.movierating.service.CountryServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

/**
 * @author Nikolay Golubitsky
 * The {@code CountryDeleteCommand} class represents an ability of manipulation
 * with an instance of {@code HttpServletRequest}.
 */

public class CountryDeleteCommand implements ActionCommand{

    private final static String SUCCESS_MESSAGE = "message.country.delete.success";
    private final static String FAIL_MESSAGE = "message.country.delete.fail";
    private static final String MESSAGE_BUNDLE = "bundle";
    private final static String COUNTRY_ID = "countryId";
    private final static String COUNTRIES = "countries";
    private final static String MESSAGE = "message";
    private final static String PAGE = "page";
    private final static String USER = "user";

    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * Delete the country from the system.
     * @param request is an instance of {@code HttpServletRequest}.
     * @return admin page after deleting the country
     */

    @Override
    public String execute(HttpServletRequest request) {
        CountryServiceImpl countryService = new CountryServiceImpl();
        HttpSession session = request.getSession();
        String page = (String) session.getAttribute(PAGE);
        if (((User)session.getAttribute(USER)).getRole().equals(Role.ADMINISTRATOR))
        {
            try {
                Long genreId = Long.valueOf(request.getParameter(COUNTRY_ID));
                countryService.delete(genreId);
                session.setAttribute(COUNTRIES, countryService.takeAll());
                request.setAttribute(MESSAGE, MessageManager.getProperty(SUCCESS_MESSAGE,
                        (ResourceBundle) session.getAttribute(MESSAGE_BUNDLE)));
            } catch (ServiceException e) {
                request.setAttribute(MESSAGE, FAIL_MESSAGE);
                LOGGER.error(e.getMessage());
            }
        }
        return page;
    }

}
