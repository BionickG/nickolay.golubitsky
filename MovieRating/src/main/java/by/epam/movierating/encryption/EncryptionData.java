package by.epam.movierating.encryption;


import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code EncryptionData} class represents an ability of encrypt data.
 */

public class EncryptionData {

    /**
     * Encrypt data.
     *
     * @param inputString is String parameter which will be encrypted.
     * @return encrypted String.
     */

    public static String getMD5(String inputString) {
        String md5Hex = DigestUtils.md5Hex(inputString);
        return md5Hex;
    }



}
