package by.epam.movierating.config;

import java.util.ResourceBundle;

/**
 * @author Nickolay Golubitsky
 * The {@code ConfigurationManager} class represents the way to use
 * notes from configuration property file.
 */
public class ConfigurationManager {

    private final static String RESOURCES_PATH = "config";
    private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(RESOURCES_PATH);

    /**
     * Gives the way to get from configuration file the
     * property we need.
     * @param   name means the name of a property we need.
     * @return  the value of property, which represents
     *          a file path.
     */
    public static String getProperty(String name) {
        return RESOURCE_BUNDLE.getString(name);
    }
}
