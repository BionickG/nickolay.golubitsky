package by.epam.movierating.config;

import java.util.ResourceBundle;

/**
 * @author Nickolay Golubitsky
 * The {@code MessageManager} class represents the way to select
 * messages in the desired language from message bundle
 */
public class MessageManager {

    private MessageManager() {
    }

    /**
     * Returns message
     * @param key is name of message in the bundle
     * @param resourceBundle is required bundle
     * @return message string in the desired language
     */
    public static String getProperty(String key, ResourceBundle resourceBundle) {
        return resourceBundle.getString(key);
    }
}
