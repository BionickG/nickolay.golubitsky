package by.epam.movierating.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code DirectoryExistValidator} class represents the way to
 * uploading files to application
 */
public class ImageUploading {

    /**
     * Upload file to the application
     *
     * @param ins       is instance of {@code InputStream}
     * @param fos       is instance of {@code FileOutputStream}
     * @param directory is uploading directory
     * @param fileName  is uploading file name
     * @return full uploading path
     * @throws IOException in case of some I/O exceptions
     */

    public static String upload(InputStream ins, FileOutputStream fos, String directory, String fileName) throws IOException {
        String uploadPath = directory + "/" + fileName;
            byte buffer[] = new byte[ 4096 ];
            int n;
            while ((n = ins.read( buffer )) > 0) {
                fos.write( buffer, 0, n );
            }
        return uploadPath;
    }
}
