package by.epam.movierating.controller;


import by.epam.movierating.command.ActionCommand;
import by.epam.movierating.command.ActionFactory;
import by.epam.movierating.config.ConfigurationManager;
import by.epam.movierating.connection.ConnectionPool;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Nikolay Golubitsky
 * The {@code Controller} class represents single servlet used as
 * controller. It involves standard servlet's life-cycle methods
 * such as {{@code doGet()}, {@code doPost()},
 * {@code destroy()}.
 */

@WebServlet("/controller")
@MultipartConfig
public class Controller extends HttpServlet {
    private static final String ERROR_PAGE_PATH = "path.page.error";

    /**
     * Carries out http-servlet's request in the case of {@code get} request.
     *
     * @param request  is the http-servlet's request.
     * @param response is the http-sevlet's response.
     * @throws ServletException    if servlet error occurs.
     * @throws java.io.IOException if input or output error occurs.
     */

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Carries out http-servlet's request in the case of {@code post} request.
     *
     * @param request  is the http-servlet's request.
     * @param response is the http-sevlet's response.
     * @throws ServletException    if servlet error occurs.
     * @throws java.io.IOException if input or output error occurs.
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Performs an action which follows after user's request.
     *
     * @param request  is the http-servlet's request.
     * @param response is the http-servlet's response.
     * @throws ServletException if servlet error occurs.
     * @throws IOException      if input or output error occurs.
     */

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page;
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);
        page = command.execute(request);
        if(page != null) {
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);
            response.sendRedirect(request.getContextPath() + page);
        }
    }

    /**
     * It is performed when servlet stops its existence.
     * It used to carry out finalizing actions.
     */

    @Override
    public void destroy() {
        super.destroy();
        ConnectionPool.getInstance().closeConnections();
    }
}
