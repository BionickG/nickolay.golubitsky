package by.epam.movierating.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code FooterTag} class represents tag date-time tag
 * without body and attributes.
 */

public class FooterTag extends TagSupport {

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.write("<footer class=\"footer\">");
            out.write("<div class=\"container\">");
            out.write("<p class=\"text-muted text-center\">\n" +
                    "Copyrights @2016, All rights reserved by Nickolay Golubitsky</p>");
            out.write("</div>");
            out.write("</footer>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }
}
