package by.epam.movierating.connection;

import by.epam.movierating.exception.DatabaseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Nikolay Golubitsky
 *
 * The {@code ConnectionPool} class represents an ability to connect with
 * database by means of {@code BlockingQueue} interface's possibility.
 */

public class ConnectionPool {
    private final static Logger LOG = LogManager.getLogger();
    private final static String RESOURCES = "database.properties";
    private final static String POOL_PROPERTIES = "pool";
    private final static String POOL_SIZE = "poolSize";
    private final static String URL = "url";
    private static final int TIMEOUT = 3;
    private static AtomicBoolean isInstanceCreated = new AtomicBoolean(false);
    private static ReentrantLock lock = new ReentrantLock();
    private static ConnectionPool instance;
    private ResourceBundle bundle;
    private ArrayBlockingQueue<ProxyConnection> pool;

    /**
     * Constructor sets up all starting database conditions:
     * login, password, pool size, encoding, etc. Causes {@code RuntimeException}
     * in the case of {@code SQLException} that occurs if any SQL error is faced,
     * case of {@code MissingResourceException} that occurs if a property file
     * does't exist
     */

    private ConnectionPool(){
        try (InputStream stream = getClass().getClassLoader().getResourceAsStream(RESOURCES)) {
            bundle = ResourceBundle.getBundle(POOL_PROPERTIES);
            Properties properties = new Properties();
            properties.load(stream);
            String url = bundle.getString(URL);
            int size = Integer.parseInt(bundle.getString(POOL_SIZE));
            pool = new ArrayBlockingQueue<>(size);
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            for (int i = 0; i < size; i++) {
                try {
                    Connection connection = DriverManager.getConnection(url, properties);
                    pool.offer(new ProxyConnection(connection));
                } catch (SQLException e) {
                    LOG.error("Can't create connection");
                }
            }
        } catch (MissingResourceException e){
            LOG.fatal(e.getMessage());
            throw new RuntimeException("Can't load properties", e);
        }
        catch (IOException | SQLException e) {
            LOG.fatal(e.getMessage());
            throw new RuntimeException("Database exception", e);
        }
    }

    /**
     * Represents singleton pattern that gets the
     * only instance of {@code ConnectionPool}.
     *
     * @return single instance of {@code ConnectionPool}.
     */

    public static ConnectionPool getInstance() {
        if (!isInstanceCreated.get()) {
            try {
                lock.lock();
                if (instance == null) {
                    instance = new ConnectionPool();
                    isInstanceCreated.getAndSet(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Gets an instance of {@code Connection} from the pool using non-
     * blocking {@code poll()} method.
     * @return an instance of {@code ProxyConnection}.
     * @throws DatabaseException (self-generated) if {@code InterruptedException}
     *          occurs due to {@code poll()} method used with time-waiting parameter.
     */

    public ProxyConnection getConnection() throws DatabaseException {
        ProxyConnection connection;
        try {
            connection = pool.poll(TIMEOUT, TimeUnit.SECONDS); //take не подходит нет timeout
        } catch (InterruptedException e) {
            throw new DatabaseException(e);
        }
        return connection;
    }

    /**
     * Gives back a non-null connection to the connection pool through the
     * use of {@code offer()} method.
     * @param  connection is an instance of {@code ProxyConnection}.
     */

    public void returnConnection(ProxyConnection connection) {
        if (connection != null) {
            pool.offer(connection);
        }
    }

    /**
     * Shuts down the connection pool by closing all the connections in it.
     * Uses {@code closeRealConnection} for cleaning the pool.
     */

    public void closeConnections() {
        for (int i = 0; i < 10; i++) {
            try {
                pool.take().closeRealConnection();
            } catch (SQLException | InterruptedException e) {
                LOG.error(e);
            }
        }

    }
}
