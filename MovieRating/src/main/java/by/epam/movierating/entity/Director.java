package by.epam.movierating.entity;

import java.io.Serializable;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code Director} class is an entity-class which includes all
 * the necessary getters and setters for its fields and also overrides
 * {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class Director implements Serializable{
    private Long directorId;
    private String firstName;
    private String lastName;

    /**
     * Default constructor of the {@code Director} class.
     */

    public Director() {
    }

    /**
     * Constructor with parameters(first and last name of a director)
     *
     * @param firstName first name of an actor
     * @param lastName  last name of an actor
     */

    public Director(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Gets director's {@code directorId}
     * @return Long value of director's {@code directorId}.
     */

    public Long getDirectorId() {
        return directorId;
    }

    /**
     * Sets director's {@code actorId}
     * @param directorId is Long value of director's {@code directorId}.
     */

    public void setDirectorId(Long directorId) {
        this.directorId = directorId;
    }

    /**
     * Gets director's {@code firstName}
     * @return string value of director's {@code firstName}
     */

    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets director's {@code firstName}
     * @param firstName is string value of director's {@code firstName}
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets director's {@code lastName}
     * @return string value of director's {@code lastName}
     */

    public String getLastName() {
        return lastName;
    }

    /**
     * Sets director's {@code lastName}
     * @param lastName is string value of director's {@code lastName}
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Determines the rules of comparison of some other object {@code o}
     * with this one. The rules depends on the fields of {@code Actor} class.
     * @param   o gives a link to an object with which it is necessary
     *          to compare this one.
     * @return  {@code true} if the object {@param o} satisfies the rules
     *          of comparison with this one, otherwise returns {@code false}.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Director)) return false;

        Director director = (Director) o;

        if (directorId != null ? !directorId.equals(director.directorId) : director.directorId != null) return false;
        if (firstName != null ? !firstName.equals(director.firstName) : director.firstName != null) return false;
        return lastName != null ? lastName.equals(director.lastName) : director.lastName == null;

    }

    /**
     * Gives a hash code value for the object according to the rules of
     * hash code generation.
     * @return an integer value that corresponds an object.
     */

    @Override
    public int hashCode() {
        int result = directorId != null ? directorId.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }

    /**
     * Gives a textual representation of the object according to
     * definite rules presented in the method.
     *
     * @return a string representation of the object.
     */

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
