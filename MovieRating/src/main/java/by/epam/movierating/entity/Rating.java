package by.epam.movierating.entity;

import java.io.Serializable;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code Rating} class is an entity-class which includes all
 * the necessary getters and setters for its fields and also overrides
 * {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class Rating implements Serializable {
    private Long movieId;
    private long sumOfRatings;
    private int numberOfUsers;

    /**
     * Gets movie's {@code movieId}
     *
     * @return Long value of movie's {@code movieId}.
     */

    public Long getMovieId() {
        return movieId;
    }

    /**
     * Sets movie's {@code movieId}
     * @param movieId is Long value of movie's {@code movieId}.
     */

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    /**
     * Gets movie's {@code sumOfRatings}
     * @return long value of movie's {@code sumOfRatings}.
     */

    public long getSumOfRatings() {
        return sumOfRatings;
    }

    /**
     * Sets movie's {@code sumOfRatings}
     * @param sumOfRatings is long value of movie's {@code sumOfRatings}.
     */

    public void setSumOfRatings(long sumOfRatings) {
        this.sumOfRatings = sumOfRatings;
    }

    /**
     * Gets movie rating's {@code numberOfUsers}
     * @return integer value of movie rating's {@code numberOfUsers}
     */

    public int getNumberOfUsers() {
        return numberOfUsers;
    }

    /**
     * Sets movie's {@code numberOfUsers}
     * @param numberOfUsers is integer value of movie rating's {@code numberOfUsers}
     */

    public void setNumberOfUsers(int numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }

    /**
     * Determines the rules of comparison of some other object {@code o}
     * with this one. The rules depends on the fields of {@code Actor} class.
     * @param   o gives a link to an object with which it is necessary
     *          to compare this one.
     * @return  {@code true} if the object {@param o} satisfies the rules
     *          of comparison with this one, otherwise returns {@code false}.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rating)) return false;

        Rating rating = (Rating) o;

        if (sumOfRatings != rating.sumOfRatings) return false;
        if (numberOfUsers != rating.numberOfUsers) return false;
        return movieId != null ? movieId.equals(rating.movieId) : rating.movieId == null;

    }

    /**
     * Gives a hash code value for the object according to the rules of
     * hash code generation.
     *
     * @return an integer value that corresponds an object.
     */

    @Override
    public int hashCode() {
        int result = movieId != null ? movieId.hashCode() : 0;
        result = 31 * result + (int) (sumOfRatings ^ (sumOfRatings >>> 32));
        result = 31 * result + numberOfUsers;
        return result;
    }

    /**
     * Gives a textual representation of the object according to
     * definite rules presented in the method.
     *
     * @return a string representation of the object.
     */

    @Override
    public String toString() {
        return "Rating{" +
                "movieId=" + movieId +
                ", sumOfRatings=" + sumOfRatings +
                ", numberOfUsers=" + numberOfUsers +
                '}';
    }
}
