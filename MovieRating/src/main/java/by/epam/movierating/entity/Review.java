package by.epam.movierating.entity;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code Actor} class is an entity-class which includes all
 * the necessary getters and setters for its fields and also overrides
 * {@code equals()}, {@code hashCode()} and {@code toString()}.
 */

public class Review implements Serializable {
    private Long reviewId;
    private Long movieId;
    private User user;
    private String review;
    private LocalDate reviewDate;
    private int mark;
    private boolean rated;

    /**
     * Gets review's {@code reviewId}
     *
     * @return Long value of review's {@code reviewId}.
     */

    public Long getReviewId() {
        return reviewId;
    }

    /**
     * Sets review's {@code reviewId}
     * @param reviewId is Long value of review's {@code reviewId}.
     */

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }

    /**
     * Gets review's {@code movieId}
     * @return Long value of review's {@code movieId}.
     */

    public Long getMovieId() {
        return movieId;
    }

    /**
     * Sets review's {@code reviewId}
     * @param movieId is Long value of review's {@code movieId}.
     */

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    /**
     * Gets review's {@code user}
     * @return {@code User} value of review's {@code user}.
     */

    public User getUser() {
        return user;
    }

    /**
     * Sets review's {@code user}
     * @param user is {@code User} value of review's {@code user}.
     */

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets review's {@code review}
     * @return string value of review's {@code review}.
     */

    public String getReview() {
        return review;
    }

    /**
     * Sets review's {@code review}
     * @param review is string value of review's {@code review}.
     */

    public void setReview(String review) {
        this.review = review;
    }

    /**
     * Gets review's {@code reviewDate}
     * @return {@code LocalDate} value of review's {@code reviewDate}.
     */

    public LocalDate getReviewDate() {
        return reviewDate;
    }

    /**
     * Sets review's {@code reviewDate}
     * @param reviewDate is {@code LocalDate} value of review's {@code reviewDate}.
     */

    public void setReviewDate(LocalDate reviewDate) {
        this.reviewDate = reviewDate;
    }

    /**
     * Gets review's {@code mark}
     * @return integer value of review's {@code mark}.
     */

    public int getMark() {
        return mark;
    }

    /**
     * Sets review's {@code mark}
     * @param mark is integer value of review's {@code mark}.
     */

    public void setMark(int mark) {
        this.mark = mark;
    }

    /**
     * Gets review's {@code rated}
     * @return boolean value of review's {@code rated}.
     */

    public boolean isRated() {
        return rated;
    }

    /**
     * Sets review's {@code rated}
     * @param rated is boolean value of review's {@code rated}.
     */

    public void setRated(boolean rated) {
        this.rated = rated;
    }

    /**
     * Determines the rules of comparison of some other object {@code o}
     * with this one. The rules depends on the fields of {@code Actor} class.
     * @param   o gives a link to an object with which it is necessary
     *          to compare this one.
     * @return  {@code true} if the object {@param o} satisfies the rules
     *          of comparison with this one, otherwise returns {@code false}.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Review)) return false;

        Review review1 = (Review) o;

        if (reviewId != null ? !reviewId.equals(review1.reviewId) : review1.reviewId != null) return false;
        if (movieId != null ? !movieId.equals(review1.movieId) : review1.movieId != null) return false;
        if (user != null ? !user.equals(review1.user) : review1.user != null) return false;
        if (review != null ? !review.equals(review1.review) : review1.review != null) return false;
        return reviewDate != null ? reviewDate.equals(review1.reviewDate) : review1.reviewDate == null;

    }

    /**
     * Gives a hash code value for the object according to the rules of
     * hash code generation.
     * @return an integer value that corresponds an object.
     */


    @Override
    public int hashCode() {
        int result = reviewId != null ? reviewId.hashCode() : 0;
        result = 31 * result + (movieId != null ? movieId.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (review != null ? review.hashCode() : 0);
        result = 31 * result + (reviewDate != null ? reviewDate.hashCode() : 0);
        return result;
    }

    /**
     * Gives a textual representation of the object according to
     * definite rules presented in the method.
     * @return a string representation of the object.
     */

    @Override
    public String toString() {
        return "Review{" +
                "reviewId=" + reviewId +
                ", movieId=" + movieId +
                ", user=" + user +
                ", review='" + review + '\'' +
                ", reviewDate=" + reviewDate +
                ", mark=" + mark +
                ", rated=" + rated +
                '}';
    }
}
