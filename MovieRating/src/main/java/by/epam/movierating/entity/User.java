package by.epam.movierating.entity;

import java.io.Serializable;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code User} class is an entity-class which includes all
 * the necessary getters and setters for its fields and also overrides
 * {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class User implements Serializable{
    private Long userId;
    private String firstName;
    private String lastName;
    private String email;
    private String login;
    private String password;
    private boolean isBanned = false;
    private Role role;
    private int userRating;
    private String avatar;

    /**
     * Gets user's {@code role}
     *
     * @return {@code Role} value of user's {@code role}.
     */

    public Role getRole() {
        return role;
    }

    /**
     * Sets user's {@code role}
     * @param role is {@code Role} value of user's {@code role}.
     */

    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Gets user's {@code userId}
     * @return Long value of user's {@code userId}.
     */

    public Long getUserId() {
        return userId;
    }

    /**
     * Sets user's {@code actorId}
     * @param userId is Long value of user's {@code actorId}.
     */

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Gets user's {@code firstName}
     * @return string value of user's {@code firstName}
     */

    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets user's {@code firstName}
     * @param firstName is string value of user's {@code firstName}
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets user's {@code lastName}
     * @return string value of user's {@code lastName}
     */

    public String getLastName() {
        return lastName;
    }

    /**
     * Sets user's {@code lastName}
     * @param lastName is string value of user's {@code lastName}
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets user's {@code email}
     * @return string value of user's {@code email}
     */

    public String getEmail() {
        return email;
    }

    /**
     * Sets user's {@code email}
     * @param email is string value of user's {@code email}
     */

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets user's {@code login}
     * @return string value of user's {@code login}
     */

    public String getLogin() {
        return login;
    }

    /**
     * Sets user's {@code login}
     *
     * @param login is string value of user's {@code login}
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets user's {@code password}
     * @return string value of user's {@code password}
     */

    public String getPassword() {
        return password;
    }

    /**
     * Sets user's {@code password}
     * @param password is string value of user's {@code password}
     */

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets user's {@code isBanned}
     * @return boolean value of user's {@code isBanned}
     */

    public boolean isBanned() {
        return isBanned;
    }

    /**
     * Sets user's {@code isBanned}
     * @param banned is boolean value of user's {@code isBanned}
     */

    public void setBanned(boolean banned) {
        isBanned = banned;
    }

    /**
     * Gets user's {@code userRating}
     * @return integer value of user's {@code userRating}
     */

    public int getUserRating() {
        return userRating;
    }

    /**
     * Sets user's {@code userRating}
     * @param userRating is integer value of user's {@code userRating}
     */

    public void setUserRating(int userRating) {
        this.userRating = userRating;
    }

    /**
     * Gets user's {@code avatar}
     * @return string url of user's {@code avatar}
     */

    public String getAvatar() {
        return avatar;
    }

    /**
     * Sets user's {@code avatar}
     * @param avatar is string url of user's {@code avatar}
     */

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * Determines the rules of comparison of some other object {@code o}
     * with this one. The rules depends on the fields of {@code Actor} class.
     * @param   o gives a link to an object with which it is necessary
     *          to compare this one.
     * @return  {@code true} if the object {@param o} satisfies the rules
     *          of comparison with this one, otherwise returns {@code false}.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (isBanned != user.isBanned) return false;
        if (userRating != user.userRating) return false;
        if (userId != null ? !userId.equals(user.userId) : user.userId != null) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (login != null ? !login.equals(user.login) : user.login != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (role != user.role) return false;
        return avatar != null ? avatar.equals(user.avatar) : user.avatar == null;

    }

    /**
     * Gives a hash code value for the object according to the rules of
     * hash code generation.
     *
     * @return an integer value that corresponds an object.
     */

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (isBanned ? 1 : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + userRating;
        result = 31 * result + (avatar != null ? avatar.hashCode() : 0);
        return result;
    }

    /**
     * Gives a textual representation of the object according to
     * definite rules presented in the method.
     *
     * @return a string representation of the object.
     */

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }

}
