package by.epam.movierating.entity;

import java.io.Serializable;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code Country} class is an entity-class which includes all
 * the necessary getters and setters for its fields and also overrides
 * {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class Country implements Serializable{
    private Long countryId;
    private String country;

    /**
     * Default constructor of the {@code Country} class.
     */

    public Country() {
    }

    /**
     * Constructor with parameters(name of the country)
     *
     * @param country is name of the current country
     */
    public Country( String country) {
        this.country = country;
    }

    /**
     * Gets country's {@code countryId}
     * @return Long value of country's {@code countryId}.
     */

    public Long getCountryId() {
        return countryId;
    }

    /**
     * Sets country's {@code countryId}
     * @param countryId is Long value of country's {@code countryId}.
     */

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    /**
     * Gets country's {@code country}
     * @return string value of country's {@code country}
     */

    public String getCountry() {
        return country;
    }

    /**
     * Sets country's {@code country}
     * @param country is string value of country's {@code country}
     */

    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Determines the rules of comparison of some other object {@code o}
     * with this one. The rules depends on the fields of {@code Actor} class.
     * @param   o gives a link to an object with which it is necessary
     *          to compare this one.
     * @return  {@code true} if the object {@param o} satisfies the rules
     *          of comparison with this one, otherwise returns {@code false}.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;

        Country country1 = (Country) o;

        if (countryId != null ? !countryId.equals(country1.countryId) : country1.countryId != null) return false;
        return country != null ? country.equals(country1.country) : country1.country == null;

    }

    /**
     * Gives a hash code value for the object according to the rules of
     * hash code generation.
     * @return an integer value that corresponds an object.
     */

    @Override
    public int hashCode() {
        int result = countryId != null ? countryId.hashCode() : 0;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        return result;
    }

    /**
     * Gives a textual representation of the object according to
     * definite rules presented in the method.
     *
     * @return a string representation of the object.
     */

    @Override
    public String toString() {
        return "Country{" +
                "countryId=" + countryId +
                ", country='" + country + '\'' +
                '}';
    }
}
