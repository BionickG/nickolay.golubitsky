package by.epam.movierating.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code Movie} class is an entity-class which includes all
 * the necessary getters and setters for its fields and also overrides
 * {@code equals()}, {@code hashCode()} and {@code toString()}.
 */

public class Movie implements Serializable {
    private Long movieId;
    private String name;
    private String description;
    private int year;
    private int duration;
    private Director director;
    private Country country;
    private List<Review> reviews;
    private double rating;
    private List<Actor> actors;
    private List<Genre> genres;
    private String cover;
    private String trailer;
    private LocalDateTime dateAdded;

    /**
     * Gets movie's {@code movieId}
     *
     * @return Long value of movie's {@code movieId}.
     */

    public Long getMovieId() {
        return movieId;
    }

    /**
     * Sets movie's {@code movieId}
     * @param movieId is Long value of movie's {@code movieId}.
     */

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    /**
     * Gets movie's {@code name}
     * @return string value of movie's {@code name}.
     */

    public String getName() {
        return name;
    }

    /**
     * Sets movie's {@code name}
     * @param name is string value of movie's {@code name}.
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets movie's {@code description}
     * @return string value of movie's {@code description}.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets movie's {@code description}
     * @param description is string value of movie's {@code description}.
     */

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets movie's {@code year}
     * @return integer value of movie's {@code year}.
     */
    public int getYear() {
        return year;
    }

    /**
     * Sets movie's {@code year}
     * @param year integer value of movie's {@code year}.
     */

    public void setYear(int year) {
        this.year = year;
    }

    /**
     * Gets movie's {@code country}
     * @return {@code Country} value of movie's {@code country}.
     */
    public Country getCountry() {
        return country;
    }

    /**
     * Sets movie's {@code country}
     * @param country is {@code Country} value of movie's {@code country}.
     */

    public void setCountry(Country country) {
        this.country = country;
    }

    /**
     * Gets list of movie's {@code reviews}
     * @return list of values of movie's {@code reviews}.
     */
    public List<Review> getReviews() {
        return reviews;
    }

    /**
     * Sets movie's {@code reviews}
     * @param reviews is list of values of movie's {@code reviews}.
     */

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    /**
     * Gets movie's {@code rating}
     * @return double value of movie's {@code rating}.
     */
    public double getRating() {
        return rating;
    }

    /**
     * Sets movie's {@code rating}
     * @param rating is double value of movie's {@code rating}.
     */

    public void setRating(double rating) {
        this.rating = rating;
    }

    /**
     * Gets list of movie's {@code actors}
     * @return list of values of movie's {@code actors}.
     */
    public List<Actor> getActors() {
        return actors;
    }

    /**
     * Sets movie's {@code actors}
     * @param actors is list of values of movie's {@code actors}.
     */

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    /**
     * Gets movie's {@code cover}
     * @return string url of movie's {@code cover}.
     */
    public String getCover() {
        return cover;
    }

    /**
     * Sets movie's {@code cover}
     * @param cover is string url of movie's {@code cover}.
     */

    public void setCover(String cover) {
        this.cover = cover;
    }

    /**
     * Gets list of movie's {@code genres}
     * @return list of values of movie's {@code genres}.
     */

    public List<Genre> getGenres() {
        return genres;
    }

    /**
     * Sets movie's {@code genres}
     * @param genres is list of values of movie's {@code genres}.
     */

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    /**
     * Gets movie's {@code duration}
     * @return integer value of movie's {@code duration}.
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets movie's {@code duration}
     * @param duration is integer value of movie's {@code duration}.
     */

    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Gets movie's {@code director}
     * @return {@code Director} value of movie's {@code director}.
     */
    public Director getDirector() {
        return director;
    }

    /**
     * Sets movie's {@code director}
     * @param director is {@code Director} value of movie's {@code director}.
     */

    public void setDirector(Director director) {
        this.director = director;
    }

    /**
     * Gets movie's {@code trailer}
     * @return string url of movie's {@code trailer}.
     */

    public String getTrailer() {
        return trailer;
    }

    /**
     * Sets movie's {@code trailer}
     * @param trailer is string url of movie's {@code trailer}.
     */

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    /**
     * Gets movie's {@code dateAdded}
     * @return {@code LocalDateTime} value of movie's {@code dateAdded}.
     */

    public LocalDateTime getDateAdded() {
        return dateAdded;
    }

    /**
     * Sets movie's {@code dateAdded}
     * @param dateAdded is {@code LocalDateTime} value of movie's {@code dateAdded}.
     */

    public void setDateAdded(LocalDateTime dateAdded) {
        this.dateAdded = dateAdded;
    }

    /**
     * Determines the rules of comparison of some other object {@code o}
     * with this one. The rules depends on the fields of {@code Actor} class.
     * @param   o gives a link to an object with which it is necessary
     *          to compare this one.
     * @return  {@code true} if the object {@param o} satisfies the rules
     *          of comparison with this one, otherwise returns {@code false}.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;

        Movie movie = (Movie) o;

        if (year != movie.year) return false;
        if (duration != movie.duration) return false;
        if (Double.compare(movie.rating, rating) != 0) return false;
        if (movieId != null ? !movieId.equals(movie.movieId) : movie.movieId != null) return false;
        if (name != null ? !name.equals(movie.name) : movie.name != null) return false;
        if (description != null ? !description.equals(movie.description) : movie.description != null) return false;
        if (director != null ? !director.equals(movie.director) : movie.director != null) return false;
        if (country != null ? !country.equals(movie.country) : movie.country != null) return false;
        if (reviews != null ? !reviews.equals(movie.reviews) : movie.reviews != null) return false;
        if (actors != null ? !actors.equals(movie.actors) : movie.actors != null) return false;
        if (genres != null ? !genres.equals(movie.genres) : movie.genres != null) return false;
        if (cover != null ? !cover.equals(movie.cover) : movie.cover != null) return false;
        if (trailer != null ? !trailer.equals(movie.trailer) : movie.trailer != null) return false;
        return dateAdded != null ? dateAdded.equals(movie.dateAdded) : movie.dateAdded == null;

    }

    /**
     * Gives a hash code value for the object according to the rules of
     * hash code generation.
     * @return an integer value that corresponds an object.
     */

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = movieId != null ? movieId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + duration;
        result = 31 * result + (director != null ? director.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (reviews != null ? reviews.hashCode() : 0);
        temp = Double.doubleToLongBits(rating);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (actors != null ? actors.hashCode() : 0);
        result = 31 * result + (genres != null ? genres.hashCode() : 0);
        result = 31 * result + (cover != null ? cover.hashCode() : 0);
        result = 31 * result + (trailer != null ? trailer.hashCode() : 0);
        result = 31 * result + (dateAdded != null ? dateAdded.hashCode() : 0);
        return result;
    }

    /**
     * Gives a textual representation of the object according to
     * definite rules presented in the method.
     * @return a string representation of the object.
     */

    @Override
    public String toString() {
        return "Movie{" +
                "movieId=" + movieId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", year=" + year +
                ", duration=" + duration +
                ", director=" + director +
                ", country=" + country +
                ", reviews=" + reviews +
                ", rating=" + rating +
                ", actors=" + actors +
                ", genres=" + genres +
                ", cover='" + cover + '\'' +
                ", trailer='" + trailer + '\'' +
                ", dateAdded=" + dateAdded +
                '}';
    }
}