package by.epam.movierating.entity;

import java.io.Serializable;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code Actor} class is an entity-class which includes all
 * the necessary getters and setters for its fields and also overrides
 * {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class Actor implements Serializable {
    private Long actorId;
    private String firstName;
    private String lastName;

    /**
     * Default constructor of the {@code Actor} class.
     */
    public Actor() {
    }

    /**
     * Constructor with parameters(first and last name of an actor)
     *
     * @param firstName first name of an actor
     * @param lastName  last name of an actor
     */

    public Actor(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Gets actor's {@code actorId}
     * @return Long value of actor's {@code actorId}.
     */

    public Long getActorId() {
        return actorId;
    }

    /**
     * Sets actor's {@code actorId}
     * @param actorId is Long value of actor's {@code actorId}.
     */

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    /**
     * Gets actor's {@code firstName}
     * @return string value of actor's {@code firstName}
     */

    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets actor's {@code firstName}
     * @param firstName is string value of actor's {@code firstName}
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets actor's {@code lastName}
     * @return string value of actor's {@code lastName}
     */

    public String getLastName() {
        return lastName;
    }

    /**
     * Sets actor's {@code lastName}
     * @param lastName is string value of actor's {@code lastName}
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Determines the rules of comparison of some other object {@code o}
     * with this one. The rules depends on the fields of {@code Actor} class.
     * @param   o gives a link to an object with which it is necessary
     *          to compare this one.
     * @return  {@code true} if the object {@param o} satisfies the rules
     *          of comparison with this one, otherwise returns {@code false}.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Actor)) return false;

        Actor actor = (Actor) o;

        if (actorId != null ? !actorId.equals(actor.actorId) : actor.actorId != null) return false;
        if (firstName != null ? !firstName.equals(actor.firstName) : actor.firstName != null) return false;
        return lastName != null ? lastName.equals(actor.lastName) : actor.lastName == null;

    }

    /**
     * Gives a hash code value for the object according to the rules of
     * hash code generation.
     * @return an integer value that corresponds an object.
     */

    @Override
    public int hashCode() {
        int result = actorId != null ? actorId.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }

    /**
     * Gives a textual representation of the object according to
     * definite rules presented in the method.
     * @return a string representation of the object.
     */

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
