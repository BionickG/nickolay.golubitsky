package by.epam.movierating.entity;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code Actor} enumeration class is an entity-class which includes all
 * the necessary getters and also overrides {@code toString()}.
 */
public enum Role {
    ADMINISTRATOR(1),
    USER(2),
    GUEST(3);

    Long roleId;

    /**
     * Constructor with parameters(role's identification number)
     *
     * @param roleId first name of an actor
     */

    Role(long roleId) {
        this.roleId = roleId;
    }

    /**
     * Gets role's {@code roleId}
     * @return Long value of role's {@code roleId}.
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * Gives a textual representation of the object according to
     * definite rules presented in the method.
     *
     * @return a string representation of the object.
     */

    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + roleId +
                "} " + super.toString();
    }

}
