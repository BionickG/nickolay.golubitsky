package by.epam.movierating.entity;

import java.io.Serializable;

/**
 * @author Nickolay Golubitsky
 *
 * The {@code Genre} class is an entity-class which includes all
 * the necessary getters and setters for its fields and also overrides
 * {@code equals()}, {@code hashCode()} and {@code toString()}.
 */

public class Genre implements Serializable{

    private String genre;
    private Long genreId;

    /**
     * Default constructor of the {@code Genre} class.
     */

    public Genre() {
    }

    /**
     * Constructor with parameters(name of the genre)
     *
     * @param genre is name of the current country
     */

    public Genre(String genre) {

        this.genre = genre;
    }

    /**
     * Gets genre's {@code genreId}
     * @return Long value of genre's {@code genreId}.
     */

    public Long getGenreId() {
        return genreId;
    }

    /**
     * Sets genre's {@code genreId}
     * @param genreId is Long value of genre's {@code genreId}.
     */

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    /**
     * Gets genre's {@code genre}
     * @return string value of genre's {@code genre}
     */

    public String getGenre() {
        return genre;
    }

    /**
     * Sets genre's {@code genre}
     *
     * @param genre is string value of country's {@code genre}
     */

    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * Determines the rules of comparison of some other object {@code o}
     * with this one. The rules depends on the fields of {@code Actor} class.
     * @param   o gives a link to an object with which it is necessary
     *          to compare this one.
     * @return  {@code true} if the object {@param o} satisfies the rules
     *          of comparison with this one, otherwise returns {@code false}.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Genre)) return false;

        Genre genre1 = (Genre) o;

        if (genre != null ? !genre.equals(genre1.genre) : genre1.genre != null) return false;
        return genreId != null ? genreId.equals(genre1.genreId) : genre1.genreId == null;
    }

    /**
     * Gives a hash code value for the object according to the rules of
     * hash code generation.
     * @return an integer value that corresponds an object.
     */

    @Override
    public int hashCode() {
        int result = genre != null ? genre.hashCode() : 0;
        result = 31 * result + (genreId != null ? genreId.hashCode() : 0);
        return result;
    }

    /**
     * Gives a textual representation of the object according to
     * definite rules presented in the method.
     *
     * @return a string representation of the object.
     */

    @Override
    public String toString() {
        return genre;
    }

}
