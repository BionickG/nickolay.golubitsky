package by.epam.movierating.connection;


import org.junit.Test;

/**
 * Created by Nickolay Golubitsky on 10.09.2016.
 */
public class ConnectionPoolTest {

    private ConnectionPool connectionPool;

    @Test(timeout = 1500)
    public void getInstance() throws Exception {
         connectionPool = ConnectionPool.getInstance();
    }

    @Test(timeout = 1000)
    public void getConnection() throws Exception {
        connectionPool = ConnectionPool.getInstance();
        connectionPool.getConnection();
    }

    @Test(timeout = 1200)
    public void returnConnection() throws Exception {
        connectionPool  = ConnectionPool.getInstance();
        ProxyConnection connection = connectionPool.getConnection();
        connectionPool.returnConnection(connection);

    }

}