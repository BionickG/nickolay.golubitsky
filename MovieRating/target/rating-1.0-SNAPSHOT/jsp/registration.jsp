<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="register.">

    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><fmt:message key="title"/> </title>

        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/font-awesome.css" rel="stylesheet">

        <style type="text/css">
            .footer{
                position: absolute;
            }
        </style>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <jsp:include page="include/header.jsp"/>
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-lg-offset-3">
                <form class="form-horizontal" role="form" method="post" action="controller">
                    <input type="hidden" value="register" name="command">
                    <h2 class="text-center"><fmt:message key="form"/> </h2>
                    <br>
                    <div class="form-group">
                        <label for="firstName" class="col-sm-4 control-label"><fmt:message key="first.name"/> </label>
                        <div class="col-xs-8" data-tip="<fmt:message key="name.tip"/> ">
                            <input type="text" id="firstName" name="firstName" placeholder="<fmt:message key="first.name"/>" class="form-control"
                                   pattern="[A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20}" autofocus>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastName" class="col-sm-4 control-label"><fmt:message key="last.name"/> </label>
                        <div class="col-xs-8" data-tip="<fmt:message key="lastName.tip"/>">
                            <input type="text" id="lastName" name="lastName"
                                   placeholder="<fmt:message key="last.name"/> " class="form-control "
                                   pattern="[A-Z-А-ЯЁ]{1}[a-zа-яё]{1,20}" data-tooltip="Подсказка">
                            <span class="help-block text-danger"> ${nameFail}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="login" class="col-sm-4 control-label"><fmt:message key="login"/> </label>
                        <div class="col-xs-8" data-tip="<fmt:message key="login.tip"/>">
                            <input type="text" id="login" name="login" placeholder="<fmt:message key="login"/>"
                                   pattern="[\w\d]{4,20}" class="form-control">
                            <span class="help-block text-danger"> ${loginFail}</span>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 control-label"><fmt:message key="email"/> </label>
                        <div class="col-xs-8" data-tip="<fmt:message key="email.tip"/>">
                            <input type="email" id="email" name="email" placeholder="<fmt:message key="email"/> " class="form-control"
                                   pattern="([\w\._]+@[\w_]+?\.[\w]{2,6})">
                            <span class="help-block text-danger"> ${emailFail}</span>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-4 control-label"><fmt:message key="password"/> </label>
                        <div class="col-xs-8" data-tip="<fmt:message key="password.tip"/>">
                            <input type="password" id="password" name="password" placeholder="<fmt:message key="password"/>" class="form-control"
                                   pattern="(^[\w0-9_-]{6,18}$)">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmPassword" class="col-sm-4 control-label"><fmt:message key="confirm"/></label>
                        <div class="col-xs-8" data-tip="<fmt:message key="password.tip"/>">
                            <input type="password" id="confirmPassword" name="confirmPassword" placeholder="<fmt:message key="confirm"/>" class="form-control"
                                   pattern="(^[\w0-9_-]{6,18}$)">
                            <span class="text-danger">${passwordMatchFail}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-sm-offset-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-default btn-block"><fmt:message key="register"/></button>
                                </div>
                                <div class="col-xs-6">
                                    <a class="btn btn-default btn-block" href="../index.jsp"><fmt:message key="return"/> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <ctg:footer/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>
    <script src="../js/tooltip.js"></script>
    </body>
    </html>

</fmt:bundle>