<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<fmt:bundle basename="pagecontent">
    <div class="col-xs-4 col-xs-offset-4">
        <nav>
            <ul class="pager">
                <li class="previous <c:if test="${pageNumber == 0}"> disabled </c:if>">
                    <c:choose>
                        <c:when test="${pageNumber != 0}">
                            <a href="controller?command=previous_page">&larr;<fmt:message key="paginator.back"/> </a>
                        </c:when>
                        <c:otherwise>
                            <a href="#">&larr;<fmt:message key="paginator.back"/> </a>
                        </c:otherwise>
                    </c:choose>
                </li>
                <li>${pageNumber+1}/${numberOfPages}</li>
                <li class="next <c:if test="${pageNumber == numberOfPages-1}"> disabled </c:if>">
                    <c:choose>
                        <c:when test="${pageNumber != numberOfPages-1}">
                            <a href="controller?command=next_page"><fmt:message key="paginator.next"/>&rarr;</a>
                        </c:when>
                        <c:otherwise>
                            <a href="#"><fmt:message key="paginator.next"/>&rarr;</a>
                        </c:otherwise>
                    </c:choose>

                </li>
            </ul>
        </nav>
    </div>
</fmt:bundle>