<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="login.">

    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><fmt:message key="title"/> </title>

        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/font-awesome.css" rel="stylesheet">

        <style type="text/css">
            .footer{
                position: absolute;
            }
        </style>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <jsp:include page="include/header.jsp"/>
    <div class="container login">
        <div class="row">
            <div class="col-xs-6 col-lg-offset-3">
                <form class="form-horizontal" action="controller" method="post">
                    <input type="hidden" name="command" value="login">
                    <h2 class="text-center"><fmt:message key="welcome"/> </h2>
                    <br>
                    <div class="form-group">
                        <label for="login" class="col-sm-3 control-label"><fmt:message key="login"/> </label>
                        <div class="col-xs-9">
                            <input type="text" id="login" name="login" placeholder="<fmt:message key="login"/>" class="form-control"
                                   pattern="[\w\d]{4,20}" autofocus>

                        </div>
                    </div>
                    <div class="form-group">
                    <label for="password" class="col-sm-3 control-label"><fmt:message key="password"/> </label>
                    <div class="col-xs-9">
                        <input type="password" id="password" name="password" placeholder="<fmt:message key="password"/> " class="form-control"
                                autofocus>
                        <span class="text-danger">${loginFail}</span>
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-xs-9 col-sm-offset-3">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-default btn-block"><fmt:message key="in"/> </button>
                                </div>
                                <div class="col-xs-6">
                                    <a class="btn btn-default btn-block" href="../index.jsp"><fmt:message key="return"/> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <ctg:footer/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>
    </body>
    </html>

</fmt:bundle>