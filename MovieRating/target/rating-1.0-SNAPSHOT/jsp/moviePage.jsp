<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="movie.">
<html>
    <head>
        <title>${movie.name}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <link href="css/font-awesome.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <jsp:include page="include/header.jsp"/>
    <div class="row">
        <div class="container">
            <div class="col-xs-3">
                <img src="../${movie.cover}" width="360" alt="">
            </div>
            <div class="col-xs-7 col-xs-offset-2">
                <div class="row">
                    <h3>${movie.name} (${movie.year})</h3>
                </div>
                <div class="row">
                    <h3>
                        <div class="rating-block">
                            <h4 class="bold text-left"><fmt:message key="rating"/> ${movie.rating} <small>/ 10</small></h4>
                            <c:choose>
                                <c:when test="${user.role == 'GUEST'}">
                                    <h3><fmt:message key="sign"/> </h3>
                                </c:when>
                                <c:when test="${userRating !=0}">
                                    <h3><fmt:message key="user.rating"/> ${userRating}</h3>
                                </c:when>
                                <c:otherwise>
                                    <form action="controller" method="post">
                                        <input type="hidden" name="command" value="add_movie_rating">
                                        <input type="hidden" name="movieId" value="${movie.movieId}">
                                        <span class="star-cb-group">
                                          <input type="radio" id="rating-10" name="rating" value="10"
                                                 onclick="submit()"/><label for="rating-10">10</label>
                                          <input type="radio" id="rating-9" name="rating" value="9" onclick="submit()"/><label
                                                for="rating-9">9</label>
                                          <input type="radio" id="rating-8" name="rating" value="8" onclick="submit()"/><label
                                                for="rating-8">8</label>
                                          <input type="radio" id="rating-7" name="rating" value="7" onclick="submit()"/><label
                                                for="rating-7">7</label>
                                          <input type="radio" id="rating-6" name="rating" value="6" onclick="submit()"/><label
                                                for="rating-6">6</label>
                                          <input type="radio" id="rating-5" name="rating" value="5" onclick="submit()"/><label
                                                for="rating-5">5</label>
                                          <input type="radio" id="rating-4" name="rating" value="4" onclick="submit()"/><label
                                                for="rating-4">4</label>
                                          <input type="radio" id="rating-3" name="rating" value="3" onclick="submit()"/><label
                                                for="rating-3">3</label>
                                          <input type="radio" id="rating-2" name="rating" value="2" onclick="submit()"/><label
                                                for="rating-2">2</label>
                                          <input type="radio" id="rating-1" name="rating" value="1" onclick="submit()"/><label
                                                for="rating-1">1</label>
                                          <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear" /><label for="rating-0">0</label>
                                        </span>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </h3>
                </div>
                <div class="row">
                    <h5 class="text-left">${movie.description}</h5>
                </div>
                <div class="row">
                    <div class="col-xs-4 pull-left">
                        <p><fmt:message key="director"/> </p>
                    </div>
                    <div class="col-xs-8">
                        <p class="pull-left">
                            <a href="controller?command=movie_by_director&directorId=${movie.director.directorId}">${movie.director.firstName}
                             ${movie.director.lastName}</a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 pull-left">
                        <p><fmt:message key="starring"/> </p>
                    </div>
                    <div class="col-xs-8">
                        <p class="pull-left">
                            <a href="controller?command=movie_by_actor&actorId=${movie.actors[0].actorId}">${movie.actors[0]}</a>
                            <c:forEach var="actor" items="${movie.actors}" begin="1">
                                , <a href="controller?command=movie_by_actor&actorId=${actor.actorId}">${actor}</a>
                            </c:forEach>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 pull-left">
                        <p><fmt:message key="genres"/> </p>
                    </div>
                    <div class="col-xs-8">
                        <p class="pull-left">
                            <a href="controller?command=take_by_genre&genreId=${movie.genres[0].genreId}">${movie.genres[0]}</a>
                            <c:forEach var="genre" items="${movie.genres}" begin="1">
                                , <a href="controller?command=take_by_genre&genreId=${genre.genreId}">${genre}</a>
                            </c:forEach>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 pull-left">
                        <p><fmt:message key="country"/> </p>
                    </div>
                    <div class="col-xs-8">
                        <p class="pull-left">
                            <a href="controller?command=take_by_country&countryId=${movie.country.countryId}">${movie.country.country}</a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 pull-left">
                        <p><fmt:message key="duration"/> </p>
                    </div>
                    <div class="col-xs-8">
                        <p class="pull-left">
                            ${movie.duration} <fmt:message key="min"/>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center"> <fmt:message key="trailer"/> </h3>
            <iframe class="center-block" width="960" height="540" src="${movie.trailer}" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="text-center"><fmt:message key="comments"/> ${movie.name}</h3>
            </div><!-- /col-sm-12 -->
        </div><!-- /row -->
        <c:choose>
            <c:when test="${user.role == 'GUEST'}">
                <h4 class="text-info text-center"><fmt:message key="no.comments"/> </h4>
            </c:when>
            <c:when test="${user.banned}">
                <h4 class="text-info text-center"><fmt:message key="banned"/> </h4>
            </c:when>
            <c:otherwise>
                <div class="row">
                    <div class="col-xs-1 col-xs-offset-1 comment">
                        <div class="thumbnail">
                            <img class="user-photo" src="${user.avatar}" alt="">
                        </div><!-- /thumbnail -->
                    </div><!-- /col-sm-1 -->
                    <div class="col-xs-9">
                        <form action="controller">
                            <input type="hidden" name="command" value="review_add"/>
                            <input type="hidden" name="check" value="${check}"/>
                            <div class="form-group">
                                  <label for="comment"><fmt:message key="comment"/> </label>
                                  <textarea class="form-control" rows="4" id="comment" name="comment"></textarea>
                                <button class="btn-default btn" type="submit"><fmt:message key="submit"/></button>
                                <h4 class="text-info text-center">${message}</h4>
                            </div>
                        </form>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>

        <c:forEach var="comment" items="${movie.reviews}">
        <div class="row">
                <div class="col-xs-1 col-xs-offset-1">
                    <div class="thumbnail">
                        <img class="user-photo" src="${comment.user.avatar}" alt="">
                    </div><!-- /thumbnail -->
                </div><!-- /col-sm-1 -->
            <div class="col-sm-9">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong>${comment.user.firstName}</strong> <span class="text-muted">commented ${comment.reviewDate}</span>
                                <span class="pull-right">
                                    <c:if test="${user.role == 'ADMINISTRATOR'}">
                                        <a class="btn btn-danger btn-xs" href="controller?command=review_delete&reviewId=${comment.reviewId}">DELETE</a>
                                    </c:if>
                                    <c:if test="${user.role != 'GUEST'}">
                                        <c:if test="${comment.user.userId != user.userId && !comment.rated}">
                                            <a class="btn btn-danger btn-xs"
                                               href="controller?command=review_mark_dec&reviewId=${comment.reviewId}&check=${check}">-</a>
                                        </c:if>
                                        <span>${comment.mark}</span>
                                        <c:if test="${comment.user.userId != user.userId && !comment.rated}">
                                            <a class="btn btn-success btn-xs"
                                               href="controller?command=review_mark_inc&reviewId=${comment.reviewId}&check=${check}">+</a>
                                        </c:if>
                                    </c:if>
                                </span>
                            </div>
                        <div class="panel-body">
                            <c:out value="${comment.review}"/>
                        </div><!-- /panel-body -->
                    </div><!-- /panel panel-default -->
                </div><!-- /col-sm-5 -->
        </div><!-- /row -->
        </c:forEach>

    </div><!-- /container -->
    <ctg:footer/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>
    </body>
</html>
</fmt:bundle>