-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: movie_rating
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor`
--

DROP TABLE IF EXISTS `actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actor` (
  `actor_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  PRIMARY KEY (`actor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actor`
--

LOCK TABLES `actor` WRITE;
/*!40000 ALTER TABLE `actor` DISABLE KEYS */;
INSERT INTO `actor` VALUES (1,'Тим','Робинс'),(2,'Морган','Фриман'),(3,'Боб','Гантон'),(4,'Уильям','Седлер'),(5,'Клэнси','Браун'),(6,'Джил','Беллоуз'),(7,'Марк','Ролстон'),(8,'Том','Хэнкс'),(9,'Дэвид','Морс'),(10,'Майкл Кларк','Дункан'),(12,'Джеймс','Кромуэлл'),(13,'Майкл','Джитер'),(14,'Робин','Райт'),(15,'Гэри','Синиз'),(29,'Лиам','Нисон'),(30,'Бен','Кингсли'),(31,'Рэйф','Файнс'),(32,'Кэролайн','Гудолл'),(33,'Эмбет','Дэвидц'),(34,'Йонатан','Сэгаль'),(35,'Франсуа','Клюзе'),(36,'Омар','Си'),(37,'Анн','ЛеНи'),(38,'Клотильд','Молле'),(39,'Жан','Рено'),(40,'Гари','Олдман'),(41,'Натали','Портман'),(44,'Леонардо','ДиКаприо'),(45,'Джозеф','Гордон-Левитт'),(46,'Эллен','Пейдж'),(47,'Том','Харди'),(48,'Кен','Ватанабе'),(49,'Марион','Котийяр'),(50,'Иван','Иванов'),(51,'Эдвард','Нортон'),(52,'Брэд','Питт'),(53,'Хелена','Бонем-Картер'),(54,'Мит','Лоаф'),(55,'Зэк','Гренье'),(56,'Марлон','Брандо'),(57,'Аль','Пачино'),(58,'Джеймс','Каан'),(59,'Роберт','Дювалл'),(60,'Джон','Траволта'),(61,'Брюс','Уиллис'),(62,'Ума','Турман'),(63,'Винг','Реймз'),(64,'Рассел','Кроу'),(65,'Эд','Харрис'),(66,'Дженнифер','Коннелли'),(67,'Кристофер','Пламмер'),(68,'Пол','Беттани'),(69,'Майкл','Фокс'),(70,'Кристофер','Ллойд'),(71,'Лиа','Томпсон'),(72,'Криспин','Гловер'),(73,'Киану','Ривз'),(74,'Лоренс','Фишбёрн'),(75,'Кэрри-Энн','Мосс'),(76,'Хьюго','Уивинг'),(77,'Эдриан','Броуди'),(78,'Эмилия','Фокс'),(79,'Даниэль','Кальтаджироне'),(81,'Морин','Липман'),(82,'Хоакин','Феникс'),(83,'Конни','Нильсен'),(84,'Оливер','Рид'),(85,'Юрий','Никулин'),(86,'Анатолий','Папанов'),(87,'Андрей','Миронов'),(88,'Эдвард','Ферлонг'),(89,'Дженнифер','Лиен'),(90,'Итан','Сапли'),(91,'Файруза','Балк'),(92,'Крис','Доннелл'),(93,'Джеймс','Ребхорн');
/*!40000 ALTER TABLE `actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(65) NOT NULL,
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `country_country_uindex` (`country`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (2,'Canada'),(8,'China'),(6,'France'),(5,'Italia'),(3,'Russia'),(1,'USA'),(9,'СССР');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `director`
--

DROP TABLE IF EXISTS `director`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `director` (
  `director_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`director_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `director`
--

LOCK TABLES `director` WRITE;
/*!40000 ALTER TABLE `director` DISABLE KEYS */;
INSERT INTO `director` VALUES (1,'Френк','Дарабонт'),(2,'Роберт','Земекис'),(3,'Стивен','Спилберг'),(4,'Оливье','Накаш'),(5,'Кристофер','Нолан'),(6,'Люк','Бессон'),(7,'Френк','Дарабон'),(8,'Дэвид','Финчер'),(9,'Френсис','Коппола'),(10,'Квентин','Тарантино'),(11,'Рон','Ховард'),(12,'Лилли','Вачовски'),(13,'Молли','Вачовски'),(14,'Роман','Полански'),(15,'Ридли','Скотт'),(16,'Леонид','Гайдай'),(17,'Тони','Кэй'),(18,'Мартин','Брест');
/*!40000 ALTER TABLE `director` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genre` (
  `genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `genre` varchar(45) NOT NULL,
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'комедия'),(2,'боевик'),(3,'триллер'),(4,'драма'),(5,'криминал'),(6,'фантастика'),(7,'биография'),(9,'Приключения'),(10,'Семейный');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `movie_id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_name` varchar(64) NOT NULL,
  `year` int(11) NOT NULL,
  `description` text,
  `country_id` int(11) NOT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `trailer_link` varchar(100) DEFAULT NULL,
  `director_id` int(11) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`movie_id`),
  KEY `movie_country_country_id_fk` (`country_id`),
  KEY `movie_director_director_id_fk` (`director_id`),
  CONSTRAINT `movie_country_country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `movie_director_director_id_fk` FOREIGN KEY (`director_id`) REFERENCES `director` (`director_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,'Побег из Шоушенкa',1994,'Успешный банкир Энди Дюфрейн обвинен в убийстве собственной жены\r\nи ее любовника. Оказавшись в тюрьме под названием Шоушенк, он сталкивается с жестокостью и беззаконием, царящими по обе\r\n стороны решетки. Каждый, кто попадает в эти стены, становится их рабом до конца жизни. Но Энди, вооруженный живым умом\r\n  и доброй душой, отказывается мириться с приговором судьбы и начинает разрабатывать невероятно дерзкий план своего освобождения.',1,'covers/1.jpg',145,'https://www.youtube.com/embed/GOniB0Ykp1M',1,'2016-10-06 16:39:29'),(2,'Зеленая миля',1999,'\r\nОбвиненный в страшном преступлении, Джон Коффи оказывается в блоке смертников тюрьмы «Холодная гора».\n  Вновь прибывший обладал поразительным ростом и был пугающе спокоен, что, впрочем, никак не влияло на отношение к нему начальника\n  блока Пола Эджкомба, привыкшего исполнять приговор.\r\n\r\nГигант удивил всех позже, когда выяснилось, что он обладает\n   невероятной магической силой…',1,'covers/iphone360_435.jpg',189,'https://www.youtube.com/embed/tDNmMydFOeI',1,'2016-08-24 21:00:00'),(3,'Форест Гамп',1994,'От лица главного героя Форреста Гампа, слабоумного безобидного человека с благородным и открытым сердцем,\n   рассказывается история его необыкновенной жизни.\r\n\r\nФантастическим образом превращается он в известного футболиста,\n    героя войны, преуспевающего бизнесмена. Он становится миллиардером, но остается таким же бесхитростным, глупым и добрым.\n     Форреста ждет постоянный успех во всем, а он любит девочку, с которой дружил в детстве, но взаимность приходит слишком поздно.',1,'covers/iphone360_448.jpg',142,'https://www.youtube.com/embed/gB7wy6YGCGA',2,'2016-08-26 21:00:00'),(8,'Начало',2010,'Кобб — талантливый вор, лучший из лучших в опасном искусстве извлечения: он крадет ценные секреты\n   из глубин подсознания во время сна, когда человеческий разум наиболее уязвим. Редкие способности Кобба сделали его\n    ценным игроком в привычном к предательству мире промышленного шпионажа, но они же превратили его в извечного беглеца\n     и лишили всего, что он когда-либо любил. \r\n\r\nИ вот у Кобба появляется шанс исправить ошибки. Его последнее дело\n      может вернуть все назад, но для этого ему нужно совершить невозможное — инициацию. Вместо идеальной кражи Кобб и\n       его команда спецов должны будут провернуть обратное. Теперь их задача — не украсть идею, а внедрить ее. Если у\n       них получится, это и станет идеальным преступлением. \r\n\r\nНо никакое планирование или мастерство не могут\n        подготовить команду к встрече с опасным противником, который, кажется, предугадывает каждый их ход. Врагом,\n         увидеть которого мог бы лишь Кобб.',1,'covers/8.jpg',148,'https://www.youtube.com/embed/YCoC3WrLOC4',5,'2016-09-07 21:00:00'),(9,'Бойцовский клуб',1999,'Терзаемый хронической бессонницей и отчаянно пытающийся вырваться из мучительно скучной жизни, к\n  лерк встречает некоего Тайлера Дардена, харизматического торговца мылом с извращенной философией. Тайлер уверен, что\n   самосовершенствование — удел слабых, а саморазрушение — единственное, ради чего стоит жить.\r\n\r\nПройдет немного времени,\n    и вот уже главные герои лупят друг друга почем зря на стоянке перед баром, и очищающий мордобой доставляет им высшее блаженство. Приобщая других мужчин к простым радостям физической жестокости, они основывают тайный Бойцовский Клуб, который имеет огромный успех. Но в концовке фильма всех ждет шокирующее открытие, которое может привести к непредсказуемым событиям…',1,'covers/9.jpg',131,'https://www.youtube.com/embed/oqHJp_ZZdU4',8,'2016-09-07 21:00:00'),(10,'Леон',1994,'Профессиональный убийца Леон, не знающий пощады и жалости, знакомится со своей очаровательной соседкой Матильдой, семью которой расстреливают полицейские, замешанные в торговле наркотиками. Благодаря этому знакомству он впервые испытывает чувство любви, но…',6,'covers/10.jpg',133,'https://www.youtube.com/embed/xqn58tWwv1c',6,'2016-09-07 21:00:00'),(11,'Крестный отец',1972,'\r\nКриминальная сага, повествующая о нью-йоркской сицилийской мафиозной семье Корлеоне. Фильм охватывает период 1945-1955 годов.\r\n\r\nГлава семьи, Дон Вито Корлеоне, выдаёт замуж свою дочь. В это время со Второй мировой войны возвращается его любимый сын Майкл. Майкл, герой войны, гордость семьи, не выражает желания заняться жестоким семейным бизнесом. Дон Корлеоне ведёт дела по старым правилам, но наступают иные времена, и появляются люди, желающие изменить сложившиеся порядки. На Дона Корлеоне совершается покушение.',1,'covers/11.jpg',175,'https://www.youtube.com/embed/_KGXoY3dpqc',9,'2016-09-07 21:00:00'),(12,'Криминальное чтиво',1994,'Двое бандитов Винсент Вега и Джулс Винфилд проводят время в философских беседах в перерыве между разборками и «решением проблем» с должниками своего криминального босса Марселласа Уоллеса. Параллельно разворачиваются три истории. \r\n\r\nВ первой из них Винсент присматривает за женой Марселласа Мией и спасает ее от передозировки наркотиков. Во второй рассказывается о Бутче Кулидже, боксере, нанятом Уоллесом, чтобы сдать бой, но обманувшим его. Третья история объединяет первые две — в кафе парочка молодых неудачливых грабителей — Пампкин и Хани Бани делают попытку ограбления, но Джулс останавливает их.',1,'covers/12.jpg',154,'https://www.youtube.com/embed/6N2ZTQ6819s',10,'2016-09-07 21:00:00'),(13,'Игры разума',2001,'\r\nОт всемирной известности до греховных глубин — все это познал на своей шкуре Джон Форбс Нэш-младший. Математический гений, он на заре своей карьеры сделал титаническую работу в области теории игр, которая перевернула этот раздел математики и практически принесла ему международную известность.\r\n\r\nОднако буквально в то же время заносчивый и пользующийся успехом у женщин Нэш получает удар судьбы, который переворачивает уже его собственную жизнь.',1,'covers/13.jpg',135,'https://www.youtube.com/embed/elmOT2EIOLQ',11,'2016-09-07 21:00:00'),(14,'Назад в будущее',1985,'Подросток Марти с помощью машины времени, сооруженной его другом профессором доком Брауном, попадает из 80-х в далекие 50-е. Там он встречается со своими будущими родителями, еще подростками, и другом-профессором, совсем молодым.',1,'covers/14.jpg',116,'https://www.youtube.com/embed/OCtt7giwbi0',2,'2016-09-07 21:00:00'),(25,'Матрица',1999,'Жизнь Томаса Андерсона разделена на две части: днём он самый обычный офисный работник, получающий нагоняи от начальства, а ночью превращается в хакера по имени Нео, и нет места в сети, куда он не смог бы дотянуться. Но однажды всё меняется — герой, сам того не желая, узнаёт страшную правду: всё, что его окружает — не более, чем иллюзия, Матрица, а люди — всего лишь источник питания для искусственного интеллекта, поработившего человечество. И только Нео под силу изменить расстановку сил в этом ставшем вдруг чужим и страшным мире.',1,'covers/25.jpg',131,'https://www.youtube.com/embed/ihTvN2iCnhA',12,'2016-10-06 08:25:07'),(26,'Пианист',2002,'\r\nФильм снят по автобиографии Владислава Шпильмана, одного из лучших пианистов Польши 30-х годов прошлого века. Главный герой фильма — Владек — занимается искусством до тех пор, пока территорию Польши не занимают нацисты. Жизнь всех евреев меняется: их помещают в Варшавское гетто, запрещают работать, унижают, заставляют носить отличительные повязки, а через некоторое время отправляют в концлагерь.',6,'covers/26.jpg',149,'https://www.youtube.com/embed/u_jE7-6Uv7E',14,'2016-09-09 13:54:04'),(27,'Гладиатор',2000,'В великой Римской империи не было военачальника, равного генералу Максимусу. Непобедимые легионы, которыми командовал этот благородный воин, боготворили его и могли последовать за ним даже в ад.\r\n\r\nНо случилось так, что отважный Максимус, готовый сразиться с любым противником в честном бою, оказался бессилен против вероломных придворных интриг. Генерала предали и приговорили к смерти. Чудом избежав гибели, Максимус становится гладиатором.\r\n\r\nБыстро снискав себе славу в кровавых поединках, он оказывается в знаменитом римском Колизее, на арене которого он встретится в смертельной схватке со своим заклятым врагом…',1,'covers/27.jpg',155,'https://www.youtube.com/embed/FVx-Yp5vtb',15,'2016-10-06 08:29:24'),(29,'Бриллиантовая рука',1968,'Кинороман из жизни контрабандистов с прологом и эпилогом. В южном городке орудует шайка «валютчиков», возглавляемая Шефом и его помощником Графом. Скромный советский служащий и примерный семьянин Семен Семеныч Горбунков отправляется в зарубежный круиз на теплоходе, на котором также плывет Граф, который должен забрать бриллианты в одном из восточных городов и провезти их в загипсованной руке. Но в силу недоразумения вместо жулика на условленном месте падает ничего не подозревающий Семен Семенович и драгоценный гипс накладывают ему. Вот тут-то все и начинается…',9,'covers/29.jpg',94,'https://www.youtube.com/embed/FVx-Yp5vtb',16,'2016-10-06 16:39:06'),(30,'Американская история Х',1998,'Лидер местной банды скинхедов Дерек Виньярд прочно удерживает авторитет в своём районе. Убеждённый в своей правоте, он беспощадно расправляется с теми, кто имеет не белый цвет кожи. Независимость и смелость Дерека вызывают восхищение у его младшего брата Дэнни, который уже тоже сделал свой выбор.\r\n\r\nНо зверское убийство двух чернокожих парней, совершённое Дереком, разделяет дороги братьев: Дерек оказывается в тюрьме, где существует свой расклад сил, а Дэнни на свободе успешно продолжает дело брата. До тех пор, пока их пути не пересеклись вновь…',1,'covers/30.jpg',119,'https://www.youtube.com/embed/SgMOG7G-xNY',17,'2016-10-06 16:43:13'),(31,'Запах женщины',1992,'Наступил День благодарения, и отставной полковник разведки Фрэнк Слэйд решает справить праздник, «побаловав» себя поездкой в Нью-Йорк. Фрэнк хочет обставить свой последний «коронный выход» по высшему разряду: изысканный отель, шикарный лимузин, дорогая выпивка и женщины потрясающей красоты.\r\n\r\nЕсть лишь две проблемки. Первая: Фрэнк слеп на оба глаза. И вторая: волнующиеся родственники полковника решают нанять за небольшую сумму провожатого в лице нуждающегося студента престижного колледжа по имени Чарли Симмз. Последнее, что было нужно в поездке полковнику Слэйду — так это «зеленый» юнец Чарли. Но случилось так, что путешествие этих двух потрясающе не похожих друг на друга людей изменило их жизни навсегда.',1,'covers/31.jpg',156,'https://www.youtube.com/embed/Y4Dw2bsblWE',18,'2016-10-06 16:49:16'),(32,'Иван Сусанин',2000,'длавжрждлвждов',1,'covers/32.jpg',120,'https://www.youtube.com/embed/FVx-Yp5vtb',16,'2016-10-07 07:04:32');
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_actor`
--

DROP TABLE IF EXISTS `movie_actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_actor` (
  `movie_id` int(11) DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  KEY `movie_actor_movie_movie_id_fk` (`movie_id`),
  KEY `movie_actor_fk` (`actor_id`),
  CONSTRAINT `movie_actor_fk` FOREIGN KEY (`actor_id`) REFERENCES `actor` (`actor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `movie_actor_movie_movie_id_fk` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_actor`
--

LOCK TABLES `movie_actor` WRITE;
/*!40000 ALTER TABLE `movie_actor` DISABLE KEYS */;
INSERT INTO `movie_actor` VALUES (1,1),(1,2),(1,3),(1,4),(2,8),(2,9),(2,10),(2,12),(3,14),(3,8),(3,15),(8,44),(8,45),(8,46),(8,47),(9,51),(9,52),(9,53),(9,54),(9,55),(10,39),(10,40),(10,41),(11,56),(11,57),(11,58),(11,59),(12,60),(12,61),(12,62),(12,63),(13,64),(13,65),(13,66),(13,67),(13,68),(14,69),(14,70),(14,71),(14,72),(25,73),(26,77),(26,78),(26,79),(26,81),(25,74),(27,64),(27,82),(27,83),(27,84),(29,86),(29,87),(29,85),(30,51),(30,88),(30,89),(30,90),(30,91),(31,57),(31,92),(31,93),(32,86),(32,87),(32,84);
/*!40000 ALTER TABLE `movie_actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_genre`
--

DROP TABLE IF EXISTS `movie_genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_genre` (
  `movie_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`movie_id`,`genre_id`),
  KEY `movie_genre_genre_genre_id_fk` (`genre_id`),
  CONSTRAINT `movie_genre_genre_genre_id_fk` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`genre_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `movie_genre_movie_movie_id_fk` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_genre`
--

LOCK TABLES `movie_genre` WRITE;
/*!40000 ALTER TABLE `movie_genre` DISABLE KEYS */;
INSERT INTO `movie_genre` VALUES (1,1),(12,1),(14,1),(29,1),(32,1),(8,2),(25,2),(27,2),(1,4),(8,4),(9,4),(10,4),(11,4),(13,4),(26,4),(27,4),(30,4),(31,4),(9,5),(10,5),(11,5),(12,5),(30,5),(25,6),(26,7),(29,9),(32,9),(29,10),(32,10);
/*!40000 ALTER TABLE `movie_genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `movie_id` int(11) NOT NULL,
  `rating_sum` int(11) DEFAULT NULL,
  `user_sum` int(11) DEFAULT NULL,
  PRIMARY KEY (`movie_id`),
  CONSTRAINT `rating_movie_movie_id_fk` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` VALUES (1,54,7),(2,97,11),(3,14,6),(8,9,1),(9,1,9),(10,7,1),(25,10,1),(30,9,1),(31,8,1);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `review` text NOT NULL,
  `movie_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `review_date` date NOT NULL,
  `mark` int(11) DEFAULT '0',
  PRIMARY KEY (`review_id`),
  KEY `review_movie_movie_id_fk` (`movie_id`),
  KEY `review_user_user_id_fk` (`user_id`),
  CONSTRAINT `review_movie_movie_id_fk` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `review_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,'Просто великолепный фильм!!!!',1,1,'2016-08-11',1),(27,'jhkjhkjhkjhjkhj',25,1,'2016-09-18',0),(41,'sdfjg;jdgy5tgdfkmsgm;sdjfgkdfg',26,1,'2016-09-20',0),(43,'gdl\'f;lg\'lhkryk[g]relt54o65=39ykhtrog[fd',26,1,'2016-09-20',0),(53,'fhfghfghfgh',25,1,'2016-09-26',0),(54,'dgdfgdfgdfgdffgdgdgdgdfgdg',26,1,'2016-09-26',0),(55,'dgfgdfgdf',25,1,'2016-09-26',0),(57,'rtertertertert',25,1,'2016-09-26',0),(58,'fghghjklhgfdsaadfghj',25,1,'2016-09-26',0),(59,'cvbbcvbcvbcvbcvbc',26,1,'2016-09-26',0),(60,'dfgdfgfgddf',8,1,'2016-09-26',0),(62,'dfgdfgdfgdfgdfg',8,1,'2016-09-26',0),(63,'dfgdfgdfgdfgdfg',8,1,'2016-09-26',0),(64,'Великолепная актерская игра! Фильм стоит посмотреть',31,2,'2016-10-06',1),(65,'Великолепная драма, фильм который держит в напряжении до самого конца',30,2,'2016-10-06',0),(78,'Пожалуй лучший фильм Эдварда Нортона!',30,13,'2016-10-07',0),(79,'Великолепная история',30,13,'2016-10-07',0),(80,'Захватывающий фильм с неожиданной развязкой',8,13,'2016-10-07',0);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ADMINISTRATOR'),(2,'USER'),(3,'GUEST');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `login` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_banned` tinyint(1) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `user_role_role_id_fk` (`role_id`),
  CONSTRAINT `user_role_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Иван','Иванов','ivan@mail.ru','admin','21232f297a57a5a743894a0e4a801fc3',1,0,'avatars/1.jpg'),(2,'Sergey','Sergeev','serg@mail.ru','user','5be7f9344cc1a265e0f0d14cec1549b2',2,0,'avatars/2.jpg'),(8,'Ivan','Ivanov','qwerty@mail.ru','qwerty','d8578edf8458ce06fbc5bb76a58c5ca4',2,0,NULL),(9,'Ivan','Bautrk','csdocsdlcm@edfvd.by','kok','81dc9bdb52d04dc20036dbd8313ed055',2,0,NULL),(10,'Asdf','Kllllll','dsfjksldkjflksdj@mail.ru','asdada','d8578edf8458ce06fbc5bb76a58c5ca4',2,0,NULL),(11,'Иван','Иванов','mail@mail.ru','administrator','402fd6af80d80e346b96c89d37aae805',1,0,NULL),(12,'Сергей','Сергеев','eeeeeeeee@e.ee','user1','9f6673cb40f329ea7c5f300c4923f717',2,0,NULL),(13,'Николай','Голубицкий','elat@tt.com','elatum','d953da70a3cc397daf7e803720272a04',2,0,'avatars/13.jpg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_movie_rating`
--

DROP TABLE IF EXISTS `user_movie_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_movie_rating` (
  `user_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`movie_id`),
  KEY `user_movie_rating_movie_movie_id_fk` (`movie_id`),
  CONSTRAINT `user_movie_rating_movie_movie_id_fk` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_movie_rating_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_movie_rating`
--

LOCK TABLES `user_movie_rating` WRITE;
/*!40000 ALTER TABLE `user_movie_rating` DISABLE KEYS */;
INSERT INTO `user_movie_rating` VALUES (1,1,10),(1,2,10),(1,8,9),(1,9,9),(1,10,7),(1,25,10),(2,31,8),(13,30,9);
/*!40000 ALTER TABLE `user_movie_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_review`
--

DROP TABLE IF EXISTS `user_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_review` (
  `user_id` int(11) NOT NULL,
  `review_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`review_id`),
  KEY `user_rewiew_review_review_id_fk` (`review_id`),
  CONSTRAINT `user_rewiew_review_review_id_fk` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_rewiew_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_review`
--

LOCK TABLES `user_review` WRITE;
/*!40000 ALTER TABLE `user_review` DISABLE KEYS */;
INSERT INTO `user_review` VALUES (2,1),(1,64);
/*!40000 ALTER TABLE `user_review` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-24 17:32:56
