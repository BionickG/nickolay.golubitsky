<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="title">
<html>
    <head>
        <title><fmt:message key="result.page"/> </title>
        <style>
        .mainTable {
            border: 3px solid lightgrey;
            border-collapse: collapse;
            width: 99%;
        }
        td {
            padding: 5px;
            border: 3px solid lightgray;
        }
            th {
                font-size: x-large;
            }
        </style>
    </head>
    <body>
    <%@include file="include/language.jsp"%>
    <div align="center">
    <h3> ${medicineType} <fmt:message key="use"/> </h3>
    <table class="mainTable">
    <th align="center"colspan="11"> <fmt:message key="herbalmedicine"/> </th>
    <tr>
        <td rowspan="2"><fmt:message key="id"/> </td>
        <td rowspan="2"><fmt:message key="name"/> </td>
        <td rowspan="2"><fmt:message key="pharm"/> </td>
        <td rowspan="2"><fmt:message key="group"/></td>
        <td rowspan="2"><fmt:message key="analogs"/> </td>
        <td rowspan="2"><fmt:message key="hetbal"/> </td>
        <td colspan="4"><fmt:message key="versions"/> </td>

    </tr>
    <tr>
        <td><fmt:message key="form"/></td>
        <td><fmt:message key="certificate"/></td>
        <td><fmt:message key="package"/></td>
        <td><fmt:message key="dosage"/></td>
    </tr>
    <c:forEach var="element" begin="" items="${herbRes}" >
        <tr>
            <td>${element.id}</td>
            <td>${element.name}</td>
            <td>${element.pharm}</td>
            <td>${element.group}</td>
            <td>
                <c:forEach var="analog" items="${element.analogs}">
                    ${analog}<br>
                </c:forEach>
            </td>
            <td>${element.herbal}</td>
                <td>${element.medicineVersions[0].form}</td>
                <td>${element.medicineVersions[0].certificate}</td>
                <td>${element.medicineVersions[0].mPackage}</td>
                <td>${element.medicineVersions[0].dosage}</td>
                <tr></tr>
            <c:forEach var="version" begin="1" items="${element.medicineVersions}">
                <td colspan="6"></td>
                <td>${version.form}</td>
                <td>${version.certificate}</td>
                <td>${version.mPackage}</td>
                <td>${version.dosage}</td>
            </c:forEach>
        </tr>
    </c:forEach>
    </table><br>
    <table class="mainTable">
    <tr >
        <th align="center" colspan="11"> <fmt:message key="recipemedicine"/>  </th>
        <tr>
            <td rowspan="2"><fmt:message key="id"/> </td>
            <td rowspan="2"><fmt:message key="name"/> </td>
            <td rowspan="2"><fmt:message key="pharm"/> </td>
            <td rowspan="2"><fmt:message key="group"/></td>
            <td rowspan="2"><fmt:message key="analogs"/> </td>
            <td rowspan="2"><fmt:message key="recipe"/></td>
            <td colspan="4"><fmt:message key="versions"/></td>
        </tr>
        <tr>
            <td><fmt:message key="form"/></td>
            <td><fmt:message key="certificate"/></td>
            <td><fmt:message key="package"/></td>
            <td><fmt:message key="dosage"/></td>
        </tr>
        <c:forEach var="element" items="${recRes}" >
            <tr>
                <td>${element.id}</td>
                <td>${element.name}</td>
                <td>${element.pharm}</td>
                <td>${element.group}</td>
                <td>
                    <c:forEach var="analog" items="${element.analogs}">
                        ${analog}<br>
                    </c:forEach>
                </td>
                <td>
                    Выдал: ${element.recipe.doctor}<br>
                    Кому: ${element.recipe.patient}
                </td>
                <td>${element.medicineVersions[0].form}</td>
                <td>${element.medicineVersions[0].certificate}</td>
                <td>${element.medicineVersions[0].mPackage}</td>
                <td>${element.medicineVersions[0].dosage}</td>
            <tr></tr>
            <c:forEach var="version" begin="1" items="${element.medicineVersions}">
                <td colspan="6"></td>
                <td>${version.form}</td>
                <td>${version.certificate}</td>
                <td>${version.mPackage}</td>
                <td>${version.dosage}</td>
            </c:forEach>
            </tr>
        </c:forEach>
    </table>
    <br>
        <a href="controller?command=return">
            <button type="submit"><fmt:message key="return"/> </button>
        </a>
        <%@include file="include/logout.jsp"%>
    </div>
</body>
</html>
</fmt:bundle>