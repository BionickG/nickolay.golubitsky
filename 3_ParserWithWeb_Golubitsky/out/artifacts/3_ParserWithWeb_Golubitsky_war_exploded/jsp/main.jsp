<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="title">
<html>
<head>
    <title><fmt:message key="main.title"/> </title>
</head>
<body>
<%@ include file="include/language.jsp" %>
<table align="center">
    <th style="font-size: xx-large"> <fmt:message key="parsertype"/> </th>
        <form action="controller" method="post">
            <input type="hidden" name="command" value="Parse">
            <tr >
                <td align="center">
                    <input placeholder="<fmt:message key="parsertype"/> " name="type">
                </td>
            </tr>
            <tr>
                <td align="center"><input type="submit" value="<fmt:message key="confirm"/> "></td>
            </tr>
        </form>
        <tr>
            <td align="center">
                <%@include file="include/logout.jsp"%>
            </td>
        </tr>
        <tr align="center">
            <td>${wrongAction}</td>
        </tr>
</table>
</body>
</html>
</fmt:bundle>
