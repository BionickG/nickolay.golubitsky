<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="title">
<html>
<head>
    <title><fmt:message key="loginpage"/> </title>
</head>
<body>
<%@ include file="include/language.jsp" %>
<%@ include file="include/timeLocale.jsp" %>
<form  action="controller" method="POST">
    <input type="hidden" name="command" value="login">
    <table style="margin-top: 20%" align="center">
        <tr>
            <td align="center"> <input type="text" placeholder="<fmt:message key="login"/> " name="login" value="" size="20">
            </td>
        </tr>
        <tr >
            <td align="center"><input type="password" placeholder="<fmt:message key="password"/> " name="password" value="" size="20">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" value="<fmt:message key="confirm"/>"/>
            </td>
        </tr>
        <tr>
            <td>${errorLoginPassMessage}</td>
        </tr>
        <tr>
            <td>${wrongAction}</td>
        </tr>
        <tr>
            <td>${nullPage}</td>
        </tr>
    </table>
</form>
</body>
</html>
</fmt:bundle>