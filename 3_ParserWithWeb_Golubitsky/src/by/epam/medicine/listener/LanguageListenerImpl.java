package by.epam.medicine.listener;
/**
 * Created by Nickolay Golubitsky on 03.07.2016.
 */

import by.epam.medicine.config.ConfigurationManager;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


@WebListener()
public class LanguageListenerImpl implements HttpSessionListener{
    private static final String LOCALE = "locale";
    private static final String DEFAULT_LOCALE = "ru_RU";
    private static final String PAGE = "page";
    private static final String RETURN_PAGE = "path.page.index";

    public LanguageListenerImpl() {
    }

    public void sessionCreated(HttpSessionEvent se) {
        se.getSession().setAttribute(LOCALE, DEFAULT_LOCALE);
        String page = ConfigurationManager.getProperty(RETURN_PAGE);
        se.getSession().setAttribute(PAGE, page);
    }

    public void sessionDestroyed(HttpSessionEvent se) {
    }

}
