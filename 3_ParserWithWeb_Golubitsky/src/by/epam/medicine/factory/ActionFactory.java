package by.epam.medicine.factory;

import by.epam.medicine.command.ActionCommand;
import by.epam.medicine.command.CommandEnum;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nickolay Golubitsky on 28.06.2016.
 */
public class ActionFactory {
    public ActionCommand defineCommand(HttpServletRequest request){
        ActionCommand current;
        String action = request.getParameter("command");
        CommandEnum commandEnum = CommandEnum.valueOf(action.toUpperCase());
        current = commandEnum.getCurrentCommand();
        return current;
    }


}
