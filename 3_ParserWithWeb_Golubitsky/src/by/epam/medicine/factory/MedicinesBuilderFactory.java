package by.epam.medicine.factory;

import by.epam.medicine.parsing.AbstractMedicineBuilder;
import by.epam.medicine.parsing.MedicineDOMBuilder;
import by.epam.medicine.parsing.MedicineSAXBuilder;
import by.epam.medicine.parsing.MedicineStAXBuilder;

public class  MedicinesBuilderFactory {
    public AbstractMedicineBuilder createMedicineBuilder(ParserType type){
        AbstractMedicineBuilder builder = null;
        switch (type){
            case SAX:
                builder = new MedicineSAXBuilder();
                break;
            case STAX:
                builder = new MedicineStAXBuilder();
                break;
            case DOM:
                builder = new MedicineDOMBuilder();
                break;
            }
        return builder;
    }

    public enum ParserType {
        SAX,
        DOM,
        STAX() {
            @Override
            public String toString() {
                return "StAX";
            }
        }
    }

}
