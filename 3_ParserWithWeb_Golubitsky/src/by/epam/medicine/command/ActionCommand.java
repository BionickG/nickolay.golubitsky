package by.epam.medicine.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nickolay Golubitsky on 28.06.2016.
 */
public interface ActionCommand {
    String execute(HttpServletRequest request);
}
