package by.epam.medicine.command;

import by.epam.medicine.config.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nickolay Golubitsky on 28.06.2016.
 */
public class LogoutCommand implements ActionCommand {
    private final static String INDEX_PAGE = "path.page.index";
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(INDEX_PAGE);
        request.getSession().invalidate();
        return page;
    }
}
