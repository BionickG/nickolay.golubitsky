package by.epam.medicine.command;

import by.epam.medicine.config.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nickolay Golubitsky on 03.07.2016.
 */
public class ReturnCommand implements ActionCommand{
    private static final String MAIN_PAGE = "path.page.main";
    private static final String PAGE = "page";
    @Override
    public String execute(HttpServletRequest request) {
        String page =ConfigurationManager.getProperty(MAIN_PAGE);
        request.getSession().setAttribute(PAGE, page);
        return page;
    }
}
