package by.epam.medicine.command;

/**
 * Created by Nickolay Golubitsky on 28.06.2016.
 */
public enum  CommandEnum {
    LOGIN{
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT{
        {
            this.command = new LogoutCommand();
        }
    },
    PARSE{
        {
            this.command = new ParseCommand();
        }
    },
    LANGUAGE{
        {
            this.command = new LanguageCommand();
        }
    },
    RETURN{
        {
            this.command = new ReturnCommand();
        }
    };
    ActionCommand command;
    public ActionCommand getCurrentCommand() {
        return command;
    }
}

