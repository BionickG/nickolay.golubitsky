package by.epam.medicine.command;

import by.epam.medicine.config.ConfigurationManager;
import by.epam.medicine.config.MessageManager;
import by.epam.medicine.logic.LoginLogic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * Created by Nickolay Golubitsky on 28.06.2016.
 */
public class LoginCommand implements ActionCommand {
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String MAIN_PAGE = "path.page.main";
    private static final String LOGIN_PAGE = "path.page.login";
    private static final String ERROR_ATTRIBUTE = "errorLoginPassMessage";
    private static final String ERROR_MESSAGE = "message.loginerror";
    private static final String LOCALE = "locale";
    private static final String PAGE = "page";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String login = request.getParameter(PARAM_LOGIN);
        String pass = request.getParameter(PARAM_PASSWORD);
        HttpSession session = request.getSession();
        if(LoginLogic.authorizationLogic(login, pass)) {
            page = ConfigurationManager.getProperty(MAIN_PAGE);
            session.setAttribute(PAGE, page);
        } else{
            request.setAttribute(ERROR_ATTRIBUTE,
                    MessageManager.getProperty(ERROR_MESSAGE, (String) session.getAttribute(LOCALE)));
            page = ConfigurationManager.getProperty(LOGIN_PAGE);
        }
        return page;
    }
}
