package by.epam.medicine.command;

import by.epam.medicine.factory.ActionFactory;
import by.epam.medicine.factory.MedicinesBuilderFactory;
import by.epam.medicine.parsing.AbstractMedicineBuilder;
import by.epam.medicine.entity.HerbalMedicine;
import by.epam.medicine.entity.Medicine;
import by.epam.medicine.config.ConfigurationManager;
import by.epam.medicine.config.MessageManager;
import by.epam.medicine.entity.RecipeMedicine;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Nickolay Golubitsky on 28.06.2016.
 */
public class ParseCommand implements ActionCommand {
    private final static String DATA_PATH = "path.xml.parser";
    private final static String RESULT_PATH = "path.page.result";
    private final static String MAIN_PATH = "path.page.main";
    private final static String HERBAL_ATTRIBUTE = "herbRes";
    private final static String RECIPE_ATTRIBUTE = "recRes";
    private final static String TYPE_ATTRIBUTE = "medicineType";
    private final static String WRONG_ACTION = "wrongAction";
    private final static String WRONG_ACTION_MESSAGE = "message.wrongaction";
    private final static String LOCALE = "locale";
    private final static String TYPE = "type";
    private final static String PAGE = "page";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession();
        String type = request.getParameter(TYPE).toUpperCase();
        try {
            MedicinesBuilderFactory factory = new MedicinesBuilderFactory();
            AbstractMedicineBuilder builder = factory.createMedicineBuilder(MedicinesBuilderFactory.ParserType.valueOf(type));
            builder.buildSetMedicines(request.getServletContext().getRealPath("/") + ConfigurationManager.getProperty(DATA_PATH));
            ArrayList<RecipeMedicine> recList = new ArrayList<>();
            ArrayList<HerbalMedicine> herbList = new ArrayList<>();
            for ( Medicine medicine : builder. getMedicines()) {
                if (HerbalMedicine.class == medicine.getClass()) {
                    herbList.add((HerbalMedicine) medicine);
                } else {
                    recList.add((RecipeMedicine) medicine);
                }
            }
            recList.sort(Comparator.comparing(Medicine::getGroup).thenComparing(Medicine::getName));
            herbList.sort(Comparator.comparing(Medicine::getGroup).thenComparing(Medicine::getName));
            page = ConfigurationManager.getProperty(RESULT_PATH);
            session.setAttribute(PAGE, page);
            session.setAttribute(HERBAL_ATTRIBUTE, herbList);
            session.setAttribute(RECIPE_ATTRIBUTE, recList);
            session.setAttribute(TYPE_ATTRIBUTE, type);
        } catch (IllegalArgumentException e) {
            request.setAttribute(WRONG_ACTION, MessageManager.getProperty(WRONG_ACTION_MESSAGE, (String) session.getAttribute(LOCALE)));
            return ConfigurationManager.getProperty(MAIN_PATH);
        }

        return page;
    }
}
