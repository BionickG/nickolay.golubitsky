package by.epam.medicine.controller;


import by.epam.medicine.command.ActionCommand;
import by.epam.medicine.factory.ActionFactory;
import by.epam.medicine.config.ConfigurationManager;
import by.epam.medicine.config.MessageManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Nickolay Golubitsky on 26.06.2016.
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
    private static final String PAGE = "page";
    private static final String LOCALE = "locale";
    private static final String ERRORPAGE_PATH = "path.page.error";
    private static final String ATTR_NULLPAGE = "nullPage";
    private static final String NULLPAGE_MESSAGE = "message.nullpage";
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page;
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);
        if (request.getAttribute(PAGE)!= null) {
            request.getSession().setAttribute(PAGE, request.getAttribute(PAGE));
        }
        page = command.execute(request);
        if(page != null) {
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            page = ConfigurationManager.getProperty(ERRORPAGE_PATH);
            request.getSession().setAttribute(ATTR_NULLPAGE,
                    MessageManager.getProperty(NULLPAGE_MESSAGE, (String) request.getSession().getAttribute(LOCALE)));
            response.sendRedirect(request.getContextPath() + page);
        }

    }
}
