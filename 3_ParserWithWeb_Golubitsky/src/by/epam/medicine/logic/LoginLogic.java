package by.epam.medicine.logic;

import java.util.ResourceBundle;

/**
 * Created by Nickolay Golubitsky on 03.07.2016.
 */
public class LoginLogic {
    private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("resources.loginPassword");
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";

    public static boolean authorizationLogic(String login, String password){
        String trueLogin = RESOURCE_BUNDLE.getString(LOGIN);
        String truePassword = RESOURCE_BUNDLE.getString(PASSWORD);
        return login.equals(trueLogin)&& password.equals(truePassword);
    }
}
