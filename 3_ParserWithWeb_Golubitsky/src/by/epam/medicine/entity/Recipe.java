package by.epam.medicine.entity;

/**
 * Created by Nickolay Golubitsky on 25.05.2016.
 */
public class Recipe {
    private String patient;
    private String doctor;

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "patient='" + patient + '\'' +
                ", doctor='" + doctor + '\'' +
                '}';
    }
}
