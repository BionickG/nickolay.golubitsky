package by.epam.medicine.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Nickolay Golubitsky on 24.05.2016.
 */
public abstract class Medicine {
    private String name;
    private String id;
    private String pharm;
    private MedicineGroup group;
    private List<String> analogs = new ArrayList<>();
    private List<MedicineVersion> medicineVersions = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPharm() {
        return pharm;
    }

    public void setPharm(String pharm) {
        this.pharm = pharm;
    }

    public MedicineGroup getGroup() {
        return group;
    }

    public void setGroup(MedicineGroup group) {
        this.group = group;
    }

    public List<String> getAnalogs() {
        return Collections.unmodifiableList(analogs);
    }

    public void addAnalogs(String analog) {
        this.analogs.add(analog);
    }

    public List<MedicineVersion> getMedicineVersions() {
        return Collections.unmodifiableList(medicineVersions);
    }

    public void addMedicineVersion(MedicineVersion medicineVersion) {
        this.medicineVersions.add(medicineVersion);
    }

    @Override
    public String toString() {
        return  "name=" + name +
                " id=" + id +
                " pharm=" + pharm +
                " group=" + group +
                " analogs=" + analogs +
                " medicineVersions=" + medicineVersions;
    }
}
