package by.epam.medicine.entity;

/**
 * Created by Nickolay Golubitsky on 25.05.2016.
 */
public class RecipeMedicine extends Medicine{
    private Recipe recipe;

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public String toString() {
        return "RecipeMedicine{" +
                super.toString() +
                "recipe=" + recipe +
                '}' + "\n";
    }
}
