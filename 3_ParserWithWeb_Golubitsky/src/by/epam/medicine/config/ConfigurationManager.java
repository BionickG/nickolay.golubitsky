package by.epam.medicine.config;

import java.util.ResourceBundle;

/**
 * Created by Nickolay Golubitsky on 29.06.2016.
 */
public class ConfigurationManager {
    private final static String RESOURCES_PATH = "resources.config";
    private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(RESOURCES_PATH);
    public static String getProperty(String name) {
        return RESOURCE_BUNDLE.getString(name);
    }
}
