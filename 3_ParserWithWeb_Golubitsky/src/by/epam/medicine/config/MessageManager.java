package by.epam.medicine.config;

import java.util.ResourceBundle;

/**
 * Created by Nickolay Golubitsky on 29.06.2016.
 */
public class MessageManager {
    private static final String LINK_RU = "messages_ru_RU";
    private static final String LINK_EN = "messages_en_US";
    private static final String RUS = "ru_RU";
    private static final String ENG = "en_US";
    private static ResourceBundle resourceBundle;


    private MessageManager() {
    }

    public static String getProperty(String key, String locale) {
        switch (locale) {
            case RUS:
                resourceBundle = ResourceBundle.getBundle(LINK_RU);
                break;
            case ENG:
                resourceBundle = ResourceBundle.getBundle(LINK_EN);
                break;
            default:
                resourceBundle = ResourceBundle.getBundle(LINK_EN);
                break;
        }
        return resourceBundle.getString(key);
    }
}
