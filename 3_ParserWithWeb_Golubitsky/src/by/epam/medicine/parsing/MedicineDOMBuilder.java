package by.epam.medicine.parsing;

import by.epam.medicine.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nickolay Golubitsky on 26.05.2016.
 */
public class MedicineDOMBuilder extends AbstractMedicineBuilder {
    private final static Logger LOG = LogManager.getLogger();
    private Set<Medicine> medicines;
    private DocumentBuilder docBuilder;

    public MedicineDOMBuilder() {
        this.medicines = new HashSet<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.error(e.getMessage(), e);
        }
    }
    public Set<Medicine> getMedicines() {
        return medicines;
    }

    public void buildSetMedicines(String fileName){
        Document doc;
        try {
            doc = docBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList recipeMedicineList = root.getElementsByTagName(MedicineTag.RECIPE_MEDICINE.getFullTag());
            for (int i = 0; i < recipeMedicineList.getLength(); i++) {
                Element medicineElement = (Element) recipeMedicineList.item(i);
                Medicine medicine = buildRecipeMedicine(medicineElement);
                medicines.add(medicine);
            }
            NodeList herbalMedicineList = root.getElementsByTagName(MedicineTag.HERBAL_MEDICINE.getFullTag());
            for (int i = 0; i < herbalMedicineList.getLength(); i++) {
                Element medicineElement = (Element) herbalMedicineList.item(i);
                Medicine medicine = buildHerbalMedicine(medicineElement);
                medicines.add(medicine);
            }
        } catch (SAXException | IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }
    private RecipeMedicine buildRecipeMedicine (Element medicineElement)
    {
        RecipeMedicine medicine = new RecipeMedicine();
        buildMedicineCore(medicineElement, medicine);
        Element recipeElement = (Element) medicineElement.getElementsByTagName(MedicineTag.RECIPE.getFullTag()).item(0);
        Recipe recipe = new Recipe();
        String doctor = getElementTextContent(recipeElement, MedicineTag.DOCTOR.getFullTag());
        recipe.setDoctor(doctor);
        String patient = getElementTextContent(recipeElement, MedicineTag.PATIENT.getFullTag());
        recipe.setPatient(patient);
        medicine.setRecipe(recipe);
        return medicine;
    }
    private HerbalMedicine buildHerbalMedicine(Element medicineElement){
        HerbalMedicine medicine = new HerbalMedicine();
        buildMedicineCore(medicineElement, medicine);
        medicine.setHerbal(getElementTextContent(medicineElement, MedicineTag.HERBAL.getFullTag()));
        return medicine;
    }
    private void buildMedicineCore(Element medicineElement, Medicine medicine)
    {
        medicine.setId(medicineElement.getAttribute(MedicineTag.ID.getTag()));
        medicine.setName(medicineElement.getAttribute(MedicineTag.NAME.getTag()));
        medicine.setPharm(getElementTextContent(medicineElement, MedicineTag.PHARM.getFullTag()));
        NodeList analogs = medicineElement.getElementsByTagName(MedicineTag.ANALOGS.getFullTag());
        for (int i = 0; i < analogs.getLength(); i++) {
            medicine.addAnalogs(analogs.item(i).getTextContent());
        }
        String group = getElementTextContent(medicineElement, MedicineTag.GROUP.getFullTag()).toUpperCase();
        medicine.setGroup(MedicineGroup.valueOf(group));
        NodeList versions = medicineElement.getElementsByTagName(MedicineTag.VERSIONS.getFullTag());
        for (int i = 0; i < versions.getLength(); i++) {
            Element versionElement = (Element) versions.item(i);
            MedicineVersion version = buildVersion(versionElement);
            medicine.addMedicineVersion(version);
        }
    }

    private MedicineVersion buildVersion(Element versionElement){
        MedicineVersion version = new MedicineVersion();
        String form = getElementTextContent(versionElement, MedicineTag.FORM.getFullTag()).trim().toUpperCase();
        version.setForm(MedicineForm.valueOf(form));
        Certificate certificate = new Certificate();
        Node cetif = versionElement.getElementsByTagName(MedicineTag.CERTIFICATE.getFullTag()).item(0);
        Element certificateElement = (Element) cetif;
        String number = getElementTextContent(certificateElement, MedicineTag.NUMBER.getFullTag());
        certificate.setNumber(number);
        certificate.setOrganization(getElementTextContent(certificateElement, MedicineTag.ORGANIZATION.getFullTag()));
        certificate.setDates(getElementTextContent(certificateElement, MedicineTag.DATES.getFullTag()));
        version.setCertificate(certificate);
        Node pack = versionElement.getElementsByTagName(MedicineTag.PACKAGE.getFullTag()).item(0);
        Element packageElement = (Element) pack;
        MedicinePackage packagE = new MedicinePackage();
        String type = getElementTextContent(packageElement, MedicineTag.TYPE.getFullTag()).toUpperCase();
        packagE.setType(PackageType.valueOf(type));
        int count = Integer.parseInt(getElementTextContent(packageElement, MedicineTag.COUNT.getFullTag()));
        packagE.setCount(count);
        double price = Double.parseDouble(getElementTextContent(packageElement, MedicineTag.PRICE.getFullTag()));
        packagE.setPrice(price);
        version.setPackage(packagE);
        version.setDosage(getElementTextContent(versionElement, MedicineTag.DOSAGE.getFullTag()));
        return version;
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        return node.getTextContent().trim();
    }

}
