package by.epam.medicine.parsing;

import by.epam.medicine.entity.*;
import by.epam.medicine.exception.MedicineParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nickolay Golubitsky on 28.05.2016.
 */
public class MedicineStAXBuilder extends AbstractMedicineBuilder {
    private static final Logger LOG = LogManager.getLogger();
    private Set<Medicine> medicines = new HashSet<>();
    private XMLInputFactory inputFactory;

    public MedicineStAXBuilder() {
        inputFactory = XMLInputFactory.newInstance();
    }

    public Set<Medicine> getMedicines() {
        return medicines;
    }
    public void buildSetMedicines (String fileName)
    {
        XMLStreamReader reader;
        String name;
        try (FileInputStream inputStream = new FileInputStream(fileName)){
             reader = inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (MedicineTag.RECIPE_MEDICINE.getTag().equals(name)) {
                        RecipeMedicine medicine = new RecipeMedicine();
                        medicine = (RecipeMedicine) buildMedicine(medicine, reader);
                        medicines.add(medicine);
                    }
                    if (MedicineTag.HERBAL_MEDICINE.getTag().equals(name)) {
                        HerbalMedicine medicine = new HerbalMedicine();
                        medicine = (HerbalMedicine) buildMedicine(medicine, reader);
                        medicines.add(medicine);
                    }
                }
            }
        } catch (IOException | XMLStreamException | MedicineParseException e) {
            LOG.error(e.getMessage(), e);
        }

    }
    private Medicine buildMedicine (Medicine medicine, XMLStreamReader reader) throws XMLStreamException, MedicineParseException {
        HerbalMedicine herbalMedicine = null;
        RecipeMedicine recipeMedicine = null;
        medicine.setName(reader.getAttributeValue(null, MedicineTag.NAME.toString().toLowerCase()));
        medicine.setId(reader.getAttributeValue(null, MedicineTag.ID.toString().toLowerCase()));
        String name;
        while (reader.hasNext())
        {
            int type = reader.next();
            switch (type){
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (MedicineTag.valueOf(name.toUpperCase())){
                        case PHARM: medicine.setPharm(getXMLText(reader));
                            break;
                        case GROUP: medicine.setGroup(MedicineGroup.valueOf(getXMLText(reader).toUpperCase()));
                            break;
                        case ANALOGS: medicine.addAnalogs(getXMLText(reader));
                            break;
                        case VERSIONS: medicine.addMedicineVersion(buildMedicineVersion(reader));
                            break;
                        case HERBAL: herbalMedicine = (HerbalMedicine) medicine;
                            herbalMedicine.setHerbal(getXMLText(reader));
                            break;
                        case RECIPE: recipeMedicine = (RecipeMedicine) medicine;
                            recipeMedicine.setRecipe(buildRecipe(reader));
                            break;
                        default: throw new MedicineParseException("not valid by.epam.medicine tag");
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (MedicineTag.HERBAL_MEDICINE.getTag().equals(name)){
                        return herbalMedicine;
                    }
                    if (MedicineTag.RECIPE_MEDICINE.getTag().equals(name)) {
                        return recipeMedicine;
                    }
                    break;
            }
        }
        throw new MedicineParseException("Medicine build exception");
    }

    private MedicineVersion buildMedicineVersion(XMLStreamReader reader) throws XMLStreamException, MedicineParseException {
        MedicineVersion version = new MedicineVersion();
        String text;
        while (reader.hasNext()){
            int type = reader.next();
            switch (type){
                case XMLStreamConstants.START_ELEMENT:
                    text = reader.getLocalName();
                    switch (MedicineTag.valueOf(text.toUpperCase())){
                        case FORM: version.setForm(MedicineForm.valueOf(getXMLText(reader).toUpperCase()));
                            break;
                        case DOSAGE: version.setDosage(getXMLText(reader));
                            break;
                        case CERTIFICATE: version.setCertificate(buildCertificate(reader));
                            break;
                        case PACKAGE: version.setPackage(buildPackage(reader));
                            break;
                        default: throw new MedicineParseException("not valid version tag");
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    text = reader.getLocalName();
                    if (MedicineTag.VERSIONS.getTag().equals(text))
                    {
                        return version;
                    }
                    break;
            }
        }
        throw new MedicineParseException("Version build exception");
    }
    private Certificate buildCertificate(XMLStreamReader reader) throws XMLStreamException, MedicineParseException {
        Certificate certificate = new Certificate();
        String certificateText;
        while(reader.hasNext()){
            int certificateType = reader.next();
            switch (certificateType){
                case XMLStreamConstants.START_ELEMENT:
                    certificateText = reader.getLocalName();
                    switch (MedicineTag.valueOf(certificateText.toUpperCase())){
                        case NUMBER: certificate.setNumber(getXMLText(reader));
                            break;
                        case DATES: certificate.setDates(getXMLText(reader));
                            break;
                        case ORGANIZATION: certificate.setOrganization(getXMLText(reader));
                            break;
                        default: throw new MedicineParseException("not valid certificate tag");
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    certificateText = reader.getLocalName();
                    if (MedicineTag.CERTIFICATE.getTag().equals(certificateText))
                    {
                        return certificate;
                    }
                    break;
            }
        }
        throw new MedicineParseException("Certificate build exception");
    }
    private MedicinePackage buildPackage(XMLStreamReader reader) throws XMLStreamException, MedicineParseException {
        MedicinePackage aPackage = new MedicinePackage();
        String text;
        while (reader.hasNext()){
            int type = reader.next();
            switch (type){
                case XMLStreamConstants.START_ELEMENT:
                    text = reader.getLocalName();
                    switch (MedicineTag.valueOf(text.toUpperCase())){
                        case TYPE: aPackage.setType(PackageType.valueOf(getXMLText(reader).toUpperCase()));
                            break;
                        case COUNT: aPackage.setCount(Integer.parseInt(getXMLText(reader)));
                            break;
                        case PRICE: aPackage.setPrice(Double.parseDouble(getXMLText(reader)));
                            break;
                        default: throw new MedicineParseException("not valid package type");
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    text = reader.getLocalName();
                    if (MedicineTag.PACKAGE.getTag().equals(text))
                    {
                        return aPackage;
                    }
                    break;
            }
        }
        throw new MedicineParseException("Certificate build exception");
    }
    private Recipe buildRecipe(XMLStreamReader reader) throws XMLStreamException, MedicineParseException {
        Recipe recipe = new Recipe();
        String text;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type){
                case XMLStreamConstants.START_ELEMENT:
                    text = reader.getLocalName();
                    switch (MedicineTag.valueOf(text.toUpperCase())){
                        case DOCTOR: recipe.setDoctor(getXMLText(reader));
                            break;
                        case PATIENT: recipe.setPatient(getXMLText(reader));
                            break;
                        default: throw new MedicineParseException("not valid recipe");
                    } break;
                case XMLStreamConstants.END_ELEMENT:
                    text = reader.getLocalName();
                    if (text.equals(MedicineTag.RECIPE.getTag())) {
                        return recipe;
                    }break;
            }
        }
        throw new MedicineParseException("Recipe build exception");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText().trim();
        }
        return text;
    }

}
