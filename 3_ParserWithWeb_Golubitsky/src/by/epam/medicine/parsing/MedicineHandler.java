package by.epam.medicine.parsing;

import by.epam.medicine.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import static by.epam.medicine.entity.MedicineTag.HERBAL_MEDICINE;
import static by.epam.medicine.entity.MedicineTag.RECIPE_MEDICINE;

/**
 * Created by Nickolay Golubitsky on 28.05.2016.
 */
public class MedicineHandler extends DefaultHandler {
    private Set<Medicine> medicines;
    private Medicine current;
    private HerbalMedicine currentHerbal;
    private RecipeMedicine currentRecipe;
    private MedicineTag currentEnum;
    private Set<MedicineTag> withText;
    private MedicineVersion version;
    private Certificate certificate;
    private MedicinePackage mPackage;
    private Recipe recipe;
    private String herbal;

    public Set<Medicine> getMedicines() {
        return medicines;
    }

    public MedicineHandler() {
        medicines = new HashSet<>();
        withText = EnumSet.range(MedicineTag.VERSIONS, MedicineTag.HERBAL);

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException {
        if (HERBAL_MEDICINE.getFullTag().equals(qName))
        {
            current = new HerbalMedicine();
            current.setName(attr.getValue(0));
            current.setId(attr.getValue(1));
        } else if (RECIPE_MEDICINE.getFullTag().equals(qName))
        {
            current = new RecipeMedicine();
            current.setName(attr.getValue(0));
            current.setId(attr.getValue(1));
        } else {
                MedicineTag tmp = MedicineTag.valueOf(localName.toUpperCase());
                if (withText.contains(tmp)){
                currentEnum = tmp;
                }
        }

    }
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (RECIPE_MEDICINE.getFullTag().equals(qName))
        {
            currentRecipe = (RecipeMedicine) current;
            currentRecipe.setRecipe(recipe);
            medicines.add(currentRecipe);
        }
        if (HERBAL_MEDICINE.getFullTag().equals(qName))
        {
            currentHerbal = (HerbalMedicine) current;
            currentHerbal.setHerbal(herbal);
            medicines.add(currentHerbal);
        }
        currentEnum = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String tmp = new String(ch, start, length).trim();
        if (currentEnum != null){
            switch (currentEnum){
                case PHARM: current.setPharm(tmp);
                    break;
                case GROUP: current.setGroup(MedicineGroup.valueOf(tmp.toUpperCase()));
                    break;
                case ANALOGS: current.addAnalogs(tmp);
                    break;
                case VERSIONS: version = new MedicineVersion();
                    break;
                case FORM: version.setForm(MedicineForm.valueOf(tmp.toUpperCase()));
                    break;
                case CERTIFICATE: certificate = new Certificate();
                    break;
                case NUMBER: certificate.setNumber(tmp);
                    break;
                case DATES: certificate.setDates(tmp);
                    break;
                case ORGANIZATION: certificate.setOrganization(tmp);
                    version.setCertificate(certificate);
                    break;
                case PACKAGE: mPackage = new MedicinePackage();
                    break;
                case TYPE: mPackage.setType(PackageType.valueOf(tmp.toUpperCase()));
                    break;
                case COUNT: mPackage.setCount(Integer.parseInt(tmp));
                    break;
                case PRICE: mPackage.setPrice(Double.parseDouble(tmp));
                    version.setPackage(mPackage);
                    break;
                case DOSAGE: version.setDosage(tmp);
                    current.addMedicineVersion(version);
                    break;
                case RECIPE: recipe = new Recipe();
                    break;
                case DOCTOR: recipe.setDoctor(tmp);
                    break;
                case PATIENT: recipe.setPatient(tmp);
                    break;
                case HERBAL: herbal = tmp;
                    break;
                default: throw new EnumConstantNotPresentException(
                        currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }
    }
}
