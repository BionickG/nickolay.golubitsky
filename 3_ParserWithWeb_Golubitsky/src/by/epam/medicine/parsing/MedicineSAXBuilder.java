package by.epam.medicine.parsing;

/**
 * Created by Nickolay Golubitsky on 26.05.2016.
 */

import by.epam.medicine.entity.Medicine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.Set;


public class MedicineSAXBuilder extends AbstractMedicineBuilder {
    private final static Logger LOG = LogManager.getLogger();
    private Set<Medicine> medicines;
    private MedicineHandler medicineHandler;
    private XMLReader reader;

    public MedicineSAXBuilder() {
        medicineHandler = new MedicineHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(medicineHandler);
        } catch (SAXException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public Set<Medicine> getMedicines() {
        return medicines;
    }

    public void buildSetMedicines(String fileName){
        try {
            reader.parse(fileName);
        } catch (IOException | SAXException e) {
            LOG.error(e.getMessage(), e);
        }
        medicines = medicineHandler.getMedicines();
    }
}
