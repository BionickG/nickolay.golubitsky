package by.epam.medicine.entity;

/**
 * Created by Nickolay Golubitsky on 25.05.2016.
 */
public class Certificate {
    private String number;
    private String dates;
    private String organization;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "Certificate{" +
                "number=" + number +
                ", dates='" + dates + '\'' +
                ", organization='" + organization + '\'' +
                '}';
    }
}
