package by.epam.medicine.entity;

/**
 * Created by Nickolay Golubitsky on 24.05.2016.
 */
public class MedicineVersion {
    private MedicineForm form;
    private Certificate certificate;
    private MedicinePackage mPackage;
    private String dosage;

    public MedicineForm getForm() {
        return form;
    }

    public void setForm(MedicineForm form) {
        this.form = form;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public MedicinePackage getmPackage() {
        return mPackage;
    }

    public void setPackage(MedicinePackage aPackage) {
        this.mPackage = aPackage;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    @Override
    public String toString() {
        return "MedicineVersion{" +
                "form=" + form +
                ", certificate=" + certificate +
                ", mPackage=" + mPackage +
                ", dosage='" + dosage + '\'' +
                '}';
    }
}
