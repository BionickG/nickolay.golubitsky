package by.epam.medicine.entity;

/**
 * Created by Nickolay Golubitsky on 26.05.2016.
 */
public enum  MedicineTag {
    MEDICINES("medicines"),
    RECIPE_MEDICINE("recipe-medicine"),
    HERBAL_MEDICINE("herbal-medicine"),
    VERSIONS("versions"),PACKAGE("package"),
    CERTIFICATE("certificate"), RECIPE("recipe"),
    ID("id"), NAME("name"),
    FORM("form"),
    PHARM("pharm"),
    GROUP("group"),
    ANALOGS("analogs"),
    DOSAGE("dosage"),
    NUMBER("number"),
    DATES("dates"),
    ORGANIZATION("organization"),
    TYPE("type"),
    COUNT("count"),
    PRICE("price"),
    DOCTOR("doctor"),
    PATIENT("patient"),
    HERBAL("herbal");

    private String tag;
    private final String nameSpace = "ns:";

    MedicineTag(String tag) {
        this.tag = tag;
    }

    public String getFullTag() {
        return nameSpace + tag;
    }
    public String getTag(){
        return tag;
    }

}
