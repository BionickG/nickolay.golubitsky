package by.epam.medicine.entity;

/**
 * Created by Nickolay Golubitsky on 25.05.2016.
 */
public class HerbalMedicine extends Medicine {
    private String herbal;

    public String getHerbal() {
        return herbal;
    }

    public void setHerbal(String herbal) {
        this.herbal = herbal;
    }

    @Override
    public String toString() {
        return "HerbalMedicine{" +
                super.toString() +
                "herbal=" + herbal  + " " +
                '}' + "\n";
    }
}
