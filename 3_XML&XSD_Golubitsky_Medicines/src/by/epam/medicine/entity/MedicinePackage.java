package by.epam.medicine.entity;

/**
 * Created by Nickolay Golubitsky on 24.05.2016.
 */
public class MedicinePackage {
    PackageType type;
    private int count;
    private double price;

    public PackageType getType() {
        return type;
    }

    public void setType(PackageType type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "MedicinePackage{" +
                "type=" + type +
                ", count=" + count +
                ", price=" + price +
                '}';
    }
}
