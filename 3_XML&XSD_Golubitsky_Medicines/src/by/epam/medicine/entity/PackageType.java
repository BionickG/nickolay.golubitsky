package by.epam.medicine.entity;

/**
 * Created by Nickolay Golubitsky on 25.05.2016.
 */
public enum  PackageType {
    GLASS,
    METAL,
    PLASTIC,
    CARTON
}
