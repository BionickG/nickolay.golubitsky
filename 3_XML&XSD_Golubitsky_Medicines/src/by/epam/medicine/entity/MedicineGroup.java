package by.epam.medicine.entity;

/**
 * Created by Nickolay Golubitsky on 24.05.2016.
 */
public enum MedicineGroup {
    ANTIPYRETIC,
    ANALGETIC,
    ANTIBIOTIC,
    ANTISEPTIC,
    STIMULANT,
    HORMONES,
    VITAMINS
}
