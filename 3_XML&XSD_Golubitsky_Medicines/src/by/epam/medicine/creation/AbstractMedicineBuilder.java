package by.epam.medicine.creation;

import by.epam.medicine.entity.Medicine;

import java.util.*;

/**
 * Created by Nickolay Golubitsky on 30.05.2016.
 */
public abstract class AbstractMedicineBuilder {
    protected Set<Medicine> medicines;

    public AbstractMedicineBuilder() {
        medicines = new HashSet<>();
    }
    public Set<Medicine> getMedicines(){
        return medicines;
    }
    abstract public void buildSetMedicines(String fileName);
}

