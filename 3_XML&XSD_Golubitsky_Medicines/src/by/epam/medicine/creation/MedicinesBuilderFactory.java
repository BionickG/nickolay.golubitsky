package by.epam.medicine.creation;

import by.epam.medicine.parsing.MedicineDOMBuilder;
import by.epam.medicine.parsing.MedicineSAXBuilder;
import by.epam.medicine.parsing.MedicineStAXBuilder;


/**
 * Created by Nickolay Golubitsky on 30.05.2016.
 */
public class MedicinesBuilderFactory {
    public enum ParserType {
        SAX,
        DOM,
        STAX() {
            @Override
            public String toString() {
                return "StAX";
            }
        }
    }
    public AbstractMedicineBuilder createMedicineBuilder(ParserType type){
        AbstractMedicineBuilder builder = null;
        switch (type){
            case SAX:
                builder = new MedicineSAXBuilder();
                break;
            case STAX:
                builder = new MedicineStAXBuilder();
                break;
            case DOM:
                builder = new MedicineDOMBuilder();
                break;
        }
        return builder;
    }
}
