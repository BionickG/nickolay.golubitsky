package by.epam.medicine.exception;

/**
 * Created by Nickolay Golubitsky on 13.06.2016.
 */
public class MedicineParseException extends Exception{
    public MedicineParseException() {
    }

    public MedicineParseException(String message) {
        super(message);
    }

    public MedicineParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public MedicineParseException(Throwable cause) {
        super(cause);
    }
}
