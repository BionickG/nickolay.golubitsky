package by.epam.medicine.main;

import by.epam.medicine.creation.MedicinesBuilderFactory;
import by.epam.medicine.entity.Medicine;
import by.epam.medicine.parsing.MedicineDOMBuilder;
import by.epam.medicine.parsing.MedicineSAXBuilder;
import by.epam.medicine.parsing.MedicineStAXBuilder;
import by.epam.medicine.validator.ValidatorSAXXSD;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Nickolay Golubitsky on 06.06.2016.
 */
public class Main {
    public static void main(String[] args) {
        ValidatorSAXXSD.valid();
        MedicinesBuilderFactory factory = new MedicinesBuilderFactory();
        MedicineDOMBuilder builder = (MedicineDOMBuilder) factory.createMedicineBuilder(MedicinesBuilderFactory.ParserType.DOM);
        builder.buildSetMedicines("data/medicine.xml");

        ArrayList<Medicine> medicines = new ArrayList<>(builder.getMedicines());
        medicines.sort(Comparator.comparing(Medicine::getGroup).thenComparing(Medicine::getPharm));

        System.out.println(medicines);



        MedicineStAXBuilder stAXBuilder = (MedicineStAXBuilder) factory.createMedicineBuilder(MedicinesBuilderFactory.ParserType.STAX);
        stAXBuilder.buildSetMedicines("data/medicine.xml");
        System.out.println(stAXBuilder.getMedicines());


        MedicineSAXBuilder saxBuilder = (MedicineSAXBuilder) factory.createMedicineBuilder(MedicinesBuilderFactory.ParserType.SAX);
        saxBuilder.buildSetMedicines("data/medicine.xml");
        System.out.println(saxBuilder.getMedicines());



    }
}
