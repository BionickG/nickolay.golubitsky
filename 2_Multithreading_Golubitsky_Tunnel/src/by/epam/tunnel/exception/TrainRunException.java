package by.epam.tunnel.exception;

/**
 * Created by Nickolay Golubitsky on 06.06.2016.
 */
public class TrainRunException extends Exception {
    public TrainRunException() {
    }

    public TrainRunException(String message) {
        super(message);
    }

    public TrainRunException(String message, Throwable cause) {
        super(message, cause);
    }

    public TrainRunException(Throwable cause) {
        super(cause);
    }
}
