package by.epam.tunnel.main;


import by.epam.tunnel.entity.ControllerOffice;

/**
 * Created by Nickolay Golubitsky on 21.05.2016.
 */
public class Main {
    public static void main(String[] args) {
        ControllerOffice office = ControllerOffice.getInstance();
        office.startControl();
    }
}
