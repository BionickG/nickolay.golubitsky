package by.epam.tunnel.logic;

import by.epam.tunnel.entity.Train;
import by.epam.tunnel.entity.Tunnel;
import by.epam.tunnel.exception.TrainRunException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static by.epam.tunnel.entity.Tunnel.MAX_ONE_DIRECTION_COUNT;
import static by.epam.tunnel.entity.Tunnel.SEMAPHORE_SIZE;

/**
 * Created by Nickolay Golubitsky on 13.06.2016.
 */
public class Trip {
    static final Logger LOG = LogManager.getLogger();
    private Tunnel tunnel;
    private Train lastTrain;
    private Semaphore westSemaphore;
    private Semaphore eastSemaphore;
    private AtomicInteger counter;
    private AtomicInteger trainsInTunnel;

    public Trip(Tunnel tunnel) {
        this.tunnel = tunnel;
        westSemaphore = tunnel.getWestSemaphore();
        eastSemaphore = tunnel.getEastSemaphore();
        counter = tunnel.getCounter();
        trainsInTunnel = tunnel.getTrainsInTunnel();
    }

    public void tryToPass(Train train) throws TrainRunException {
        try {
            if (!westSemaphore.hasQueuedThreads() && !eastSemaphore.hasQueuedThreads()) {
                if (train.isDirection()) {
                    westSemaphore.acquire();
                    eastSemaphore.drainPermits();
                } else {
                    eastSemaphore.acquire();
                    westSemaphore.drainPermits();
                }
            } else {
                if (train.isDirection()) {
                    westSemaphore.acquire();
                } else {
                    eastSemaphore.acquire();
                }
            }
        } catch (InterruptedException e) {
            throw new TrainRunException(e);
        }
    }

    public void passThrough(Train train)  {
        try {
            lastTrain = train;
            LOG.info(train.toString() + train.getTrainId() + " going into tunnel " + tunnel.getTunnelId());
            trainsInTunnel.getAndIncrement();
            counter.getAndIncrement();
            TimeUnit.SECONDS.sleep(3);
            LOG.info(train.toString() + train.getTrainId() + " is leaving tunnel " + tunnel.getTunnelId());
            trainsInTunnel.getAndDecrement();
            semaphoreRelease(train);
        } catch (InterruptedException e) {
            LOG.error(e.getMessage());
        }
    }

    private void semaphoreRelease(Train train){
        if (train.isDirection()){
            releasing(train, westSemaphore, eastSemaphore);
        } else {
            releasing(train, eastSemaphore, westSemaphore);
        }
    }

    private void releasing(Train train, Semaphore mainSemaphore, Semaphore secondarySemaphore)
    {
        if (counter.get() == MAX_ONE_DIRECTION_COUNT) {
            if (trainsInTunnel.get() == 0)
            {
                LOG.info(train.toString() + train.getTrainId() + " is last");
                LOG.info("_____________Change direction_____________");
                secondarySemaphore.release(SEMAPHORE_SIZE);
                counter.getAndSet(0);
            } else {
                LOG.info(train.toString() + train.getTrainId() +  " not last train");
            }
        } else if (!mainSemaphore.hasQueuedThreads() && train.equals(lastTrain)){
            secondarySemaphore.release(SEMAPHORE_SIZE);
        } else if (!mainSemaphore.hasQueuedThreads() && !secondarySemaphore.hasQueuedThreads()){
            mainSemaphore.release(SEMAPHORE_SIZE);
            secondarySemaphore.release(SEMAPHORE_SIZE);
        }else {
            mainSemaphore.release();
        }
    }
}
