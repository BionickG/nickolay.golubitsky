package by.epam.tunnel.entity;

import by.epam.tunnel.exception.TrainRunException;
import by.epam.tunnel.logic.Trip;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nickolay Golubitsky on 23.05.2016.
 */
public class Train extends Thread {
    final static Logger LOG = LogManager.getLogger();
    private boolean direction;
    private Tunnel tunnel;
    private int trainId;

    public Train(int trainId, boolean direction, Tunnel tunnel) {
        this.trainId = trainId;
        this.direction = direction;
        this.tunnel = tunnel;
    }

    private void getTimeBeforeTunnel(){
        try {
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(5000));
            LOG.info(this.toString()  + trainId + " came to tunnel " + tunnel.getTunnelId());
        } catch (InterruptedException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void run() {
        Trip trip = new Trip(this.getTunnel());
        try {
            getTimeBeforeTunnel();
            trip.tryToPass(this);
            trip.passThrough(this);
        } catch (TrainRunException e) {
            LOG.error(e.getMessage(), e);
        }


    }

    public Tunnel getTunnel() {
        return tunnel;
    }

    public void setTunnel(Tunnel tunnel) {
        this.tunnel = tunnel;
    }

    public boolean isDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public int getTrainId() {
        return trainId;
    }

    public void setTrainId(int trainId) {
        this.trainId = trainId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Train)) return false;

        Train train = (Train) o;

        if (direction != train.direction) return false;
        if (trainId != train.trainId) return false;
        return tunnel != null ? tunnel.equals(train.tunnel) : train.tunnel == null;

    }

    @Override
    public int hashCode() {
        int result = (direction ? 1 : 0);
        result = 31 * result + (tunnel != null ? tunnel.hashCode() : 0);
        result = 31 * result + trainId;
        return result;
    }

    @Override
    public String toString() {
        String direction;
        if (this.direction == true){
            direction = "West";
        } else {
            direction = "East";
        }
        return direction + " " + "Train " ;
    }
}
