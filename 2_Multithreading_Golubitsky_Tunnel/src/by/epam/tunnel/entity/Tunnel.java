package by.epam.tunnel.entity;


import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Nickolay Golubitsky on 23.05.2016.
 */
public class Tunnel{

    public final static int SEMAPHORE_SIZE = 2;
    public final static int MAX_ONE_DIRECTION_COUNT = 4;
    private final Semaphore westSemaphore = new Semaphore(SEMAPHORE_SIZE);
    private final Semaphore eastSemaphore = new Semaphore(SEMAPHORE_SIZE);
    private AtomicInteger counter = new AtomicInteger(0);
    private AtomicInteger trainsInTunnel = new AtomicInteger(0);
    private long tunnelId;

    public Tunnel(long tunnelId) {
        this.tunnelId = tunnelId;
    }

    public AtomicInteger getCounter() {
        return counter;
    }

    public void setCounter(AtomicInteger counter) {
        this.counter = counter;
    }

    public AtomicInteger getTrainsInTunnel() {
        return trainsInTunnel;
    }

    public void setTrainsInTunnel(AtomicInteger trainsInTunnel) {
        this.trainsInTunnel = trainsInTunnel;
    }

    public long getTunnelId() {
        return tunnelId;
    }

    public void setTunnelId(long tunnelId) {
        this.tunnelId = tunnelId;
    }

    public Semaphore getWestSemaphore() {
        return westSemaphore;
    }

    public Semaphore getEastSemaphore() {
        return eastSemaphore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tunnel)) return false;

        Tunnel tunnel = (Tunnel) o;

        if (tunnelId != tunnel.tunnelId) return false;
        if (!westSemaphore.equals(tunnel.westSemaphore)) return false;
        return eastSemaphore.equals(tunnel.eastSemaphore);

    }

    @Override
    public int hashCode() {
        int result = westSemaphore.hashCode();
        result = 31 * result + eastSemaphore.hashCode();
        result = 31 * result + (int) (tunnelId ^ (tunnelId >>> 32));
        return result;
    }
}
