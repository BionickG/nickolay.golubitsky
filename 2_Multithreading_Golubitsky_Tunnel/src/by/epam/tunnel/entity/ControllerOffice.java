package by.epam.tunnel.entity;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created by Nickolay Golubitsky on 23.05.2016.
 */

public class ControllerOffice{

    private ArrayList<Train> trains = new ArrayList<>();
    private Tunnel tunnelOne = new Tunnel(1);
    private Tunnel tunnelTwo = new Tunnel(2);
    private static ReentrantLock lock = new ReentrantLock();
    private static ControllerOffice instance;
    private static AtomicBoolean isInstanceCreated = new AtomicBoolean(false);

    private ControllerOffice() {

    }

    public static ControllerOffice getInstance(){
        if (!isInstanceCreated.get()){
            try {
                lock.lock();
                if (instance == null){
                    instance = new ControllerOffice();
                    isInstanceCreated.getAndSet(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    private void randomFillIn(Tunnel tunnel){
        Random random = new Random();
        for (int i = 0; i < 15; i++) {
            trains.add(new Train(i, random.nextBoolean(), tunnel));
            }
    }

    public void startControl() {
        randomFillIn(tunnelOne);
        randomFillIn(tunnelTwo);
        trains.forEach(Thread::start);
    }

}