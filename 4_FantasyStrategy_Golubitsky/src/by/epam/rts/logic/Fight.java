package by.epam.rts.logic;

import by.epam.rts.entity.Squad;

import java.util.ArrayList;

/**
 * Created by Nickolay Golubitsky on 18.06.2016.
 */
public class Fight {
    public static void twoArmiesFight(ArrayList<Squad> units){
        Squad humans = units.get(0);
        Squad orcs = units.get(1);
        // & not && to call either methods
        while (!humans.isDead() & !orcs.isDead() ) {
            orcs.attack(humans);
            humans.attack(orcs);
        }
        Squad winner = humans.isDead()?orcs:humans;
        System.out.println(winner);
    }
}
