package by.epam.rts.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

/**
 * Created by Nickolay Golubitsky on 18.06.2016.
 */
public class Soldier implements Unit, Cloneable{

    private Random random = new Random();
    private boolean isCast = false;
    private boolean isBlock;
    private int health;
    private int armor;
    private int maxAttack;
    private int minAttack;
    private double critChance;

    public Soldier() {
    }

    public void block() {
        if(Double.compare(random.nextDouble(), AdditionalStats.BLOCK_CHANCE.getStat()) < 0 )
        {isBlock = true;}

    }

    public void cast(Soldier oponent) {
        if (Double.compare(random.nextDouble(), AdditionalStats.CAST_CHANCE.getStat()) < 0)
        {oponent.setHealth(( getHealth()- (int) Math.round(AdditionalStats.SPELL_DAMAGE.getStat())));}
    }
    @Override
    public void attack(Squad squad) {
        Random random = new Random();
        int size = squad.getAllUnits().size();
        Soldier opponent = squad.getAllUnits().get(random.nextInt(size));
        opponent.block();
        int attack = maxAttack > opponent.getArmor() ? maxAttack : minAttack;
        if (random.nextInt((int)Math.round(AdditionalStats.CRITICAL.getStat())) < critChance) {
            attack = attack*2;
        }
        if (isCast){
            cast(opponent);
        }
        if (!isBlock) {
            opponent.setHealth(getHealth() - attack);
        } else {
            opponent.setHealth(getHealth() - 1);
            isBlock = false;
        }
    }

    @Override
    public boolean isDead() {
        return this.getHealth() <= 0;
    }

    public boolean isCast() {
        return isCast;
    }

    public void setCast(boolean cast) {
        isCast = cast;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxAttack() {
        return maxAttack;
    }

    public void setMaxAttack(int maxAttack) {
        this.maxAttack = maxAttack;
    }

    public int getMinAttack() {
        return minAttack;
    }

    public void setMinAttack(int minAttack) {
        this.minAttack = minAttack;
    }

    public boolean isBlock() {
        return isBlock;
    }

    public void setBlock(boolean block) {
        isBlock = block;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public double getCritChance() {
        return critChance;
    }

    public void setCritChance(double critChance) {
        this.critChance = critChance;
    }

    @Override
    public Soldier clone() throws CloneNotSupportedException {
        return (Soldier) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Soldier)) return false;

        Soldier soldier = (Soldier) o;

        if (health != soldier.health) return false;
        if (maxAttack != soldier.maxAttack) return false;
        if (minAttack != soldier.minAttack) return false;
        if (armor != soldier.armor) return false;
        return Double.compare(soldier.critChance, critChance) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = health;
        result = 31 * result + maxAttack;
        result = 31 * result + minAttack;
        result = 31 * result + armor;
        temp = Double.doubleToLongBits(critChance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Soldier{" +
                "health=" + health +
                '}';
    }
}
