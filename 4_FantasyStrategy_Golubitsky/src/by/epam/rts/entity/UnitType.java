package by.epam.rts.entity;

/**
 * Created by Nickolay Golubitsky on 23.06.2016.
 */
public enum  UnitType {
    ARCHER,
    KNIGHT,
    MAGE,
    COMMANDER,
    SPEARMAN,
    GRUNT,
    SHAMAN

}
