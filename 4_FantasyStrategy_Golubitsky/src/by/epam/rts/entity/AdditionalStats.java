package by.epam.rts.entity;

/**
 * Created by Nickolay Golubitsky on 08.07.2016.
 */
public enum AdditionalStats {
    BLOCK_CHANCE(0.10),
    CAST_CHANCE(0.20),
    CRITICAL(100),
    SPELL_DAMAGE(100);

    private final double stat;

    AdditionalStats(double stat) {
        this.stat = stat;
    }

    public double getStat() {
        return stat;
    }
}
