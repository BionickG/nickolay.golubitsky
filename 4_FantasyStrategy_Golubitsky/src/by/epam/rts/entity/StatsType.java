package by.epam.rts.entity;

/**
 * Created by Nickolay Golubitsky on 08.07.2016.
 */
public enum StatsType {
    HEALTH,
    MAXATTACK,
    MINATTACK,
    ARMOR,
    CRITCHANCE
}
