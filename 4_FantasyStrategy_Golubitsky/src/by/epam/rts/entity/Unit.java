package by.epam.rts.entity;

/**
 * Created by Nickolay Golubitsky on 17.06.2016.
 */
public interface Unit {
    void attack(Squad squad);
    boolean isDead();
}
