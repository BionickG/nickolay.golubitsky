package by.epam.rts.entity;

import java.util.ArrayList;

/**
 * Created by Nickolay Golubitsky on 17.06.2016.
 */
public class Squad implements Unit {
    private String squadName;
    private ArrayList<Unit> units = new ArrayList<>();

    private ArrayList<Unit> getUnits() {
        return units;
    }
    public void addUnits(Unit unit) {
        units.add(unit);
    }
    public String getSquadName() {
        return squadName;
    }

    public void setSquadName(String squadName) {
        this.squadName = squadName;
    }

    public ArrayList<Soldier> getAllUnits(){
        ArrayList<Soldier> units = new ArrayList<>();
        for (Unit unit : getUnits()){
            if (unit.getClass() != getClass()){
                units.add((Soldier) unit);
            } else {
                Squad tmp = (Squad) unit;
                units.addAll(tmp.getAllUnits());
            }
        }
        return units;
    }
    @Override
    public void attack(Squad squad) {
            for (Unit unit : units) {
                unit.attack(squad);
            }
    }
    @Override
    public boolean isDead() {
        units.removeIf(Unit::isDead);
        return units.isEmpty();
    }

    @Override
    public String toString() {
        return squadName + units;
    }
}
