package by.epam.rts.exception;

/**
 * Created by Nickolay Golubitsky on 23.06.2016.
 */
public class SoldierCreateException extends Exception{

    public SoldierCreateException(String message) {
        super(message);
    }

    public SoldierCreateException(String message, Throwable cause) {
        super(message, cause);
    }

    public SoldierCreateException(Throwable cause) {
        super(cause);
    }

    public SoldierCreateException() {

    }
}
