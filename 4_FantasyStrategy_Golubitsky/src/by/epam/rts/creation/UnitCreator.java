package by.epam.rts.creation;

import by.epam.rts.entity.Soldier;
import by.epam.rts.entity.StatsType;
import by.epam.rts.entity.UnitType;
import by.epam.rts.exception.SoldierCreateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Nickolay Golubitsky on 20.06.2016.
 */
public class UnitCreator {
    private final static String FILE_PATH = "resources/unitStats.json";
    private final static Logger LOG = LogManager.getLogger();
    private final static HashMap<UnitType, Soldier> soldiersStats = new HashMap<>();

    public void createSoldiers(){
        try(FileInputStream fis = new FileInputStream(FILE_PATH)) {
            Scanner scanner = new Scanner(fis);
            StringBuilder builder = new StringBuilder();
            while (scanner.hasNext()) {
                builder.append(scanner.nextLine().trim());
            }
            Pattern pattern = RegexType.JSON_SUBOBJECT.getPattern();
            Matcher matcher = pattern.matcher(builder.toString());
            String unit;
            while (matcher.find()) {
                unit =  matcher.group();
                oneSoldierCreation(unit);
            }
        } catch (IOException | SoldierCreateException e){
            LOG.error(e.getMessage(), e);
        }
    }
    private void oneSoldierCreation(String unit) throws SoldierCreateException {
        Soldier soldier = new Soldier();
        Pattern typePattern = RegexType.JSON_NAME.getPattern();
        Matcher typeMatcher = typePattern.matcher(unit);
        UnitType type = null;
        while (typeMatcher.find()){
            type = UnitType.valueOf(typeMatcher.group().toUpperCase());
        }
        Pattern fieldsPattern = RegexType.JSON_FIELD.getPattern();
        Matcher fieldsMatcher = fieldsPattern.matcher(unit);
        String field;
        while (fieldsMatcher.find()){
            field = fieldsMatcher.group();
            statsCreate(field, soldier);
        }
        if (type.equals(UnitType.MAGE)||type.equals(UnitType.MAGE)){
            soldier.setCast(true);
        }
        soldiersStats.put(type, soldier);
    }

    private Soldier statsCreate(String field, Soldier soldier) throws SoldierCreateException {
        Pattern namePattern = RegexType.JSON_NAME.getPattern();
        Matcher nameMatcher = namePattern.matcher(field);
        Pattern statPattern = RegexType.JSON_VALUE.getPattern();
        Matcher statMatcher = statPattern.matcher(field);
        StatsType name;
        int stat;
        while (nameMatcher.find()&& statMatcher.find()){
            name = StatsType.valueOf(nameMatcher.group().toUpperCase());
            stat = Integer.parseInt(statMatcher.group().trim());
            switch (name){
                case HEALTH: soldier.setHealth(stat);
                    break;
                case MAXATTACK: soldier.setMaxAttack(stat);
                    break;
                case MINATTACK: soldier.setMinAttack(stat);
                    break;
                case ARMOR: soldier.setArmor(stat);
                    break;
                case CRITCHANCE: soldier.setCritChance(stat);
                    break;
                default: throw new SoldierCreateException("unknown stat!");
            }
        }
        return soldier;
    }

    public static HashMap<UnitType, Soldier> getSoldiersStats() {
        return soldiersStats;
    }
}
