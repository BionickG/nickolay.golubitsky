package by.epam.rts.creation;

import java.util.regex.Pattern;

/**
 * Created by Nickolay Golubitsky on 06.07.2016.
 */
public enum RegexType {
    JSON_OBJECT("\".+?\"\\s*:\\s*\\{([^{}]+|\\{[^{}]+\\})+\\}"),
    JSON_SUBOBJECT("(?<=[{,])\"[^\"]+\":\\s*\\{[^}]+\\}"),
    JSON_FIELD("\"[^\"]+\"\\s*:\\s*\\d+"),
    JSON_NAME("(?<=^\").*?(?=\")"),
    JSON_VALUE("[^\\w]\\d+");

    private Pattern pattern;

    RegexType(String regexType) {
        pattern = Pattern.compile(regexType);
    }

    public Pattern getPattern() {
        return pattern;
    }
}