package by.epam.rts.creation;

import by.epam.rts.entity.Soldier;
import by.epam.rts.entity.Squad;
import by.epam.rts.entity.Unit;
import by.epam.rts.entity.UnitType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nickolay Golubitsky on 05.07.2016.
 */
public class ArmyCreator {
    private final static Logger LOG = LogManager.getLogger();
    private final static String FILE_PATH = "resources/armies.json";
    private ArrayList<Squad> armies = new ArrayList<>();
    UnitCreator creator = new UnitCreator();

    public ArmyCreator() {
        creator.createSoldiers();
    }

    public void create(){
        try(FileInputStream fis = new FileInputStream(FILE_PATH)) {
            Scanner scanner = new Scanner(fis);
            StringBuilder builder = new StringBuilder();
            while (scanner.hasNext()) {
                builder.append(scanner.nextLine().trim());
            }
            Pattern pattern = RegexType.JSON_OBJECT.getPattern();
            Matcher matcher = pattern.matcher(builder);
            String oneArmy;
            while (matcher.find()) {
                oneArmy =  matcher.group();
                armies.add(oneSquadCreator(oneArmy));
            }
        } catch (IOException e){
            LOG.error(e.getMessage(), e);
        }
    }
    private Squad oneSquadCreator(String armyString){
        Squad squad = new Squad();
        Pattern namePattern = RegexType.JSON_NAME.getPattern();
        Matcher nameMatcher = namePattern.matcher(armyString);
        while (nameMatcher.find()){
            squad.setSquadName(nameMatcher.group());
        }
        Pattern subibjectPattern = RegexType.JSON_SUBOBJECT.getPattern();
        Matcher subobjectMatcher = subibjectPattern.matcher(armyString);
        StringBuffer sb = new StringBuffer();
        String squadString;
        while (subobjectMatcher.find()){
            squadString = subobjectMatcher.group();
            squad.addUnits(oneSquadCreator(squadString));
            subobjectMatcher.appendReplacement(sb, "");
        }
        subobjectMatcher.appendTail(sb);
        if (sb.length() == 0){
            sb.append(armyString);
        }
        Pattern fieldsPattern = RegexType.JSON_FIELD.getPattern();
        Matcher fieldsMatcher = fieldsPattern.matcher(sb);
        while (fieldsMatcher.find()){
            unitsAdd(fieldsMatcher.group(), squad);
        }
        return squad;
    }
    private void unitsAdd(String field, Squad squad){
        UnitType type = null;
        int count = 0;
        Pattern patternKey = RegexType.JSON_NAME.getPattern();
        Matcher matcherKey = patternKey.matcher(field);
        Pattern patternValue = RegexType.JSON_VALUE.getPattern();
        Matcher matcherValue = patternValue.matcher(field);
        while (matcherKey.find()&& matcherValue.find()) {
            type = UnitType.valueOf(matcherKey.group().toUpperCase());
            String countString = matcherValue.group().trim();
            count = Integer.parseInt(countString);
        }
        for (int i = 0; i < count; i++) {
            try {
                squad.addUnits((Unit) creator.getSoldiersStats().get(type).clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Squad> getArmies() {
        return armies;
    }
}
