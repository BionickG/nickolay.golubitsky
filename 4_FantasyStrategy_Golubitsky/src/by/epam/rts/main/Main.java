package by.epam.rts.main;

import by.epam.rts.creation.ArmyCreator;
import by.epam.rts.logic.Fight;



/**
 * Created by Nickolay Golubitsky on 18.06.2016.
 */
public class Main {
    public static void main(String[] args) {
        ArmyCreator creator = new ArmyCreator();
        creator.create();
        Fight.twoArmiesFight(creator.getArmies());
    }
}
