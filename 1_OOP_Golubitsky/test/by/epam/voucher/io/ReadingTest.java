package by.epam.voucher.io;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Created by Nickolay Golubitsky on 20.05.2016.
 */
public class ReadingTest {
    @Test
    public void readFile() throws Exception {
        File hotel = new File("resources/hotels.txt");
        File tours = new File("resources/tours.txt");
        Assert.assertTrue(hotel.exists());
        Assert.assertTrue(tours.exists());
    }

}