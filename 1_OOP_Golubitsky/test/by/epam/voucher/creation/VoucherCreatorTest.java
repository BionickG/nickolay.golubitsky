package by.epam.voucher.creation;

import by.epam.voucher.entity.*;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

/**
 * Created by Nickolay Golubitsky on 20.05.2016.
 */
public class VoucherCreatorTest {
    @Test
    public void createVoucher() throws Exception {
        SeaTour expected = new SeaTour();
        Voucher actual = VoucherCreator.createVoucher(" SEA_TOUR :  15 :  BUS   :  2 :  1 :  AI  :  AagunaResort0 :  ITALY   :  1990-03-01 :  1990-03-16 : 12 :  true");
        expected.setDuration(15);
        expected.setTransport(Transport.BUS);
        expected.setNumberOfAdults(2);
        expected.setNumberOfChildren(1);
        expected.setFoodType(FoodType.AI);
        BookingService.fillIn();
        BookingService.hotels.stream().filter(hotel -> "AagunaResort0".equals(hotel.getHotelName())).forEach(expected::setHotel);
        expected.setCountry(Country.ITALY);
        expected.setDepartureDate(LocalDate.parse("1990-03-01"));
        expected.setArriveDate(LocalDate.parse("1990-03-16"));
        expected.setDistanceToSea(12);
        expected.setHot(true);
        expected.setCost();
        Assert.assertEquals(expected, actual);

    }

}