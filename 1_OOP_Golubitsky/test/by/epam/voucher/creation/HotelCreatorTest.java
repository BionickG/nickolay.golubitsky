package by.epam.voucher.creation;

import by.epam.voucher.entity.Hotel;
import by.epam.voucher.entity.RoomType;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Nickolay Golubitsky on 20.05.2016.
 */
public class HotelCreatorTest {
    @Test
    public void createHotel() throws Exception {
        Hotel expected = new Hotel();
        Hotel actual = HotelCreator.createHotel("AagunaResort0 : SGL : 3 : 400 ");
        expected.setHotelName("AagunaResort0");
        expected.setRoomType(RoomType.SGL);
        expected.setStarship(3);
        expected.setCostPerNight(400);
        Assert.assertEquals(expected, actual);
    }

}