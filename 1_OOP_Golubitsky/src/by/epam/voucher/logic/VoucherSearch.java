package by.epam.voucher.logic;

import by.epam.voucher.entity.FoodType;
import by.epam.voucher.entity.TourOperator;
import by.epam.voucher.entity.Voucher;
import by.epam.voucher.io.Writing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by Nickolay Golubitsky on 19.05.2016.
 */
public class VoucherSearch {
    final static Logger LOG = LogManager.getLogger();
    final static String RESULT_PATH = "results/result.txt";

    public static void searchByDuration(int duration, TourOperator operator) {
        ArrayList<Voucher> result = new ArrayList<>();
        if (operator.getVouchers().size() != 0) {
            for (Voucher voucher : operator.getVouchers())
                if (voucher.getDuration() == duration) {
                    result.add(voucher);
                }
        } else {
            LOG.info(operator + " have no vouchers");

        }
        Writing.writeFile(result, RESULT_PATH);
    }
    public static void searchByFootType(FoodType foodType, TourOperator operator){
        ArrayList<Voucher> result = new ArrayList<>();
        if (operator.getVouchers().size() != 0) {
            for (Voucher voucher : operator.getVouchers())
                if (voucher.getFoodType().equals(foodType)) {
                    result.add(voucher);
                }
        } else {
            LOG.info(operator + " have no vouchers");
        }
        Writing.writeFile(result, RESULT_PATH);
    }
    public static void searchByTransport(String transport, TourOperator operator) {
        ArrayList<Voucher> result = new ArrayList<>();
        if (operator.getVouchers().size() != 0) {
            for (Voucher voucher : operator.getVouchers()) {
                if (voucher.getTransport().toString().equals(transport)) {
                    result.add(voucher);
                }
            }
        } else {
            LOG.info(operator + " have no vouchers");
        }
        Writing.writeFile(result, RESULT_PATH);
    }
    public static void searchByHot(boolean isHot, TourOperator operator)
    {
        ArrayList<Voucher> result = new ArrayList<>();
        if (operator.getVouchers().size() != 0) {
            for (Voucher voucher : operator.getVouchers()) {
                if (voucher.isHot() == isHot) {
                    result.add(voucher);
                }
            }
        } else {
            LOG.info(operator + " have no vouchers");
        }
        Writing.writeFile(result, RESULT_PATH);
    }
}
