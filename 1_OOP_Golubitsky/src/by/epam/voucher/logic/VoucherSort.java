package by.epam.voucher.logic;

import by.epam.voucher.entity.TourOperator;
import by.epam.voucher.entity.Voucher;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Nickolay Golubitsky on 19.05.2016.
 */
public class VoucherSort {
    public static void sortByCost(TourOperator operator)
    {
        ArrayList<Voucher> vouchers = operator.getVouchers();
        Collections.sort(vouchers, (o1, o2) -> {
            double cost1 = o1.getCost();
            double cost2 = o2.getCost();
            if (cost1 > cost2){
                return 1;
            }else if (cost1 < cost2){
                return -1;
            }else {
                return 0;
            }
        });
        operator.setVouchers(vouchers);
    }
    public static void sortByCountry(TourOperator operator)
    {
        ArrayList<Voucher> vouchers = operator.getVouchers();
        Collections.sort(operator.getVouchers(), (o1, o2) -> {
            String country1 = o1.getCountry().toString();
            String country2 = o2.getCountry().toString();
            if (country1 == country2) {
                return 0;
            }
            if (country1 == null) {
                return -1;
            }
            if (country2 == null) {
                return 1;
            }
            return country1.compareTo(country2);
        });
        operator.setVouchers(vouchers);
    }
    public static void sortByTransport(TourOperator operator)
    {
        ArrayList<Voucher> vouchers = operator.getVouchers();
        Collections.sort(operator.getVouchers(), (o1, o2) -> {
                String transport1 = o1.getTransport().toString();
                String transport2 = o2.getTransport().toString();
                if (transport1 == transport2) {
                    return 0;
                }
                if (transport1 == null) {
                    return -1;
                }
                if (transport2 == null) {
                    return 1;
                }
                return transport1.compareTo(transport2);

        });
        operator.setVouchers(vouchers);
    }
    public static void sortByDyration(TourOperator operator)
    {
        ArrayList<Voucher> vouchers = operator.getVouchers();
        Collections.sort(operator.getVouchers(), (o1, o2) -> {
            int duration1 = o1.getDuration();
            int duration2 = o2.getDuration();
            return duration1-duration2;
        });
        operator.setVouchers(vouchers);
    }
}
