package by.epam.voucher.io;

import by.epam.voucher.exception.CorruptFileException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Nickolay Golubitsky on 14.05.2016.
 */
public class Reading {
    static final Logger LOG = LogManager.getLogger();

    public static ArrayList<String> readFile (String fileName) {
        ArrayList<String> list = new ArrayList<>();
        File inputFile = new File(fileName);
        try (Scanner fileReader = new Scanner(inputFile)){
            while(fileReader.hasNextLine())
            {
                String string = fileReader.nextLine();
                if (!string.isEmpty()) {
                    list.add(string);
                }
            }
        } catch (FileNotFoundException e) {
            LOG.error(e.getMessage());
        }
        return list;
    }
}
