package by.epam.voucher.io;

import by.epam.voucher.entity.Voucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Nickolay Golubitsky on 14.05.2016.
 */
public class Writing {
    static final Logger LOG = LogManager.getLogger();

    public static void writeFile(ArrayList<Voucher> vouchers, String filePath)
    {
        File outputFile = new File(filePath);
        try(FileWriter fw = new FileWriter(outputFile);
            PrintWriter writer = new PrintWriter(fw)) {

            for (Voucher voucher : vouchers) {
                writer.println(voucher.toString());
            }
        } catch (IOException e){
            LOG.error("Write to file error" + e.getMessage());
        }

    }
}
