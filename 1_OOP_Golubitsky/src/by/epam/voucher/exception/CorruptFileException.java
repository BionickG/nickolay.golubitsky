package by.epam.voucher.exception;

/**
 * Created by Nickolay Golubitsky on 16.05.2016.
 */
public class CorruptFileException extends Exception {
    public CorruptFileException() {
    }

    public CorruptFileException(String message) {
        super(message);
    }

    public CorruptFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public CorruptFileException(Throwable cause) {
        super(cause);
    }
}
