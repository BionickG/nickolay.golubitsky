package by.epam.voucher.main;


import by.epam.voucher.entity.TourOperator;
import by.epam.voucher.logic.VoucherSearch;
import by.epam.voucher.logic.VoucherSort;

/**
 * Created by Nickolay Golubitsky on 14.05.2016.
 */
public class Main {
    public static void main(String[] args) {
        TourOperator operator = new TourOperator();
        operator.fillIn();
        VoucherSort.sortByCost(operator);
        VoucherSearch.searchByHot(true, operator);
    }

}
