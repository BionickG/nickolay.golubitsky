package by.epam.voucher.creation;

import by.epam.voucher.entity.*;
import by.epam.voucher.exception.CorruptFileException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * Created by Nickolay Golubitsky on 17.05.2016.
 */
public class VoucherCreator {
    final static Logger LOG = LogManager.getLogger();
    public static Voucher createVoucher (String inputString)
    {

        String[] params = inputString.split(" : ");
        if (params.length != 12) {
            try {
                throw new CorruptFileException("Incorrect number of options");
            } catch (CorruptFileException e) {
                LOG.error(e.getMessage());
            }
        }
        String typeString = params[0].trim().toUpperCase();
        String durationString = params[1].trim();
        String transportString = params[2].trim().toUpperCase();
        String adultsString = params[3].trim();
        String childrenString = params[4].trim();
        String foodString = params[5].trim().toUpperCase();
        String hotelString = params[6].trim();
        String countryString = params[7].trim().toUpperCase();
        String departureString = params[8].trim();
        String arriveString = params[9].trim();
        String optionString = params[10].trim();
        String hotString = params[11].trim().toLowerCase();
        Voucher voucher = null;
        VoucherType type = null;
        try {
            type = VoucherType.valueOf(typeString);
        } catch (IllegalArgumentException e){
            LOG.error("Voucher type error : " + e.getMessage());
        }
        switch (type){
            case SEA_TOUR:
                int distance;
                try {
                    distance = Integer.parseInt(optionString);
                    voucher = new SeaTour(distance);
                }catch (NumberFormatException e){
                    LOG.error("Distance to the sea error : " + e.getMessage());
                }
                break;
            case SHOPPING:
                int shopNumber;
                try {
                    shopNumber = Integer.parseInt(optionString);
                    voucher = new Shopping(shopNumber);
                }catch (NumberFormatException e){
                    LOG.error("Number of shops sea error : " + e.getMessage());
                }
                break;
            case CRUISE:
                String seaName = optionString;
                voucher = new Cruise(seaName);
                break;
            case EXCURSION:
                int excursionNumber;
                try {
                    excursionNumber = Integer.parseInt(optionString);
                    voucher = new Excursion(excursionNumber);
                }catch (NumberFormatException e){
                    LOG.error("Number of excursion error : " + e.getMessage());
                }
                break;
            case HUNTING:
                String wildfowl = optionString;
                voucher = new Hunting(wildfowl);
                break;
            case SKI_TOUR:
                String mountainName = optionString;
                voucher = new SkiTour(mountainName);
                break;
        }
        int duration = 0;
        try {
            duration = Integer.parseInt(durationString);
        } catch (NumberFormatException e) {
            LOG.warn("Duration error : " + e.getMessage());
        }
        Transport transport = null;
        try {
            transport = Transport.valueOf(transportString);
        } catch (IllegalArgumentException e){
            LOG.warn("Transport error : " + e.getMessage());
        }
        int numberOfAdults = 0;
        int numberOfChildren = 0;
        try {
            numberOfAdults = Integer.parseInt(adultsString);
            numberOfChildren = Integer.parseInt(childrenString);
        } catch (NumberFormatException e) {
            LOG.warn("Number of people : " + e.getMessage());
        }
        FoodType foodType = null;
        try {
            foodType = FoodType.valueOf(foodString);
        } catch (IllegalArgumentException e){
            LOG.warn("FoodType error : " + e.getMessage());
        }
        Hotel hotel = null;
        BookingService.fillIn();
        for (Hotel hot : BookingService.hotels)
        {
            if (hotelString.equalsIgnoreCase(hot.getHotelName()))
                hotel = hot;
        }
        if (hotel == null){
                LOG.warn("Hotel error : No such hotel");

        }
        Country country = null;
        try {
            country = Country.valueOf(countryString);
        } catch (IllegalArgumentException e){
            LOG.warn("Country error : " + e.getMessage());
        }
        LocalDate departure = null;
        LocalDate arrive = null;
        try {
            departure = LocalDate.parse(departureString);
            arrive = LocalDate.parse(arriveString);
        } catch (DateTimeParseException e) {
            LOG.warn("Departure or arrive data error : " + e.getMessage());
        }
        boolean isHot = false;
        switch (hotString){
            case "true" : isHot = true; break;
            case "false" : isHot = false; break;
            default:
                try {
                throw new CorruptFileException("corrupt isHot string");
            } catch (CorruptFileException e) {
                LOG.warn("is hot error : " + e.getMessage());
            }
        }
        voucher.setDuration(duration);
        voucher.setTransport(transport);
        voucher.setNumberOfAdults(numberOfAdults);
        voucher.setNumberOfChildren(numberOfChildren);
        voucher.setFoodType(foodType);
        voucher.setHotel(hotel);
        voucher.setDepartureDate(departure);
        voucher.setArriveDate(arrive);
        voucher.setCountry(country);
        voucher.setHot(isHot);
        if (hotel != null) {
            voucher.setCost();
        }
        return voucher;
    }
}
