package by.epam.voucher.creation;

import by.epam.voucher.entity.Hotel;
import by.epam.voucher.entity.RoomType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Created by Nickolay Golubitsky on 18.05.2016.
 */
public class HotelCreator {
    final static Logger LOG = LogManager.getLogger();

    public static Hotel createHotel(String inputString)
    {
        Hotel hotel = new Hotel();
        String [] params = inputString.split(" : ");
        String hotelName = params[0].trim();
        String typeString = params[1].trim();
        String starshipString = params[2].trim();
        String costString = params[3].trim();
            int starship = 0;
            try{
                starship = Integer.parseInt(starshipString);
            } catch (NumberFormatException e)
            {
                LOG.error("Starship error :" + e.getMessage());
            }
            RoomType roomType = null;
            try {
                roomType = RoomType.valueOf(typeString);
            }catch (IllegalArgumentException e){
                LOG.error("Roomtype error : " + e.getMessage());
            }
            double cost = 0;
            try {
                cost = Double.parseDouble(costString);
            } catch (NumberFormatException e){
                LOG.error("Price error : " + e.getMessage());
            }
        hotel.setHotelName(hotelName);
        hotel.setStarship(starship);
        hotel.setRoomType(roomType);
        hotel.setCostPerNight(cost);

        return hotel;
    }
}
