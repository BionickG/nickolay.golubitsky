package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 14.05.2016.
 */
public enum Transport {
    PLAN(0.15),
    TRAIN(0.12),
    BUS(0.05);

    private double costPerKm;

    public double getCostPerKM() {
        return costPerKm;
    }
    Transport(double cost) {
        this.costPerKm = cost;
    }
}
