package by.epam.voucher.entity;

import java.time.LocalDate;

/**
 * Created by Nickolay Golubitsky on 17.05.2016.
 */
public class SeaTour extends Voucher implements Cloneable{
    private int distanceToSea;

    public SeaTour() {
    }

    public SeaTour(int distanceToSea) {
        this.distanceToSea = distanceToSea;
    }

    public int getDistanceToSea() {
        return distanceToSea;
    }

    public void setDistanceToSea(int distanceToSea) {
        this.distanceToSea = distanceToSea;
    }

    @Override
    public boolean isValid() {
        boolean seaDistanceValid = (this.distanceToSea >= 0);
        boolean valid = seaDistanceValid && super.isValid();
        return valid ;
    }

    @Override
    public String toString() {
        return "SeaTour " +
                "distanceToSea=" + distanceToSea + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SeaTour)) return false;
        if (!super.equals(o)) return false;

        SeaTour seaTour = (SeaTour) o;

        return distanceToSea == seaTour.distanceToSea;

    }

    @Override
    public Voucher clone() {
        return super.clone();
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + distanceToSea;
        return result;
    }
}
