package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 17.05.2016.
 */
public class Cruise extends Voucher implements Cloneable{
    private String seaName;

    public Cruise() {
    }

    public Cruise(String seaName) {
        this.seaName = seaName;
    }

    public String getSeaName() {
        return seaName;
    }

    public void setSeaName(String seaName) {
        this.seaName = seaName;
    }

    @Override
    public String toString() {
        return "Cruise " +
                "seaName=" + seaName  + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cruise)) return false;
        if (!super.equals(o)) return false;
        Cruise cruise = (Cruise) o;
        return seaName.equals(cruise.seaName);

    }

    @Override
    public Voucher clone() {
        return super.clone();
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + seaName.hashCode();
        return result;
    }
}
