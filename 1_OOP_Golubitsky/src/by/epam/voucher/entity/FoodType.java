package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 14.05.2016.
 */
public enum FoodType {
    RO,
    BB,
    HB,
    FB,
    AI,
    UAI
}
