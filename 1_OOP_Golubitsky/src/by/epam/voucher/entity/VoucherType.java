package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 18.05.2016.
 */
public enum VoucherType {
    SEA_TOUR,
    SHOPPING,
    CRUISE,
    EXCURSION,
    HUNTING,
    SKI_TOUR
}
