package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 17.05.2016.
 */
public class SkiTour extends Voucher implements Cloneable{
    private String mountainName;

    public SkiTour() {
    }

    public SkiTour(String mountainName) {
        this.mountainName = mountainName;
    }

    public String getMountainName() {
        return mountainName;
    }

    public void setMountainName(String mountainName) {
        this.mountainName = mountainName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SkiTour)) return false;
        if (!super.equals(o)) return false;

        SkiTour skiTour = (SkiTour) o;

        return mountainName != null ? mountainName.equals(skiTour.mountainName) : skiTour.mountainName == null;

    }

    @Override
    public Voucher clone() {
        return super.clone();
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (mountainName != null ? mountainName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SkiTour " +
                "mountainName='" + mountainName + '\''
                + " " + super.toString();

    }
}
