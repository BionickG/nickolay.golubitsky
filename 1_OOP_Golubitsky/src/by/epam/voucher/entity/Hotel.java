package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 16.05.2016.
 */
public class Hotel implements Cloneable{
    private String hotelName;
    private RoomType roomType;
    private int starship;
    private double costPerNight;

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public int getStarship() {
        return starship;
    }

    public void setStarship(int starship) {
        this.starship = starship;
    }

    public double getCostPerNight() {
        return costPerNight;
    }

    public void setCostPerNight(double costPerNight) {
        this.costPerNight = costPerNight;
    }

    public boolean isValid()
    {
        if (this.costPerNight > 0 && this.starship > 0 && this.starship <= 5)
        {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hotel)) return false;

        Hotel hotel = (Hotel) o;

        if (starship != hotel.starship) return false;
        if (Double.compare(hotel.costPerNight, costPerNight) != 0) return false;
        if (hotelName != null ? !hotelName.equals(hotel.hotelName) : hotel.hotelName != null) return false;
        return roomType == hotel.roomType;

    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = hotelName != null ? hotelName.hashCode() : 0;
        result = 31 * result + (roomType != null ? roomType.hashCode() : 0);
        result = 31 * result + starship;
        temp = Double.doubleToLongBits(costPerNight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return this.getHotelName();
    }
}
