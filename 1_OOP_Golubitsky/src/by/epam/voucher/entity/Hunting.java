package by.epam.voucher.entity;

import java.util.ArrayList;

/**
 * Created by Nickolay Golubitsky on 17.05.2016.
 */
public class Hunting extends Voucher implements Cloneable{
    private String wildfowl;

    public Hunting() {
    }

    public Hunting(String wildfowl) {
        this.wildfowl = wildfowl;
    }

    public String getWildfowl() {
        return wildfowl;
    }

    public void setWildfowl(String wildfowl) {
        this.wildfowl = wildfowl;
    }

    @Override
    public String toString() {
        return "Hunting " +
                "wildfowl=" + wildfowl + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hunting)) return false;
        if (!super.equals(o)) return false;
        Hunting hunting = (Hunting) o;
        return wildfowl.equals(hunting.wildfowl);

    }

    @Override
    public Voucher clone() {
        return super.clone();
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + wildfowl.hashCode();
        return result;
    }
}
