package by.epam.voucher.entity;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;

/**
 * Created by Nickolay Golubitsky on 14.05.2016.
 */
public abstract class Voucher implements Cloneable{

    final static double DISCOUNT_FOR_CHILDREN = 0.5;
    final static double HOT_DISCOUNT = 0.5;
    final static Logger LOG = LogManager.getLogger();

    private int duration;
    private Transport transport;
    private int numberOfAdults;
    private int numberOfChildren;
    private double cost;
    private FoodType foodType;
    private Country country;
    private Hotel hotel;
    private LocalDate departureDate;
    private LocalDate arriveDate;
    private boolean isHot;

    public Voucher() {
    }

    public double costCalculate(){
        double cost;
        double hotelCost = this.duration * this.hotel.getCostPerNight();
        double transportCost = this.transport.getCostPerKM() * this.country.getDistance();
        cost = this.numberOfAdults * (hotelCost + transportCost)
                + this.numberOfChildren * (hotelCost + (transportCost * DISCOUNT_FOR_CHILDREN));
        if (this.isHot) {
            return cost * HOT_DISCOUNT;
        } else {
            return cost;
        }
    }

    public boolean isValid()
    {
        boolean digitValid;
        boolean notNull;
        notNull = !( (this.getTransport()== null) || (this.getFoodType()== null) || (this.getCountry()== null)
                || (this.getArriveDate()== null) || (this.getDepartureDate() == null) || this.getHotel() == null);
        if (notNull) {
            digitValid = !((this.getDuration() <= 0) || (this.getNumberOfAdults() < 0) || (this.getNumberOfChildren() < 0)
                    || (this.getCost() <= 0)  || !this.departureDate.plusDays(this.duration).isEqual(this.arriveDate));
        } else {
            return false;
        }
        return digitValid && notNull;


    }


    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public int getNumberOfAdults() {
        return numberOfAdults;
    }

    public void setNumberOfAdults(int numberOfAdults) {
        this.numberOfAdults = numberOfAdults;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public void setCost() {
        this.cost = this.costCalculate();
    }

    public double getCost() {

        return cost;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArriveDate() {
        return arriveDate;
    }

    public void setArriveDate(LocalDate arriveDate) {
        this.arriveDate = arriveDate;
    }

    public boolean isHot() {
        return isHot;
    }

    public void setHot(boolean hot) {
        isHot = hot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Voucher)) return false;
        Voucher voucher = (Voucher) o;
        if (duration != voucher.duration) return false;
        if (numberOfAdults != voucher.numberOfAdults) return false;
        if (numberOfChildren != voucher.numberOfChildren) return false;
        if (Double.compare(voucher.cost, cost) != 0) return false;
        if (isHot != voucher.isHot) return false;
        if (transport != voucher.transport) return false;
        if (foodType != voucher.foodType) return false;
        if (country != voucher.country) return false;
        if (!hotel.equals(voucher.hotel)) return false;
        if (!departureDate.equals(voucher.departureDate)) return false;
        return arriveDate.equals(voucher.arriveDate);

    }

    @Override
    public Voucher clone() {
        Voucher clone = null;
        try {
            clone = (Voucher) super.clone();
            clone.hotel = (Hotel) hotel.clone();
        } catch (CloneNotSupportedException e) {
            LOG.error(e.getMessage());
        }
        return clone;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = duration;
        result = 31 * result + transport.hashCode();
        result = 31 * result + numberOfAdults;
        result = 31 * result + numberOfChildren;
        temp = Double.doubleToLongBits(cost);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + foodType.hashCode();
        result = 31 * result + country.hashCode();
        result = 31 * result + hotel.hashCode();
        result = 31 * result + departureDate.hashCode();
        result = 31 * result + arriveDate.hashCode();
        result = 31 * result + (isHot ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return  " duration=" + duration +
                " transport=" + transport +
                " numberOfAdults=" + numberOfAdults +
                " numberOfChildren=" + numberOfChildren +
                " cost=" + cost +
                " foodType=" + foodType +
                " country=" + country +
                " hotel=" + hotel +
                " departureDate=" + departureDate +
                " arriveDate=" + arriveDate +
                " isHot=" + isHot;
    }
}
