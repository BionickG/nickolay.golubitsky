package by.epam.voucher.entity;

import by.epam.voucher.creation.VoucherCreator;
import by.epam.voucher.exception.CorruptFileException;
import by.epam.voucher.io.Reading;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by Nickolay Golubitsky on 17.05.2016.
 */
public class TourOperator {
    final static String TOURS_PATH = "resources/tours.txt";
    final static Logger LOG = LogManager.getLogger();
    private ArrayList<Voucher> vouchers = new ArrayList<>();
    public ArrayList<Voucher> getVouchers() {
        return (ArrayList<Voucher>) vouchers.clone();
    }
    public void setVouchers(ArrayList<Voucher> vouchers) {
        this.vouchers = vouchers;
    }

    public void fillIn ()
    {
        ArrayList<String> stringsFromFile = Reading.readFile(TOURS_PATH);
        for (String string : stringsFromFile)
        {
            Voucher voucher = VoucherCreator.createVoucher(string);
            if (voucher.isValid()) {
                this.vouchers.add(voucher);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TourOperator)) return false;
        TourOperator operator = (TourOperator) o;
        return vouchers != null ? vouchers.equals(operator.vouchers) : operator.vouchers == null;

    }

    @Override
    public int hashCode() {
        return vouchers != null ? vouchers.hashCode() : 0;
    }
}
