package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 16.05.2016.
 */
public enum RoomType {
    SGL,
    DBL,
    TRPL,
    DE_LUXE,
}
