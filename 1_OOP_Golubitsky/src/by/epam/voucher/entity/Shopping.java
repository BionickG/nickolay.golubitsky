package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 17.05.2016.
 */
public class Shopping extends Voucher implements Cloneable {
    private int shopNumber;

    public Shopping() {
    }

    public Shopping(int shopNumber) {
        this.shopNumber = shopNumber;
    }

    public int getShopNumber() {
        return shopNumber;
    }

    public void setShopNumber(int shopNumber) {
        this.shopNumber = shopNumber;
    }

    @Override
    public String toString() {
        return "Shopping " +
                "shopNumber=" + shopNumber
                + " " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shopping)) return false;
        if (!super.equals(o)) return false;

        Shopping shopping = (Shopping) o;

        return shopNumber == shopping.shopNumber;

    }

    @Override
    public Voucher clone() {
        return super.clone();
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + shopNumber;
        return result;
    }
}
