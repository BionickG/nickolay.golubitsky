package by.epam.voucher.entity;

import by.epam.voucher.creation.HotelCreator;
import by.epam.voucher.exception.CorruptFileException;
import by.epam.voucher.io.Reading;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by Nickolay Golubitsky on 18.05.2016.
 */
public class BookingService {
    final static String HOTEL_PATH = "resources/hotels.txt";
    final static Logger LOG = LogManager.getLogger();

    public static ArrayList<Hotel> hotels = new ArrayList<>();
    public static void fillIn ()
    {
        ArrayList<String> stringsFromFile = Reading.readFile(HOTEL_PATH);
        for (String string : stringsFromFile)
        {
            Hotel hotel = HotelCreator.createHotel(string);
            if (hotel.isValid()) {
                hotels.add(hotel);
            }
        }
        if (hotels.isEmpty())
        {
            LOG.warn("Hotel error : No valid hotels in file");
        }
    }

}
