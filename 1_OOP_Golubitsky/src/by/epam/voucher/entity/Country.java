package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 16.05.2016.
 */
public enum  Country implements Cloneable {
    ITALY(1800),
    SPAIN(2700),
    FRANCE(1700),
    UK(1900),
    NORWAY(1200),
    INDIA(5900);

    private int distance;

    public int getDistance() {
        return distance;
    }

    Country(int distance) {
        this.distance = distance;
    }
}
