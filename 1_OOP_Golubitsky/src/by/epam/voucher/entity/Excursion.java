package by.epam.voucher.entity;

/**
 * Created by Nickolay Golubitsky on 17.05.2016.
 */
public class Excursion extends Voucher implements Cloneable{
    private int excursionNumber;

    public Excursion() {
    }

    public Excursion(int excursionNumber) {
        this.excursionNumber = excursionNumber;
    }

    public int getExcursionNumber() {
        return excursionNumber;
    }

    public void setExcursionNumber(int excursionNumber) {
        this.excursionNumber = excursionNumber;
    }

    @Override
    public String toString() {
        return "Excursion " +
                "excursionNumber=" + excursionNumber + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Excursion)) return false;
        if (!super.equals(o)) return false;
        Excursion excursion = (Excursion) o;
        return excursionNumber == excursion.excursionNumber;

    }

    @Override
    public Voucher clone() {
        return super.clone();
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + excursionNumber;
        return result;
    }
}
